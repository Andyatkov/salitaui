//
//  UILabel+Size.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 07.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public extension UILabel {

    func preferredSize(for size: CGSize) -> CGSize {
        return self.textRect(forBounds: CGRect(origin: .zero, size: size), limitedToNumberOfLines: numberOfLines).size
    }
    
    func sizeForMaxNumberOfLines(width: CGFloat) -> CGSize {
        if numberOfLines > 0 {
            let height = CGFloat(numberOfLines) * font.lineHeight
            return CGSize(width: width, height: height)
        } else {
            return preferredSize(for: CGSize(width: width, height: .infinity))
        }
    }

}


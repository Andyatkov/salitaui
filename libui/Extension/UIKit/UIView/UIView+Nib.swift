//
//  UIView+Nib.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public extension UIView {
    
    class func loadFromNib() -> Self? {
        return UIView.viewFromNib(view: self)
    }
    
    class func viewFromNib<T: UIView>(view: T.Type) -> T? {
        let bundle = Bundle(for: T.self)
        let className = String(describing: T.self)
        
        guard bundle.path(forResource: className, ofType: "nib") != nil else {
            return nil
        }
        
        guard let nibContent = bundle.loadNibNamed(className, owner: nil) else {
            return nil
        }
        
        for element in nibContent where element is T {
            return element as? T !! fatalError("Couldn't cast to type \(T.self)")
        }
        
        return nil
    }
    
}

//
//  UITableView+Register.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public extension UITableView {
    
    func registerCell(_ viewType: AnyClass, reuseIdentifier: String? = nil, nibName: String? = nil) {
        let sourceBundle = Bundle(for: viewType)
        let reuseIdentifier = reuseIdentifier ?? String(describing: viewType)
        let nibName = nibName ?? reuseIdentifier
        
        if sourceBundle.path(forResource: reuseIdentifier, ofType: "nib") != nil {
            let nib = UINib(nibName: nibName, bundle: sourceBundle)
            register(nib, forCellReuseIdentifier: reuseIdentifier)
        } else {
            register(viewType, forCellReuseIdentifier: reuseIdentifier)
        }
    }
    
    func registerSupplementary(_ viewType: AnyClass, reuseIdentifier: String? = nil, nibName: String? = nil) {
        let sourceBundle = Bundle(for: viewType)
        let reuseIdentifier = reuseIdentifier ?? String(describing: viewType)
        let nibName = nibName ?? reuseIdentifier
        
        if sourceBundle.path(forResource: reuseIdentifier, ofType: "nib") != nil {
            let nib = UINib(nibName: nibName, bundle: sourceBundle)
            register(nib, forHeaderFooterViewReuseIdentifier: reuseIdentifier)
        } else {
            register(viewType, forHeaderFooterViewReuseIdentifier: reuseIdentifier)
        }
    }
    
}


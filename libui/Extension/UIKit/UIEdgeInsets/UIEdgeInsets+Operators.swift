//
//  UIEdgeInsets+Operators.swift
//  libui
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public func + (left: UIEdgeInsets, right: UIEdgeInsets) -> UIEdgeInsets {

    return UIEdgeInsets(top: left.top + right.top,
                        left: left.left + right.left,
                        bottom: left.bottom + right.bottom,
                        right: left.right + right.right)

}

public func - (left: UIEdgeInsets, right: UIEdgeInsets) -> UIEdgeInsets {

    return UIEdgeInsets(top: left.top + right.top,
                        left: left.left + right.left,
                        bottom: left.bottom + right.bottom,
                        right: left.right + right.right)

}

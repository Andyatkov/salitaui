//
//  UIColor+Blending.swift
//  SalitaUI
//
//  Created by Andrey Dyatkov on 24.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

extension UIColor {
    
    public func append(backgroundColor: UIColor, alpha: CGFloat) -> UIColor {
        let currentColor = CIColor(color: self)
        let backgroundColor = CIColor(color: backgroundColor)
        
        let red = backgroundColor.red * alpha + currentColor.red * (1 - alpha)
        let green = backgroundColor.green * alpha + currentColor.green * (1 - alpha)
        let blue = backgroundColor.blue * alpha + currentColor.blue * (1 - alpha)
        
        return UIColor(red: red, green: green, blue: blue, alpha: currentColor.alpha)
    }
    
}

//
//  UIColor+Light.swift
//  Demo
//
//  Created by Andrey Dyatkov on 17.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public extension UIColor {
    
    /// Set more light color.
    /// - Parameter value: from 0 to 1.
    func lighter(by value: CGFloat) -> UIColor {
        return adjust(by: abs(value) )
    }
    
    /// Set more dark color.
    /// - Parameter value:from 0 to 1.
    func darker(by value: CGFloat) -> UIColor {
        return adjust(by: -1 * abs(value) )
    }

    private func adjust(by value: CGFloat) -> UIColor {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        
        if getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            return UIColor(red: min(red + value, 1.0),
                           green: min(green + value, 1.0),
                           blue: min(blue + value, 1.0),
                           alpha: alpha)
        } else {
            return self
        }
    }
    
}

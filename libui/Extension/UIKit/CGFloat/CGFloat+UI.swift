//
//  CGFloat+UI.swift
//  libui
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

extension CGFloat {
    static let defaultCornerRadius: CGFloat = 4.0
}

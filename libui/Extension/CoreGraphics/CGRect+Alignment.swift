//
//  CGRect+Alignment.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import Foundation
import CoreGraphics

public extension CGRect {
    
    // MARK: - Public Methods
    
    /// Rectangle alignment anchor point.
    ///
    /// - top: Top anchor point.
    /// - bottom: Bottom anchor point.
    /// - center: Center anchor point.
    /// - left: Left anchor point.
    /// - right: Right anchor point.
    /// - center: Center anchor point.
    enum Anchor {
        public enum Vertical {
            case top
            case bottom
            case center
        }
        
        public enum Horizontal {
            case left
            case right
            case center
        }
    }

    /// Change rectangle horizontal position using anchor points.
    ///
    /// - Parameters:
    ///   - to: Rectangle relative to which alignment will be performed.
    ///   - fromAnchor: Anchor point of rectangle that will move.
    ///   - toAnchor: Rectangle anchor with respect to which the movement will be performed.
    ///   - offset: Horizontal offset.
    mutating func alignmentX(to: CGRect, fromAnchor: Anchor.Horizontal, toAnchor: Anchor.Horizontal, offset: CGFloat = 0) {
        origin.x = getAlignmentX(to: to, fromAnchor: fromAnchor, toAnchor: toAnchor, offset: offset)
    }
    
    /// Change rectangle vertical position using anchor points.
    ///
    /// - Parameters:
    ///   - to: Rectangle relative to which alignment will be performed.
    ///   - fromAnchor: Anchor point of rectangle that will move.
    ///   - toAnchor: Rectangle anchor with respect to which the movement will be performed.
    ///   - offset: Vertical offset.
    mutating func alignmentY(to: CGRect, fromAnchor: Anchor.Vertical, toAnchor: Anchor.Vertical, offset: CGFloat = 0) {
        origin.y = getAlignmentY(to: to, fromAnchor: fromAnchor, toAnchor: toAnchor, offset: offset)
    }
    
    /// Return rectangle horizontal position using anchor points.
    ///
    /// - Parameters:
    ///   - to: Rectangle relative to which alignment will be performed.
    ///   - fromAnchor: Anchor point of rectangle that will move.
    ///   - toAnchor: Rectangle anchor with respect to which the movement will be performed.
    ///   - offset: Horizontal offset.
    /// - Returns: The new position X coordinate.
    func getAlignmentX(to: CGRect, fromAnchor: Anchor.Horizontal, toAnchor: Anchor.Horizontal, offset: CGFloat = 0) -> CGFloat {
        return CGRect.alignmentX(from: self, to: to, fromAnchor: fromAnchor, toAnchor: toAnchor, offset: offset)
    }
    
    /// Return rectangle vertical position using anchor points.
    ///
    /// - Parameters:
    ///   - to: Rectangle relative to which alignment will be performed.
    ///   - fromAnchor: Anchor point of rectangle that will move.
    ///   - toAnchor: Rectangle anchor with respect to which the movement will be performed.
    ///   - offset: Vertical offset.
    /// - Returns: The new position Y coordinate.
    func getAlignmentY(to: CGRect, fromAnchor: Anchor.Vertical, toAnchor: Anchor.Vertical, offset: CGFloat = 0) -> CGFloat {
        return CGRect.alignmentY(from: self, to: to, fromAnchor: fromAnchor, toAnchor: toAnchor, offset: offset)
    }
    
    /// Return extra
    ///
    /// - Parameter frames: External frame that will surrounds all frames.
    /// - Returns: result external frame
    static func alignmentRect(for frames: [CGRect]) -> CGRect {
        let minX = frames.map { $0.minX }.min() ?? 0
        let minY = frames.map { $0.minY }.min() ?? 0
        
        let maxX = frames.map { $0.maxX }.max() ?? 0
        let maxY = frames.map { $0.maxY }.max() ?? 0
        
        return CGRect(x: minX, y: minY, width: maxX - minX, height: maxY - minY)
    }
    
    /// Return rectangle horizontal position using anchor points.
    ///
    /// - Parameters:
    ///   - from: Rectangle tha will be moved.
    ///   - to: Rectangle relative to which alignment will be performed.
    ///   - fromAnchor: Anchor point of rectangle that will move.
    ///   - toAnchor: Rectangle anchor with respect to which the movement will be performed.
    ///   - offset: Horizontal offset.
    /// - Returns: The new position X coordinate.
    static func alignmentY(from: CGRect, to: CGRect, fromAnchor: Anchor.Vertical, toAnchor: Anchor.Vertical, offset: CGFloat) -> CGFloat {
        switch fromAnchor {
        case .top:
            switch toAnchor {
            case .top:
                return CGRect.alignmentTopToTop(from: from, to: to, offset: offset)
            case .bottom:
                return CGRect.alignmentTopToBottom(from: from, to: to, offset: offset)
            case .center:
                return CGRect.alignmentTopToCenter(from: from, to: to, offset: offset)
            }
        case .bottom:
            switch toAnchor {
            case .top:
                return CGRect.alignmentBottomToTop(from: from, to: to, offset: offset)
            case .bottom:
                return CGRect.alignmentBottomToBottom(from: from, to: to, offset: offset)
            case .center:
                return CGRect.alignmentBottomToCenter(from: from, to: to, offset: offset)
            }
        case .center:
            switch toAnchor {
            case .top:
                return CGRect.alignmentCenterToTop(from: from, to: to, offset: offset)
            case .bottom:
                return CGRect.alignmentCenterToBottom(from: from, to: to, offset: offset)
            case .center:
                return CGRect.alignmentVerticalCenterToCenter(from: from, to: to, offset: offset)
            }
        }
    }
    
    /// Return rectangle vertical position using anchor points.
    ///
    /// - Parameters:
    ///   - from: Rectangle tha will be moved.
    ///   - to: Rectangle relative to which alignment will be performed.
    ///   - fromAnchor: Anchor point of rectangle that will move.
    ///   - toAnchor: Rectangle anchor with respect to which the movement will be performed.
    ///   - offset: Vertical offset.
    /// - Returns: The new position Y coordinate.
    static func alignmentX(from: CGRect, to: CGRect, fromAnchor: Anchor.Horizontal, toAnchor: Anchor.Horizontal, offset: CGFloat) -> CGFloat {
        switch fromAnchor {
        case .left:
            switch toAnchor {
            case .left:
                return CGRect.alignmentLeftToLeft(from: from, to: to, offset: offset)
            case .right:
                return CGRect.alignmentLeftToRight(from: from, to: to, offset: offset)
            case .center:
                return CGRect.alignmentLeftToCenter(from: from, to: to, offset: offset)
            }
        case .right:
            switch toAnchor {
            case .left:
                return CGRect.alignmentRightToLeft(from: from, to: to, offset: offset)
            case .right:
                return CGRect.alignmentRightToRight(from: from, to: to, offset: offset)
            case .center:
                return CGRect.alignmentRightToCenter(from: from, to: to, offset: offset)
            }
        case .center:
            switch toAnchor {
            case .left:
                return CGRect.alignmentCenterToLeft(from: from, to: to, offset: offset)
            case .right:
                return CGRect.alignmentCenterToRight(from: from, to: to, offset: offset)
            case .center:
                return CGRect.alignmentHorizontalCenterToCenter(from: from, to: to, offset: offset)
            }
        }
    }
    
    // MARK: - Private Methods
    
    // Vertical
    
    private static func alignmentTopToTop(from: CGRect, to: CGRect, offset: CGFloat) -> CGFloat {
        return to.minY + offset
    }
    
    private static func alignmentTopToBottom(from: CGRect, to: CGRect, offset: CGFloat) -> CGFloat {
        return to.maxY + offset
    }
    
    private static func alignmentTopToCenter(from: CGRect, to: CGRect, offset: CGFloat) -> CGFloat {
        return to.midY + offset
    }
    
    //
    
    private static func alignmentBottomToTop(from: CGRect, to: CGRect, offset: CGFloat) -> CGFloat {
        return to.minY - from.height + offset
    }
    
    private static func alignmentBottomToBottom(from: CGRect, to: CGRect, offset: CGFloat) -> CGFloat {
        return to.maxY - from.height + offset
    }
    
    private static func alignmentBottomToCenter(from: CGRect, to: CGRect, offset: CGFloat) -> CGFloat {
        return to.midY + (to.height / 2.0) + offset
    }
    
    //
    
    private static func alignmentCenterToTop(from: CGRect, to: CGRect, offset: CGFloat) -> CGFloat {
        return to.minY - (from.height / 2.0 ) + offset
    }
    
    private static func alignmentCenterToBottom(from: CGRect, to: CGRect, offset: CGFloat) -> CGFloat {
        return to.maxY - (from.height / 2.0) + offset
    }
    
    private static func alignmentVerticalCenterToCenter(from: CGRect, to: CGRect, offset: CGFloat) -> CGFloat {
        return to.midY - (from.height / 2.0) + offset
    }
    
    // Horizontal
    
    private static func alignmentLeftToLeft(from: CGRect, to: CGRect, offset: CGFloat) -> CGFloat {
        return to.minX + offset
    }
    
    private static func alignmentLeftToRight(from: CGRect, to: CGRect, offset: CGFloat) -> CGFloat {
        return to.maxX + offset
    }
    
    private static func alignmentLeftToCenter(from: CGRect, to: CGRect, offset: CGFloat) -> CGFloat {
        return to.midX + offset
    }
    
    //
    
    private static func alignmentRightToLeft(from: CGRect, to: CGRect, offset: CGFloat) -> CGFloat {
        return to.minX - from.width + offset
    }
    
    private static func alignmentRightToRight(from: CGRect, to: CGRect, offset: CGFloat) -> CGFloat {
        return to.maxX - from.width + offset
    }
    
    private static func alignmentRightToCenter(from: CGRect, to: CGRect, offset: CGFloat) -> CGFloat {
        return to.midX + (to.width / 2.0) + offset
    }
    
    //
    
    private static func alignmentCenterToLeft(from: CGRect, to: CGRect, offset: CGFloat) -> CGFloat {
        return to.minX - (from.width / 2.0 ) + offset
    }
    
    private static func alignmentCenterToRight(from: CGRect, to: CGRect, offset: CGFloat) -> CGFloat {
        return to.maxX - (from.width / 2.0) + offset
    }
    
    private static func alignmentHorizontalCenterToCenter(from: CGRect, to: CGRect, offset: CGFloat) -> CGFloat {
        return to.midX - (from.width / 2.0) + offset
    }
    
}


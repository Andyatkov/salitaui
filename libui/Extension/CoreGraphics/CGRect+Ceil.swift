//
//  CGRect+Ceil.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import CoreGraphics

func ceil(_ rect: CGRect) -> CGRect {
    return CGRect(origin: ceil(rect.origin), size: ceil(rect.size))
}

func ceil(_ size: CGSize) -> CGSize {
    return CGSize(width: ceil(size.width), height: ceil(size.height))
}

func ceil(_ point: CGPoint) -> CGPoint {
    return CGPoint(x: ceil(point.x), y: ceil(point.y))
}

//
//  Formatter.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 16.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import Foundation

public extension Formatter {
    
    static let withSeparator: NumberFormatter = {
        
        let formatter = NumberFormatter()
        formatter.groupingSeparator = " "
        formatter.numberStyle = .decimal
        return formatter
    }()
    
}

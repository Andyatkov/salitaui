//
//  Formatter+Float.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 16.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import Foundation

public extension Float {
    
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? ""
    }
    
}

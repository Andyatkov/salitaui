//
//  Durations.swift
//  libui
//
//  Created by Andrey Dyatkov on 20.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import Foundation

public struct Durations {

    public struct Default {
        public static let single: TimeInterval = 1.0 / 3.0
        public static let half: TimeInterval = single * 0.5
        public static let double: TimeInterval = single * 2.0
    }

}

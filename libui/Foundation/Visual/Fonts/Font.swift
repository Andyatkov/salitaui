//
//  Font.swift
//  libui
//
//  Created by Andrey Dyatkov on 20.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public extension UIFont {
    
    private enum Lato {
        static let allFonts: [String] = []
    }

    static var all: [UIFont] = []
}

public extension UIFont {
    
    /// Register custom fonts
    static func registerAllFonts() {
        let bundle = Bundle(for: BundleToken.self)
        
        Lato.allFonts.forEach {
            UIFont.registerFontWithFilenameString(filenameString: $0 + ".otf", bundle: bundle)
        }
    }
    
    static func registerFontWithFilenameString(filenameString: String, bundle: Bundle) {
        guard let pathForResourceString = bundle.path(forResource: filenameString, ofType: nil) else {
            print("UIFont+:  Failed to register font - path for resource not found.")
            return
        }
        
        guard let fontData = NSData(contentsOfFile: pathForResourceString) else {
            print("UIFont+:  Failed to register font - font data could not be loaded.")
            return
        }
        
        guard let dataProvider = CGDataProvider(data: fontData) else {
            print("UIFont+:  Failed to register font - data provider could not be loaded.")
            return
        }
        
        guard let fontRef = CGFont(dataProvider) else {
            print("UIFont+:  Failed to register font - font could not be loaded.")
            return
        }
        
        var errorRef: Unmanaged<CFError>?
        if CTFontManagerRegisterGraphicsFont(fontRef, &errorRef) == false {
            print("UIFont+:  Failed to register font - register graphics font failed - this font may have already been registered in the main bundle.")
        }
    }
    
}

private final class BundleToken {}

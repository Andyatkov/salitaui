//
//  Palette.swift
//  libui
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import Foundation

public protocol PaletteProtocol {
    static var `default`: Self { get }
    static var dark: Self { get }
}

public struct Palette: PaletteProtocol {
    
    public let colors: ColorPalette

    public static let `default`: Palette = .init(
        colors: .default
    )

    public static let dark: Palette = .init(
        colors: .dark
    )
    
}

//
//  ColorPalette.swift
//  libui
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public struct StaticPalette {
}

public struct DynamicPalette {
    
    // Colors
    public let wbBlack: UIColor
    public let wbGray: UIColor
    public let wbWhite: UIColor

    // Elements
    public let wbBrand: UIColor
    public let wbAccent: UIColor
    public let wbOrange: UIColor
    public let wbGreen: UIColor
    public let wbRed: UIColor
    public let wbDarkGray: UIColor
    public let wbYellow: UIColor

    // Background
    public let wbBackground: UIColor
    
    // With transparency
    public let wbClear: UIColor
}

public struct ColorPalette: PaletteProtocol {
    
    public let `static`: StaticPalette
    public let dynamic: DynamicPalette

    public static let `default`: ColorPalette = .init(
        static: .init(),
        dynamic: .init(
            wbBlack: UIColor(hexString: "#131313"),
            wbGray: UIColor(hexString: "#E7E7E7"),
            wbWhite: UIColor(hexString: "#FFFFFF"),
            wbBrand: UIColor(hexString: "#000000"),
            wbAccent: UIColor(hexString: "#7E00E6"),
            wbOrange: UIColor(hexString: "#FF783C"),
            wbGreen: UIColor(hexString: "#1EA52D"),
            wbRed: UIColor(hexString: "#EB3546"),
            wbDarkGray: UIColor(hexString: "#A7A7A7"),
            wbYellow: UIColor(hexString: "#F5BA45"),
            wbBackground: UIColor(hexString: "#F6F6F6"),
            wbClear: .clear)
    )

    public static let dark: ColorPalette = .init(
        static: .init(),
        dynamic: .init(wbBlack: UIColor(hexString: "#FFFFFF"),
                       wbGray: UIColor(hexString: "#E7E7E7"),
                       wbWhite: UIColor(hexString: "#131313"),
                       wbBrand: UIColor(hexString: "#000000"),
                       wbAccent: UIColor(hexString: "#7E00E6"),
                       wbOrange: UIColor(hexString: "#FF783C"),
                       wbGreen: UIColor(hexString: "#1EA52D"),
                       wbRed: UIColor(hexString: "#EB3546"),
                       wbDarkGray: UIColor(hexString: "#A7A7A7"),
                       wbYellow: UIColor(hexString: "#F5BA45"),
                       wbBackground: UIColor(hexString: "#F6F6F6"),
                       wbClear: .clear)
    )
    
}

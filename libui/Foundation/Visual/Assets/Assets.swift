// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSImage
  public typealias AssetColorTypeAlias = NSColor
  public typealias AssetImageTypeAlias = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIImage
  public typealias AssetColorTypeAlias = UIColor
  public typealias AssetImageTypeAlias = UIImage
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum Asset {
  public enum Cell {

    public enum Accessory {
      public static let icCar = ImageAsset(name: "Cell/Accessory/icCar")
      public static let icCheckBoxOFF = ImageAsset(name: "Cell/Accessory/icCheckBoxOFF")
      public static let icCheckBoxON = ImageAsset(name: "Cell/Accessory/icCheckBoxON")
      public static let icClose = ImageAsset(name: "Cell/Accessory/icClose")
      public static let icCustomer = ImageAsset(name: "Cell/Accessory/icCustomer")
      public static let icDownload = ImageAsset(name: "Cell/Accessory/icDownload")
      public static let icDownloading = ImageAsset(name: "Cell/Accessory/icDownloading")
      public static let icEdit = ImageAsset(name: "Cell/Accessory/icEdit")
      public static let icError = ImageAsset(name: "Cell/Accessory/icError")
      public static let icGoods = ImageAsset(name: "Cell/Accessory/icGoods")
      public static let icHistory = ImageAsset(name: "Cell/Accessory/icHistory")
      public static let icLink = ImageAsset(name: "Cell/Accessory/icLink")
      public static let icNews = ImageAsset(name: "Cell/Accessory/icNews")
      public static let icPackage = ImageAsset(name: "Cell/Accessory/icPackage")
      public static let icReport = ImageAsset(name: "Cell/Accessory/icReport")
      public static let icSaleSCalendar = ImageAsset(name: "Cell/Accessory/icSale'sCalendar")
      public static let icSale = ImageAsset(name: "Cell/Accessory/icSale")
      public static let icStats = ImageAsset(name: "Cell/Accessory/icStats")
      public static let icUnload = ImageAsset(name: "Cell/Accessory/icUnload")
    }
    public enum Arrow {
      public static let icArrow = ImageAsset(name: "Cell/Arrow/icArrow")
    }
  }
  public enum Navigation {

    public static let icAdd = ImageAsset(name: "Navigation/icAdd")
    public static let icAttention = ImageAsset(name: "Navigation/icAttention")
    public static let icCalendar = ImageAsset(name: "Navigation/icCalendar")
    public static let icDelete = ImageAsset(name: "Navigation/icDelete")
    public static let icExcel = ImageAsset(name: "Navigation/icExcel")
    public static let icFilter = ImageAsset(name: "Navigation/icFilter")
    public static let icFiltered = ImageAsset(name: "Navigation/icFiltered")
    public static let icHelp = ImageAsset(name: "Navigation/icHelp")
    public static let icHistory = ImageAsset(name: "Navigation/icHistory")
    public static let icPhone = ImageAsset(name: "Navigation/icPhone")
    public static let icPlaceholder = ImageAsset(name: "Navigation/icPlaceholder")
    public static let icSetting = ImageAsset(name: "Navigation/icSetting")
    public static let icStats = ImageAsset(name: "Navigation/icStats")
  }
  public enum Placeholder {

    public enum Default {
      public static let icBlankLarge = ImageAsset(name: "Placeholder/Default/icBlankLarge")
      public static let icConnectLarge = ImageAsset(name: "Placeholder/Default/icConnectLarge")
      public static let icDocumentLarge = ImageAsset(name: "Placeholder/Default/icDocumentLarge")
      public static let icGoodsLarge = ImageAsset(name: "Placeholder/Default/icGoodsLarge")
      public static let icHistoryLarge = ImageAsset(name: "Placeholder/Default/icHistoryLarge")
      public static let icLensLarge = ImageAsset(name: "Placeholder/Default/icLensLarge")
      public static let icNotificationLarge = ImageAsset(name: "Placeholder/Default/icNotificationLarge")
      public static let icReportLarge = ImageAsset(name: "Placeholder/Default/icReportLarge")
      public static let icScan = ImageAsset(name: "Placeholder/Default/icScan")
      public static let icServerLarge = ImageAsset(name: "Placeholder/Default/icServerLarge")
      public static let icSupplyLarge = ImageAsset(name: "Placeholder/Default/icSupplyLarge")
      public static let icWBLogo = ImageAsset(name: "Placeholder/Default/icWBLogo")
    }
    public enum Product {
      public static let icProductPlaceholder = ImageAsset(name: "Placeholder/Product/icProductPlaceholder")
    }
  }
  public enum TabBar {

    public static let icHome = ImageAsset(name: "TabBar/icHome")
    public static let icHomeII = ImageAsset(name: "TabBar/icHomeII")
    public static let icMenu = ImageAsset(name: "TabBar/icMenu")
    public static let icNotification = ImageAsset(name: "TabBar/icNotification")
    public static let icSupply = ImageAsset(name: "TabBar/icSupply")
    public static let icSupplyII = ImageAsset(name: "TabBar/icSupplyII")
  }
  public enum Textfield {

    public enum Regions {
      public static let flagAr = ImageAsset(name: "Textfield/Regions/flagAr")
      public static let flagBy = ImageAsset(name: "Textfield/Regions/flagBy")
      public static let flagKg = ImageAsset(name: "Textfield/Regions/flagKg")
      public static let flagKz = ImageAsset(name: "Textfield/Regions/flagKz")
      public static let flagRu = ImageAsset(name: "Textfield/Regions/flagRu")
      public static let flagUa = ImageAsset(name: "Textfield/Regions/flagUa")
    }
    public static let icDownArrow = ImageAsset(name: "Textfield/icDownArrow")
    public static let icFillDownArrow = ImageAsset(name: "Textfield/icFillDownArrow")
    public static let icMasked = ImageAsset(name: "Textfield/icMasked")
    public static let icNotMasked = ImageAsset(name: "Textfield/icNotMasked")
    public static let icSearch = ImageAsset(name: "Textfield/icSearch")
    public static let icSuccess = ImageAsset(name: "Textfield/icSuccess")
  }
  public enum Views {

    public enum NewFeatureView {
      public static let ilBag = ImageAsset(name: "Views/NewFeatureView/ilBag")
      public static let ilMultiselect = ImageAsset(name: "Views/NewFeatureView/ilMultiselect")
    }
  }

  // swiftlint:disable trailing_comma
  public static let allColors: [ColorAsset] = [
  ]
  public static let allDataAssets: [DataAsset] = [
  ]
  public static let allImages: [ImageAsset] = [
    Cell.Accessory.icCar,
    Cell.Accessory.icCheckBoxOFF,
    Cell.Accessory.icCheckBoxON,
    Cell.Accessory.icClose,
    Cell.Accessory.icCustomer,
    Cell.Accessory.icDownload,
    Cell.Accessory.icDownloading,
    Cell.Accessory.icEdit,
    Cell.Accessory.icError,
    Cell.Accessory.icGoods,
    Cell.Accessory.icHistory,
    Cell.Accessory.icLink,
    Cell.Accessory.icNews,
    Cell.Accessory.icPackage,
    Cell.Accessory.icReport,
    Cell.Accessory.icSaleSCalendar,
    Cell.Accessory.icSale,
    Cell.Accessory.icStats,
    Cell.Accessory.icUnload,
    Cell.Arrow.icArrow,
    Navigation.icAdd,
    Navigation.icAttention,
    Navigation.icCalendar,
    Navigation.icDelete,
    Navigation.icExcel,
    Navigation.icFilter,
    Navigation.icFiltered,
    Navigation.icHelp,
    Navigation.icHistory,
    Navigation.icPhone,
    Navigation.icPlaceholder,
    Navigation.icSetting,
    Navigation.icStats,
    Placeholder.Default.icBlankLarge,
    Placeholder.Default.icConnectLarge,
    Placeholder.Default.icDocumentLarge,
    Placeholder.Default.icGoodsLarge,
    Placeholder.Default.icHistoryLarge,
    Placeholder.Default.icLensLarge,
    Placeholder.Default.icNotificationLarge,
    Placeholder.Default.icReportLarge,
    Placeholder.Default.icScan,
    Placeholder.Default.icServerLarge,
    Placeholder.Default.icSupplyLarge,
    Placeholder.Default.icWBLogo,
    Placeholder.Product.icProductPlaceholder,
    TabBar.icHome,
    TabBar.icHomeII,
    TabBar.icMenu,
    TabBar.icNotification,
    TabBar.icSupply,
    TabBar.icSupplyII,
    Textfield.Regions.flagAr,
    Textfield.Regions.flagBy,
    Textfield.Regions.flagKg,
    Textfield.Regions.flagKz,
    Textfield.Regions.flagRu,
    Textfield.Regions.flagUa,
    Textfield.icDownArrow,
    Textfield.icFillDownArrow,
    Textfield.icMasked,
    Textfield.icNotMasked,
    Textfield.icSearch,
    Textfield.icSuccess,
    Views.NewFeatureView.ilBag,
    Views.NewFeatureView.ilMultiselect,
  ]
  // swiftlint:enable trailing_comma
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

public struct ColorAsset {
  public fileprivate(set) var name: String

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  public var color: AssetColorTypeAlias {
    return AssetColorTypeAlias(asset: self)
  }
}

public extension AssetColorTypeAlias {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  convenience init!(asset: ColorAsset) {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

public struct DataAsset {
  public fileprivate(set) var name: String

  #if os(iOS) || os(tvOS) || os(OSX)
  @available(iOS 9.0, tvOS 9.0, OSX 10.11, *)
  public var data: NSDataAsset {
    return NSDataAsset(asset: self)
  }
  #endif
}

#if os(iOS) || os(tvOS) || os(OSX)
@available(iOS 9.0, tvOS 9.0, OSX 10.11, *)
public extension NSDataAsset {
  convenience init!(asset: DataAsset) {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    self.init(name: asset.name, bundle: bundle)
    #elseif os(OSX)
    self.init(name: NSDataAsset.Name(asset.name), bundle: bundle)
    #endif
  }
}
#endif

public struct ImageAsset {
  public fileprivate(set) var name: String

  public var image: AssetImageTypeAlias {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    let image = AssetImageTypeAlias(named: name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    let image = bundle.image(forResource: NSImage.Name(name))
    #elseif os(watchOS)
    let image = AssetImageTypeAlias(named: name)
    #endif
    guard let result = image else { fatalError("Unable to load image named \(name).") }
    return result
  }
}

public extension AssetImageTypeAlias {
  @available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
  @available(OSX, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init!(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = Bundle(for: BundleToken.self)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

private final class BundleToken {}

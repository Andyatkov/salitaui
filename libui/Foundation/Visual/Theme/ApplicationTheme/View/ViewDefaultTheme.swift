//
//  ViewDefaultTheme.swift
//  libui
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public struct ViewDefaultTheme: ColorTheme {

    public let banner: TextNotificationViewTheme
    public let newFeatureView: NewFeatureViewTheme
    public let separatirView: SeparatorViewTheme
    public let segmentControlTheme: SegmentControlItemTheme
    public let materialSegmentedControlTheme: MaterialSegmentedControlTheme
    
    public init(palette: Palette) {
        
        banner = .init(palette: palette)
        newFeatureView = .init(palette: palette)
        separatirView = .init(palette: palette)
        segmentControlTheme = .init(palette: palette)
        materialSegmentedControlTheme = .init(pallete: palette)
    }
    
}

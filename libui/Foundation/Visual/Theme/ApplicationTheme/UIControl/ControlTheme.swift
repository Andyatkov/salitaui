//
//  ControlTheme.swift
//  Demo
//
//  Created by Andrey Dyatkov on 14.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public struct ControlTheme: ColorTheme {
    
    let pinViewTheme: PinViewTheme

    public init(palette: Palette) {
        
        pinViewTheme = .init(palette: palette)
    }
    
}



//
//  SegmentControlItemTheme.swift
//  WBLibui
//
//  Created by Юрий Андрюшин on 31.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public struct SegmentControlItemTheme: ColorTheme {

    public let filledSegmentTitleItem: FilledSegmentTitleItemTheme
    public let lineSegmentTitleItem: LineSegmentTitleItemTheme

    public init(palette: Palette) {
        
        filledSegmentTitleItem = .init(palette: palette)
        lineSegmentTitleItem = .init(palette: palette)
    }
    
}


//
//  UIViewController+Theme.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 05.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

// TODO: - Add Swizling after
//private let swizzling: (AnyClass, Selector, Selector) -> () = { forClass, originalSelector, swizzledSelector in
//    guard
//        let originalMethod = class_getInstanceMethod(forClass, originalSelector),
//        let swizzledMethod = class_getInstanceMethod(forClass, swizzledSelector)
//    else { return }
//    method_exchangeImplementations(originalMethod, swizzledMethod)
//}
//
//extension UIViewController {
//
//    public static let classInit: Void = {
//        let originalSelector = #selector(viewDidLoad)
//        let swizzledSelector = #selector(swizzled_viewDidLoad)
//        swizzling(UIViewController.self, originalSelector, swizzledSelector)
//    }()
//
//    @objc func swizzled_viewDidLoad() {
//        let bundle = Bundle(for: self.classForCoder)
//        guard bundle.bundleIdentifier == Bundle.main.bundleIdentifier else { return }
//
//        observe(theme: \DefaultTheme.viewController, on: WBLibThemeManager.wblibuiManager)
//    }
//
//}

//extension UIViewController: Themeable {
//
//    public typealias Theme = ViewControllerTheme
//
//    public func apply(theme: Theme) {
//        view.backgroundColor = theme.backgroundColor
//    }
//}
//
//class A<T: ColorTheme>: UIViewController, Themeable {
//    
//    
//    func apply(theme: T) {
//    }
//    
//}
//
//class B: A<ViewControllerTheme> {
//    
//    override func apply(theme: ViewControllerTheme) {
//        
//    }
//    
//}

//
//  BaseViewController.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 20.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public struct BaseViewControllerTheme: ColorTheme {
    
    let backgroundColor: UIColor

    init(palette: Palette) {
        backgroundColor = palette.colors.dynamic.wbWhite
    }
    
}

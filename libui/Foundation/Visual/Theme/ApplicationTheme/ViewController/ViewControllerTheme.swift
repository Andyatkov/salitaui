//
//  ViewTheme.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 05.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public struct ViewControllerTheme: ColorTheme {
    
    let baseViewControllerTheme: BaseViewControllerTheme

    init(palette: Palette) {
        baseViewControllerTheme = .init(palette: palette)
    }
}

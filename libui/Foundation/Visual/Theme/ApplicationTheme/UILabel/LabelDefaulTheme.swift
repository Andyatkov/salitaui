//
//  LabelDefaulTheme.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 16.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public struct LabelDefaulTheme: ColorTheme {

    public let currencyLabelTheme: CurrencyLabelTheme

    public init(palette: Palette) {
        currencyLabelTheme = .init(palette: palette)
    }
}

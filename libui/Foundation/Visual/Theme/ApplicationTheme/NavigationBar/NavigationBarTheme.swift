//
//  NavigationBarTheme.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 05.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public struct NavigationBarTheme: ColorTheme {
    
    let titleColor: UIColor
    let tintColor: UIColor
    let backgroundColor: UIColor

    init(palette: Palette) {
        titleColor = palette.colors.dynamic.wbBlack
        tintColor = palette.colors.dynamic.wbBrand
        backgroundColor = palette.colors.dynamic.wbWhite
    }
}

extension UINavigationBar: Themeable {
    
    public typealias Theme = NavigationBarTheme

    public func apply(theme: Theme) {
        titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: theme.titleColor
        ]
        tintColor = theme.tintColor
        barTintColor = theme.backgroundColor
    }
    
}

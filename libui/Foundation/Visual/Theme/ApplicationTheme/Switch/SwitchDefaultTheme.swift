//
//  SwitchDefaultTheme.swift
//  libui
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public struct SwitchDefaultTheme: ColorTheme {

    public let customSwitch: CustomSwitchTheme

    public init(palette: Palette) {
        customSwitch = .init(palette: palette)
    }
}

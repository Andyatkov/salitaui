//
//  TextFieldDefaultTheme.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public struct TextFieldDefaultTheme: ColorTheme {

    public let textField: TextFieldTheme
    public let searchTextField: SearchTextFieldTheme
    public let searchCancelableTextField: SearchCancelableTextFieldTheme
    public let searchBar: SearchBarTheme
    public let materialTextFieldTheme: MaterialTextFieldTheme
    public let regionTextFieldTheme: RegionTextFieldTheme

    public init(palette: Palette) {
        textField = .init(palette: palette)
        searchTextField = .init(palette: palette)
        searchCancelableTextField = .init(palette: palette)
        searchBar = .init(palette: palette)
        materialTextFieldTheme = .init(palette: palette)
        regionTextFieldTheme = .init(palette: palette)
    }
    
}

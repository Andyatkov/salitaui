//
//  TableViewTheme.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 05.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public struct ScrollViewTheme: ColorTheme {
    
    let backgroundColor: UIColor

    init(palette: Palette) {
        backgroundColor = palette.colors.dynamic.wbWhite
    }
}

extension UIScrollView: Themeable {
    
    public typealias Theme = ScrollViewTheme

    public func apply(theme: Theme) {
        backgroundColor = theme.backgroundColor
    }
    
}

//
//  ApplicationTheme.swift
//  Demo
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public struct DefaultTheme: ColorTheme {

    public let label: LabelDefaulTheme
    public let button: ButtonDefaultTheme
    public let `switch`: SwitchDefaultTheme
    public let view: ViewDefaultTheme
    public let textfield: TextFieldDefaultTheme
    public let navigationBar: NavigationBarTheme
    public let scrollView: ScrollViewTheme
    public let viewController: ViewControllerTheme
    public let control: ControlTheme
    public let collectionViewCell: CollectionViewCellDefaultTheme

    public init(palette: Palette) {
        
        label = .init(palette: palette)
        button = .init(palette: palette)
        self.switch = .init(palette: palette)
        view = .init(palette: palette)
        textfield = .init(palette: palette)
        navigationBar = .init(palette: palette)
        scrollView = .init(palette: palette)
        viewController = .init(palette: palette)
        control = .init(palette: palette)
        collectionViewCell = .init(palette: palette)
    }
    
}

public class WBLibThemeManager {
    
    public static var palette: Palette = .default {
        didSet {
            WBLibThemeManager.wblibuiManager.theme = DefaultTheme(palette: palette)
        }
    }
    
    public static let wblibuiManager = ThemeManager()
}

//
//  DefaultTheme+Button.swift
//  libui
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public struct ButtonDefaultTheme: ColorTheme {

    public let plainButton: PlainButtonTheme
    public let fillButton: FillButtonTheme
    public let borderButton: BorderButtonTheme

    public init(palette: Palette) {
        plainButton = .init(palette: palette)
        fillButton = .init(palette: palette)
        borderButton = .init(palette: palette)
    }
}

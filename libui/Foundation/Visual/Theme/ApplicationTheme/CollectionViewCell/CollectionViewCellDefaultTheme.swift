//
//  CollectionViewCellDefaultTheme.swift
//  WBLibui
//
//  Created by Orlov Maxim on 16.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public struct CollectionViewCellDefaultTheme: ColorTheme {

    public let phoneTextFieldCollectionCellCustomTheme: PhoneTextFieldCollectionCellCustomTheme
    public let strictTextFieldCollectionCellCustomTheme: StrictTextFieldCollectionCellCustomTheme

    public init(palette: Palette) {
        
        phoneTextFieldCollectionCellCustomTheme = .init(palette: palette)
        strictTextFieldCollectionCellCustomTheme = .init(palette: palette)
    }
    
}

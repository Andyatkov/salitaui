//
//  Theme.swift
//  libui
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

/// Tagging protocol for  themes
public protocol ColorTheme {}

/// Disposable pattern
public class Disposable {
    private let disposal: () -> ()

    init(disposal: @escaping () -> ()) {
        self.disposal = disposal
    }

    deinit {
        self.disposal()
    }
}

//
//  Themeable.swift
//  libui
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

/// Utility protocol for enabling  themes
public protocol Themeable: class {
    associatedtype Theme: ColorTheme

    /// The implementation of this function should be
    /// [idempotent](https://en.wikipedia.org/wiki/Idempotence)
    /// to avoid unwanted side-effects on repeated calls.
    func apply(theme: Theme)
}

extension Themeable {
    
    public func observe<T>(theme keyPath: KeyPath<T, Theme>,
                           on themeManager: ThemeManager = .default) where T: ColorTheme {
        themeManager.manage(theme: keyPath, for: self)
    }
    
}

extension Themeable where Self: UIAppearance {
    
    public static func observe<T>(theme keyPath: KeyPath<T, Theme>,
                                  on themeManager: ThemeManager = .default,
                                  appearance: @escaping (Self.Type) -> Self) -> Disposable where T: ColorTheme {
        
        return themeManager.observe(theme: T.self) { theme in
            self.apply(
                theme: theme[keyPath: keyPath],
                appearance: appearance
            )
        }
    }

    public static func apply(theme: Theme, appearance: (Self.Type) -> Self) {
        appearance(self).apply(theme: theme)
    }
    
}

public typealias AnyThemeable = AnyObject


//
//  ThemeObserver.swift
//  libui
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

internal class ThemeObserver {
    
    private let closure: (ColorTheme) -> ()

    internal init(closure: @escaping (ColorTheme) -> ()) {
        self.closure = closure
    }

    internal func handleThemeChange(on themeManager: ThemeManager) {
        assert(Thread.isMainThread)

        guard let theme = themeManager.theme else {
            var themeManager = themeManager
            Swift.withUnsafePointer(to: &themeManager) {
                NSLog("No theme found for theme manager \($0)")
            }
            return
        }
        themeManager.animated(duration: themeManager.animationDuration) {
            self.closure(theme)
        }
    }
    
}


//
//  libui.h
//  libui
//
//  Created by Andrey Dyatkov on 19.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for libui.
FOUNDATION_EXPORT double libuiVersionNumber;

//! Project version string for libui.
FOUNDATION_EXPORT const unsigned char libuiVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <libui/PublicHeader.h>



//
//  CodeInputControl.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 14.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

@objc public protocol CodeInputControlDelegate: class {
    @objc optional func shouldReturn(control: CodeInputControl)
    @objc optional func codeInputControlDidFinish(_ control: CodeInputControl, text: String)
    @objc optional func codeInputControl(_ control: CodeInputControl, textDidChange text: String)
    @objc optional func codeInputControl(_ control: CodeInputControl, shouldChange newString: String) -> Bool
}

/// Abstract class of input control.
open class CodeInputControl: UIControl {

    public enum Style {
        case `default`
    }
    
    public enum Status {
        case normal
        case success
        case error
    }
    
    // MARK: - Public Properties
    
    open var style: Style = .default {
        didSet { updateColors() }
    }
    
    /// Number of items in control.
    open var itemsCount = 4 {
        willSet { prepareViews(elementsCount: newValue) }
    }
    
    /// Delegate
    open weak var delegate: CodeInputControlDelegate?
    
    /// Input text
    open var text: String {
        get {
            return codeViews
                .compactMap { $0.text }
                .reduce("") { $0 + $1 }
        }
        set {
            cursorPosition = 0
            codeViews.forEach { $0.text = nil }
            newValue.forEach { insert(text: String($0), delegate: nil) }
        }
    }
    
    /// Current controll status.
    open var status: Status = .normal {
        didSet { updateColorsAnimated() }
    }
    
    /// The property that indicate is control focused.
    open var isOnFocus: Bool = false {
        didSet { updateColorsAnimated() }
    }
    
    /// The property that indicate that control is filled.
    open var filled: Bool {
        return cursorPosition >= itemsCount
    }
    
    /// Set this propetry to true to ignore all user input.
    open var ignoreUserInput: Bool = false
    
    // MARK: - UITextInputTraits
    
    /// The type of keyboard to display for a given text-based view.
    open var keyboardType: UIKeyboardType = .default {
        didSet { textField.keyboardType = keyboardType }
    }
    
    // The textContentType property is to provide the keyboard with extra information about the semantic intent of the text document.
    open var textContentType: UITextContentType! {
        didSet { textField.textContentType = textContentType }
    }
    
    // MARK: - Internal Properties
    
    var codeViews = [CodeInputItemView]()
    var textField = UITextField()
    var parameters = Layout.Parameters()
    
    var cursorPosition = 0 {
        didSet { updateColorsAnimated() }
    }
    
    // MARK: - UIResponder
    
    override open var canBecomeFirstResponder: Bool {
        return true
    }
    
    // MARK: - Initializers
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        backgroundColor = .clear
        prepareViews(elementsCount: itemsCount)
        
        textField.delegate = self
        
        addSubview(textField)
    }
    
    @discardableResult
    override open func becomeFirstResponder() -> Bool {
        defer { isOnFocus = true }
        textField.becomeFirstResponder()
        return false
    }
    
    @discardableResult
    override open func resignFirstResponder() -> Bool {
        defer { isOnFocus = false }
        return super.resignFirstResponder()
    }
    
    // MARK: - Public Methods
    
    /// Abstract method to create new input view.
    /// Must be overridden in subclass.
    /// - Returns: Custom code input item CodeInputItemView.
    func createNewItem() -> CodeInputItemView {
        fatalError("This is abstract property. Override it to return new item.")
    }
    
    // MARK: - Internal Methods
    
    func updateColorsAnimated() {
        let durartion = Durations.Default.single
        
        UIView.animate(
            withDuration: durartion,
            delay: 0,
            options: [.curveEaseInOut],
            animations: { [weak self] in
                self?.updateColors()
            }, completion: nil)
    }
    
    func updateColors() {
        codeViews.forEach { $0.isOnFocus = false }
        codeViews.forEach { $0.style = style }
        
        if cursorPosition < codeViews.count {
            codeViews[cursorPosition].isOnFocus = isOnFocus
        } else {
            codeViews.last?.isOnFocus = false
        }
        
        codeViews.forEach { $0.status = status }
    }
    
    // MARK: - Private Methods
    
    private func prepareViews(elementsCount: Int) {
        let count = codeViews.count
        
        if elementsCount > count {
            for i in count..<elementsCount {
                let view = createNewItem()
                view.tag = i + 1
                
                addSubview(view)
                codeViews.append(view)
            }
            updateColors()
            
        } else if elementsCount < count {
            var tmpCount = count
            
            for i in elementsCount..<tmpCount {
                codeViews[i].removeFromSuperview()
                codeViews.remove(at: i)

                tmpCount -= 1
            }
            
            cursorPosition = min(cursorPosition, tmpCount)
        }
    }
    
    // MARK: - Layout
    
    override open var intrinsicContentSize: CGSize {
        return sizeThatFits(frame.size)
    }
    
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        let count = codeViews.count
        let itemSize = parameters.itemSize
        let minSpacing = parameters.minSpacing
        
        let width = itemSize.width * CGFloat(count) + CGFloat(max(0, count - 1)) * minSpacing
        
        return CGSize(width: width, height: itemSize.height)
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        let result = calculateLayout(size: frame.size)
        result.digitViews.enumerated().forEach { codeViews[$0.offset].frame = $0.element }
        
        invalidateIntrinsicContentSize()
    }

}

// MARK: - UITextFieldDelegate

extension CodeInputControl: UITextFieldDelegate {
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch string {
        case " ":
            return true
        case "":
            deleteBackward()
            return true
        default:
            insertText(string)
            return false
        }
    }
    
}

extension CodeInputControl {
    
    fileprivate func insert(text: String, delegate: CodeInputControlDelegate? = nil) {

        var resultText: String = (textField.text ?? "") + text
        
        if text.count >= itemsCount {
            let autoText = Array(text)
            resultText = ""
            
            for index in 0..<itemsCount {
                guard String(autoText[index]) != "\n" else {
                    delegate?.shouldReturn?(control: self)
                    return
                }

                guard
                    !String(autoText[index]).isEmpty,
                    index <= itemsCount,
                    let view = viewWithTag(index + 1) as? CodeInputItemView
                    else {
                        delegate?.codeInputControlDidFinish?(self, text: String(autoText[index]))
                        return
                }

                let shoudChange = delegate?.codeInputControl?(self, shouldChange: String(autoText[index])) ?? true

                guard shoudChange else { return }

                view.text = String(autoText[index])
                resultText += String(autoText[index])
                cursorPosition += 1

                delegate?.codeInputControl?(self, textDidChange: text)
            }
        } else if resultText.count > itemsCount {
            resultText = textField.text ?? ""
        }

        let nextTag = cursorPosition + 1

        guard text != "\n" else {
            delegate?.shouldReturn?(control: self)
            return
        }
        
        textField.text = resultText

        guard
            !text.isEmpty,
            nextTag <= itemsCount,
            let view = viewWithTag(nextTag) as? CodeInputItemView
        else {
            delegate?.codeInputControlDidFinish?(self, text: text)

            return
        }

        let shoudChange = delegate?.codeInputControl?(self, shouldChange: text) ?? true

        guard shoudChange else { return }

        view.text = text
        cursorPosition += 1
        
        delegate?.codeInputControl?(self, textDidChange: text)
    }


    fileprivate func deleteText(delegate: CodeInputControlDelegate? = nil) {
        guard cursorPosition > 0,
            let view = viewWithTag(cursorPosition) as? CodeInputItemView else {
                return
        }

        let shoudChange = delegate?.codeInputControl?(self, shouldChange: "") ?? true

        guard shoudChange else { return }

        view.text = ""
        cursorPosition -= 1

        delegate?.codeInputControl?(self, textDidChange: text)
    }
    
    public func insertText(_ text: String) {
        guard !ignoreUserInput else { return }

        insert(text: text, delegate: delegate)
    }

    public func deleteBackward() {
        guard !ignoreUserInput else { return }

        deleteText(delegate: delegate)
    }
}

// MARK: - Layout

extension CodeInputControl {

    struct Layout {
        struct Parameters {
            var itemSize = CGSize(width: 44, height: 44)
            var minSpacing: CGFloat = 16
        }
        struct Frames {
            var spacing: CGFloat
            var digitViews: [CGRect]
        }
    }

    func calculateLayout(size: CGSize) -> Layout.Frames {
        let count = codeViews.count
        let itemSize = parameters.itemSize
        let minSpacing = parameters.minSpacing

        var spacing: CGFloat = 0

        if count > 1 {
            spacing = max((size.width - CGFloat(count) * itemSize.width) / (CGFloat(count) - 1), minSpacing)
        }

        var frames = [CGRect]()
        var offsetX: CGFloat = 0

        for _ in 0..<count {
            let frame = CGRect(x: offsetX, y: 0, width: itemSize.width, height: itemSize.height)
            offsetX += itemSize.width + spacing

            frames.append(frame)
        }

        return Layout.Frames(spacing: spacing, digitViews: frames)
    }

}


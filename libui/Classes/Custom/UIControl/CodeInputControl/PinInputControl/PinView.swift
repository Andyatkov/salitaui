//
//  PinView.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 14.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

/// Single pin circle that can be displayed by CodeInputControl.
open class PinView: UIView, CodeInputItem {

    // MARK: - Public Properties
    
    open var style: CodeInputControl.Style = .default {
        didSet { updateColors() }
    }
    
    public var text: String? {
        didSet { updateColors() }
    }

    public var status: CodeInputControl.Status = .normal {
        didSet { updateColors() }
    }
    
    public var isOnFocus: Bool = false {
        didSet { updateColors() }
    }
    
    open override class var layerClass: AnyClass {
        return CAShapeLayer.self
    }
    
    // MARK: - Internal Properties
    
    var theme: PinViewTheme? {
        didSet {
            updateColors()
        }
    }
    
    // MARK: - Initializers
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        
        observe(theme: \DefaultTheme.control.pinViewTheme, on: WBLibThemeManager.wblibuiManager)
        
        layer.borderWidth = 1.5
        
        if let layer = layer as? CAShapeLayer {
            layer.shouldRasterize = true
            layer.rasterizationScale = UIScreen.main.scale
        }
        
        updateColors()
    }
    
    private func updateColors() {
        switch status {
        case .normal:
            break
        case .success:
            backgroundColor = theme?.status.success
            layer.borderColor = theme?.status.success.cgColor
            
            return
        case .error:
            backgroundColor = theme?.status.error
            layer.borderColor = theme?.status.error.cgColor
            
            return
        }
        
        let isEmpty = (text ?? "").isEmpty
        
        if !isEmpty {
            backgroundColor = theme?.status.normal.select
            layer.borderColor = theme?.status.normal.select.cgColor
        } else {
            backgroundColor = theme?.status.normal.deselect
            layer.borderColor = theme?.status.normal.deselect.cgColor
        }
        
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        round()
    }
    
    func round() {
        layer.cornerRadius = frame.width / 2.0
    }
    
}

public struct PinViewTheme: ColorTheme {
    
    struct Normal {
        let focus: UIColor
        let select: UIColor
        let deselect: UIColor
    }
    
    struct Status {
        let success: UIColor
        let error: UIColor
        let normal: Normal
    }
    
    let status: Status
    
    public init(palette: Palette) {
        let normalState = Normal(focus: palette.colors.dynamic.wbBrand,
                                 select: palette.colors.dynamic.wbBrand,
                                 deselect: palette.colors.dynamic.wbGray)
        status = Status(success: palette.colors.dynamic.wbGreen,
                        error: palette.colors.dynamic.wbRed,
                        normal: normalState)
    }
    
}

extension PinView: Themeable {
    
    public typealias Theme = PinViewTheme

    public func apply(theme: Theme) {
        self.theme = theme
    }
    
}



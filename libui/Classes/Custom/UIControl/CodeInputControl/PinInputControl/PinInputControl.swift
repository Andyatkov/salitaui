//
//  PinInputControl.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 14.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

/// Control to input secure text pin code etc.
open class PinInputControl: CodeInputControl {
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        parameters.itemSize = CGSize(width: 20, height: 20)
    }
    
    override open func createNewItem() -> CodeInputItemView {
        return PinView()
    }
}


//
//  CodeInputItem.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 14.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public typealias CodeInputItemView = UIView & CodeInputItem

/// Protocol of custom item that is used by CodeInputControl.
public protocol CodeInputItem: class {
    /// The input item text. In common case that will be one character.
    var text: String? { get set }
    
    /// The property that indicate is control focused and user interacts with it.
    var isOnFocus: Bool { get set }
    
    /// The control status.
    var status: CodeInputControl.Status { get set }
    
    /// The control style.
    var style: CodeInputControl.Style { get set }
}


//
//  WeakBox.swift
//  WBPortal
//
//  Created by Andrey Dyatkov on 01.02.2020.
//  Copyright © 2020 Wildberries OOO. All rights reserved.
//

import Foundation

/// Class to encapsulate weak reference to object.
public class WeakBox<T: AnyObject> {

    /// Weak reference to object.
    public weak var value: T?

    /// Initializer
    ///
    /// - Parameter value: Weak reference to object.
    public init(value: T?) {
        self.value = value
    }

}

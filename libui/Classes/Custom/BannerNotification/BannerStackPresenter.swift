//
//  BannerStackPresenter.swift
//  WBPortal
//
//  Created by Andrey Dyatkov on 01.02.2020.
//  Copyright © 2020 Wildberries OOO. All rights reserved.
//

import UIKit

/// Presenter with stack banner notification logic.
/// All banner view will be presented as stack and will disappear one by one.
public class BannerStackPresenter: BannerPresenterProtocol {
    
    // MARK: - Internal Properties
    
    var viewControllers = [WeakBox<BannerWindowViewController>]()
    
    var lastViewController: BannerWindowViewController? {
        return viewControllers.last?.value
    }
    
    // MARK: - Private Properties
    
    private var displayingTime: TimeInterval {
        return lastViewController?.config.displayingTime ?? 0.0
    }
    
    private let queue = DispatchQueue(label: "com.banner.stack.presenter.queue")
    var lastDispatchItem: DispatchWorkItem?
    
    // MARK: - Public Methods
    
    public func show(banner: UIView, config: BannerNotification.Config) {
        let viewController = BannerWindowViewController(banner: banner, config: config)
        viewController.window?.makeKeyAndVisible()
        
        append(viewController: viewController)
        startTimer()
    }
    
    deinit {
        invalidateTimer()
        viewControllers.forEach { $0.value?.hideBanner() }
    }
    
    private func startTimer() {
        invalidateTimer()
        
        let item = DispatchWorkItem(block: timerAction)
        lastDispatchItem = item
        
        queue.asyncAfter(deadline: .now() + displayingTime, execute: item)
    }
    
    private func timerAction() {
        if viewControllers.count > 0 {
            hideLastBanner()
            removeLastViewController()
            startTimer()
        }
    }
    
    private func invalidateTimer() {
        lastDispatchItem?.cancel()
        lastDispatchItem = nil
    }
    
    private func append(viewController: BannerWindowViewController) {
        lastViewController?.delegate = nil
        
        viewController.delegate = self
        viewControllers.append(WeakBox(value: viewController))
    }
    
    private func removeLastViewController() {
        lastViewController?.delegate = nil
        
        viewControllers.removeLast()
    }
    
    private func hideLastBanner() {
        lastViewController?.hideBanner()
    }
    
}

extension BannerStackPresenter: BannerWindowViewControllerDelegate {

    func startUserInteraction(viewController: BannerWindowViewController) {
        invalidateTimer()
    }
    
    func endUserInteraction(viewController: BannerWindowViewController) {
        removeLastViewController()
        
        if viewControllers.count > 0 {
            startTimer()
        }
    }
    
}


//
//  TextNotificationView.swift
//  WBPortal
//
//  Created by Andrey Dyatkov on 01.02.2020.
//  Copyright © 2020 Wildberries OOO. All rights reserved.
//

import UIKit

/// The custom notification view that will display text message.
public class TextNotificationView: UIView {
    
    // MARK: - Public Properties
    
    /// Notification state.
    ///
    /// - error: error state.
    /// - success: success state.
    public enum State {
        case error
        case success
    }
    
    /// The current text that is displayed by the notification.
    public var text: String? {
        get {
            return label.text
        }
        set {
            label.text = newValue
            setNeedsLayout()
        }
    }
    
    /// The text vertical insets.
    public var verticalInset: CGFloat = 16.0 {
        didSet { setNeedsLayout() }
    }
    
    /// The text horizontal insets.
    public var horizontalInset: CGFloat = 16.0 {
        didSet { setNeedsLayout() }
    }
    
    /// The label that display text message.
    public let label = UILabel()

    /// The notification state.
    public var state: State = .error {
        didSet {
            guard let theme = theme else { return }
            
            apply(theme: theme)
        }
    }
    
    // MARK: Private Properties
    
    private var theme: TextNotificationViewTheme?
    
    // MARK: - Initializers
    
    public convenience init(text: String, state: State) {
        self.init(frame: .zero)
        
        self.text = text
        self.state = state
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        observe(theme: \DefaultTheme.view.banner, on: WBLibThemeManager.wblibuiManager)
        
        addSubview(label)
        
        layer.masksToBounds = true
        layer.cornerRadius = 3.0
        
        label.numberOfLines = 0
        //TODO: - Change font
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.textAlignment = .center
    }
    
    // MARK: - Layout
    
    open override func sizeThatFits(_ size: CGSize) -> CGSize {
        let size = CGSize(
            width: size.width - 2.0 * horizontalInset,
            height: size.height - 2.0 * verticalInset)
        
        return label.sizeThatFits(size)
    }
    
    open override var intrinsicContentSize: CGSize {
        return CGSize(
            width: label.intrinsicContentSize.width + 2.0 * horizontalInset,
            height: label.intrinsicContentSize.height + 2.0 * verticalInset)
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        
        let bounds = CGRect(origin: .zero, size: frame.size)
        
        let width = bounds.width - 2.0 * horizontalInset
        
        label.frame.size.width = width
        label.sizeToFit()
        label.frame.size.width = width
        
        label.frame.origin.x = horizontalInset
        label.frame.origin.y = (bounds.height - label.frame.height) / 2.0
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
    }
    
}

// MARK: - Theme

public class TextNotificationViewTheme: ColorTheme {

    struct BackgroundColor {
        let success: UIColor
        let error: UIColor
    }
    
    let textColor: UIColor
    let backgroundColor: BackgroundColor
    
    public init(palette: Palette) {
        textColor = palette.colors.dynamic.wbWhite
        backgroundColor = BackgroundColor(success: palette.colors.dynamic.wbGreen,
                                      error: palette.colors.dynamic.wbRed)
    }
    
}

extension TextNotificationView: Themeable {
    
    public typealias Theme = TextNotificationViewTheme

    public func apply(theme: Theme) {
        self.theme = theme
        
        label.textColor = theme.textColor
        switch state {
        case .error:
            backgroundColor = theme.backgroundColor.error
        case .success:
            backgroundColor = theme.backgroundColor.success
        }
    }
    
}

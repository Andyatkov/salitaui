//
//  BannerNotification.swift
//  WBPortal
//
//  Created by Andrey Dyatkov on 01.02.2020.
//  Copyright © 2020 Wildberries OOO. All rights reserved.
//

import UIKit

/// Base class to displaying local banner notifications.
public class BannerNotification: BannerPresenterProtocol {
    
    // MARK: - Internal Properties

    /// Banner notification presenter, that encapsulate all presentation logic.
    public let presenter: BannerPresenterProtocol
    
    // MARK: - Public Methods
    
    /// Base initializer.
    ///
    /// - Parameter presenter: Banner notification presenter, that encapsulate all presentation logic.
    public init(presenter: BannerPresenterProtocol) {
        self.presenter = presenter
    }
    
    public func show(banner: UIView, config: Config) {
        presenter.show(banner: banner, config: config)
    }
    
    public static func defaultStackBanner() -> BannerNotification {
        return BannerNotification(presenter: BannerStackPresenter())
    }
    
    public static func defaultSwitchBanner() -> BannerNotification {
        return BannerNotification(presenter: BannerSwitchPresenter())
    }
    
}

extension BannerNotification {
    
    /// Banner presentation configuration
    public struct Config {
        
        /// Banner insets.
        ///
        /// - safeArea: SafeArea mode will calculate insets automatic from view safe area.
        /// - absolute: Absolute mode will calculate manual insets from view absolute coordinate system.
        public enum Insets {
            case safeArea(offset: UIEdgeInsets)
            case absolute(insets: UIEdgeInsets)
        }
        
        /// Banner insets.
        public var insets: Insets
        
        /// Banner maximum width.
        public var maxWidth: CGFloat
        
        /// Banner minimum height.
        public var minHeight: CGFloat
        
        /// Banner displaying time. The time after which the banner will disappear
        public var displayingTime: TimeInterval
        
        
        /// Banner presenting animation duration.
        public var presentingDuration: TimeInterval
        
        /// Banner dismissing animation duration.
        public var dismissingDuration: TimeInterval
        
        public init(insets: Insets,
                    maxWidth: CGFloat,
                    minHeight: CGFloat,
                    displayingTime: TimeInterval,
                    presentingDuration: TimeInterval,
                    dismissingDuration: TimeInterval) {
            self.insets = insets
            self.maxWidth = maxWidth
            self.minHeight = minHeight
            
            self.displayingTime = displayingTime
            self.presentingDuration = presentingDuration
            self.dismissingDuration = dismissingDuration
        }
        
        /// Default configuration.
        public static let `default` = Config(
            insets: .safeArea(offset: UIEdgeInsets(top: 4.0, left: 8, bottom: 0, right: 8)),
            maxWidth: .infinity,
            minHeight: 48.0,
            displayingTime: 2.0,
            presentingDuration: Durations.Default.single,
            dismissingDuration: Durations.Default.single)
        
    }
    
}


//
//  BannerPresenterProtocol.swift
//  WBPortal
//
//  Created by Andrey Dyatkov on 01.02.2020.
//  Copyright © 2020 Wildberries OOO. All rights reserved.
//

import UIKit

public protocol BannerPresenterProtocol {
    /// Show notification
    ///
    /// - Parameters:
    ///   - banner: View to present.
    ///   - config: Banner presentation configuration.
    func show(banner: UIView, config: BannerNotification.Config)
}


//
//  ContainView.swift
//  WBLibui
//
//  Created by Юрий Андрюшин on 02.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

class ContainView: UIView {
    
    weak var parrentViewController: UIViewController?
    private(set) var currentController: UIViewController?
    
    func setViewController(controller: UIViewController) {
        
        guard
            let parrentViewController = parrentViewController,
            controller.view != currentController?.view
            else { return }
        
        currentController?.willMove(toParent: nil)
        controller.willMove(toParent: parrentViewController)
        parrentViewController.addChild(controller)
        currentController?.view.removeFromSuperview()
        currentController?.didMove(toParent: nil)
        
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(controller.view)
        
        NSLayoutConstraint.activate([
            controller.view.leadingAnchor.constraint(equalTo: leadingAnchor),
            controller.view.trailingAnchor.constraint(equalTo: trailingAnchor),
            controller.view.topAnchor.constraint(equalTo: topAnchor),
            controller.view.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
        layoutIfNeeded()
        
        currentController?.removeFromParent()
        controller.didMove(toParent: parrentViewController)
        
        currentController = controller
    }
    
}

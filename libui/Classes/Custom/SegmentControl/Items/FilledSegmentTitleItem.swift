//
//  FilledSegmentTitleItem.swift
//  WBLibui
//
//  Created by Юрий Андрюшин on 31.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

// MARK: - ViewModel

public struct FilledSegmentTitleItemViewModel {
    
    let text: String?
    let delegate: SegmentTitleItemOutput
    
}

/// Segment item
open class FilledSegmentTitleItem: UIView, SegmentTitleItemInput {
    
    // MARK: - Public Properties
    
    /// item is active: Bool value
    public var isActive: Bool = false {
        didSet {
            guard let theme = theme else { return }
            
            apply(theme: theme)
        }
    }
    
    // MARK: - Private Properties
    
    private let button = UIButton()
    private var text: String?
    private weak var delegate: SegmentTitleItemOutput?
    private var theme: FilledSegmentTitleItemTheme?
    
    // MARK: - Initializers
    
    public init(viewModel: FilledSegmentTitleItemViewModel) {
        super.init(frame: .zero)
        
        self.text = viewModel.text
        self.delegate = viewModel.delegate
        
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private Methods
    
    private func setup() {
        
        observe(theme: \DefaultTheme.view.segmentControlTheme.filledSegmentTitleItem,
                on: WBLibThemeManager.wblibuiManager)
        
        layer.cornerRadius = .defaultCornerRadius
        layer.borderWidth = 1
        clipsToBounds = true
        
        setupButton()
        addConstraints()
    }
    
    private func setupButton() {
        
        addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(text, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 16, weight: .regular)
        button.addTarget(self, action: #selector(emptyViewButtonDidSelect), for: .touchUpInside)
    }
    
    private func addConstraints() {
        
        NSLayoutConstraint.activate([
        
            button.topAnchor.constraint(equalTo: topAnchor),
            button.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            button.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            button.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    // MARK: - Handlers
    
    @objc private func emptyViewButtonDidSelect() {
        
        delegate?.buttonDidSelect(index: tag)
    }
    
}

// MARK: - Theme

public class FilledSegmentTitleItemTheme: ColorTheme {
    
    let borderColorActive: UIColor
    let borderColorNotActive: UIColor
    let titleColorActive: UIColor
    let titleColorNotActive: UIColor
    let backgroundColorActive: UIColor
    let backgroundColorNotActive: UIColor
    let buttonBackgroundColor: UIColor

    public init(palette: Palette) {
        
        borderColorActive = palette.colors.dynamic.wbGray
        borderColorNotActive = palette.colors.dynamic.wbClear
        titleColorActive = palette.colors.dynamic.wbWhite
        titleColorNotActive = palette.colors.dynamic.wbBlack
        backgroundColorActive = palette.colors.dynamic.wbBrand
        backgroundColorNotActive = palette.colors.dynamic.wbWhite
        buttonBackgroundColor = palette.colors.dynamic.wbClear
    }
    
}

extension FilledSegmentTitleItem: Themeable {
    
    public typealias Theme = FilledSegmentTitleItemTheme

    public func apply(theme: Theme) {
        
        self.theme = theme
        
        backgroundColor = isActive ? theme.backgroundColorActive : theme.borderColorNotActive
        button.setTitleColor(isActive ? theme.titleColorActive : theme.titleColorNotActive, for: .normal)
        layer.borderColor = isActive ? theme.borderColorActive.cgColor : theme.backgroundColorNotActive.cgColor
        button.backgroundColor = theme.buttonBackgroundColor
    }
    
}

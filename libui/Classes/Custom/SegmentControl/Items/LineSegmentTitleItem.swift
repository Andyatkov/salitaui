//
//  LineSegmentTitleItem.swift
//  WBLibui
//
//  Created by Юрий Андрюшин on 31.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

// MARK: - ViewModel

public struct LineSegmentTitleItemViewModel {
    
    let text: String?
    let delegate: SegmentTitleItemOutput
    
}

/// Segment item
open class LineSegmentTitleItem: UIView, SegmentTitleItemInput {

    // MARK: - Public Properties
    
    /// item is active: Bool value
    public var isActive: Bool = false {
        didSet {
            lineView.isHidden = !isActive
            
            guard let theme = theme else { return }
            
            apply(theme: theme)
        }
    }
    
    // MARK: - Private Properties
    
    private let button = UIButton()
    private var text: String?
    private let lineView = UIView()
    private var theme: LineSegmentTitleItemTheme?
    
    private weak var delegate: SegmentTitleItemOutput?
    
    // MARK: - Initializers
    
    public init(viewModel: LineSegmentTitleItemViewModel) {
        super.init(frame: .zero)
        
        self.text = viewModel.text
        self.delegate = viewModel.delegate
        
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private Methods
    
    private func setup() {
        
        observe(theme: \DefaultTheme.view.segmentControlTheme.lineSegmentTitleItem,
                on: WBLibThemeManager.wblibuiManager)
        
        addSubviews()
        setupView()
        setupButton()
        addConstrants()
    }
    
    private func addSubviews() {
        
        [button,
         lineView].forEach({
            
            addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        })
    }
    
    private func setupView() {
        
        clipsToBounds = true
    }
    
    private func setupButton() {
        
        button.setTitle(text, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 16, weight: .regular)
        button.addTarget(self, action: #selector(emptyViewButtonDidSelect), for: .touchUpInside)
    }
    
    private func addConstrants() {
        
        NSLayoutConstraint.activate([
        
            button.topAnchor.constraint(equalTo: topAnchor),
            button.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            button.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            button.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            lineView.topAnchor.constraint(equalTo: button.centerYAnchor, constant: 28),
            lineView.heightAnchor.constraint(equalToConstant: 2),
            lineView.bottomAnchor.constraint(equalTo: bottomAnchor),
            lineView.leadingAnchor.constraint(equalTo: leadingAnchor),
            lineView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
    
    // MARK: - Handlers
    
    @objc private func emptyViewButtonDidSelect() {
        
        delegate?.buttonDidSelect(index: tag)
    }
    
}

// MARK: - Themeable

extension LineSegmentTitleItem: Themeable {
    
    public typealias Theme = LineSegmentTitleItemTheme

    public func apply(theme: Theme) {
        
        self.theme = theme
        
        button.setTitleColor(isActive ? theme.buttonBackgroundActive : theme.buttonBackgroundNotActive, for: .normal)
        backgroundColor = theme.backgroundColor
        lineView.backgroundColor = theme.lineViewBackgroundColor
        button.backgroundColor = theme.buttonBackgroundColor
    }
    
}

// MARK: - ColorTheme

public struct LineSegmentTitleItemTheme: ColorTheme {
    
    let backgroundColor: UIColor
    let buttonBackgroundActive: UIColor
    let buttonBackgroundNotActive: UIColor
    let lineViewBackgroundColor: UIColor
    let buttonBackgroundColor: UIColor
    
    public init(palette: Palette) {
        
        buttonBackgroundActive = palette.colors.dynamic.wbAccent
        buttonBackgroundNotActive = palette.colors.dynamic.wbDarkGray
        backgroundColor = palette.colors.dynamic.wbWhite
        lineViewBackgroundColor = palette.colors.dynamic.wbAccent
        buttonBackgroundColor = .clear
    }
    
}

//
//  SegmentTitleItemBasicProtocols.swift
//  WBLibui
//
//  Created by Юрий Андрюшин on 31.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public protocol SegmentTitleItemInput: class {
    var isActive: Bool { get set }
    var tag: Int { get set }
    var frame: CGRect { get set }
}

public protocol SegmentTitleItemOutput: class {
    func buttonDidSelect(index: Int)
}

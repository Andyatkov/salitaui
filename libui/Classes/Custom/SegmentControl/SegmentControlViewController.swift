//
//  SegmentControlViewController.swift
//  WBLibui
//
//  Created by Юрий Андрюшин on 31.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

// MARK: - Protocols

public protocol SegmentControlProtocol: class {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    func scrollViewDidScroll(_ scrollView: UIScrollView)
}

// MARK: - ViewModel

public struct SegmentControlVCViewModel {
    
    var segmentItemType: SegmentControlViewController.SegmentItemTypes
    var isHiddenSeparator: Bool
    
    public init(segmentItemType: SegmentControlViewController.SegmentItemTypes,
                isHiddenSeparator: Bool) {
        
        self.segmentItemType = segmentItemType
        self.isHiddenSeparator = isHiddenSeparator
    }
    
}

// MARK: - Private extensions

private extension CGFloat {
    
    static let scrollViewHeight: CGFloat = 60
    static let stackViewSpacing: CGFloat = 6
    
}

private extension UIEdgeInsets {
    
    static let scrollViewContentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    
}

// MARK: - PageItemTypes

extension SegmentControlViewController {
    
    public enum SegmentItemTypes {
        
        case filled
        case line
    }
    
}

open class SegmentControlViewController: UIViewController {
    
    // MARK: - Private Properties
    
    private let scrollView = UIScrollView()
    private let stackView = UIStackView()
    private var pageVC: UIPageViewController!
    private var segmentTitleItems = [SegmentTitleItemInput]()
    private var childControllers = [UIViewController]()
    private var lastContentOffsets = [CGFloat]()
    private let containView = ContainView()
    private var currentIndex = 0
    private var scrollViewTopConstraint = NSLayoutConstraint()
    private var isHiddenBar = false
    private var segmentItemType: SegmentItemTypes = .filled
    private let separatorView = SeparatorView()
    
    // MARK: - Initializers
    
    public init(viewModel: SegmentControlVCViewModel) {
        super.init(nibName: nil, bundle: nil)
        
        self.segmentItemType = viewModel.segmentItemType
        separatorView.isHidden = viewModel.isHiddenSeparator
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life cycle
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        view.layoutIfNeeded()
        
        let scrollViewContentWidth = scrollView.contentSize.width
        let scrollWidth = scrollView.frame.size.width
        
        if scrollViewContentWidth < scrollWidth {
            
            let centerOffsetX = (scrollViewContentWidth - scrollWidth) / 2
            scrollView.setContentOffset(CGPoint(x: centerOffsetX, y: 0), animated: false)
        }
    }
    
    // MARK: - Public Methods
    
    public func set(_ controllers: [UIViewController]) {
        
        childControllers = controllers
        lastContentOffsets = Array(repeating: 0, count: childControllers.count)
    }
    
    // MARK: - Private Methods
    
    private func setup() {

        setupPageVC()
        addSubviews()
        setupStackView()
        setupScrollView()
        setupPageVC()
        addConstraints()
    }
    
    private func addSubviews() {
        
        [scrollView,
         containView,
         separatorView].forEach({
            
            view.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        })
        
        scrollView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = segmentItemType == .filled ? .fillProportionally : .fillEqually
        stackView.axis = .horizontal
        stackView.spacing = .stackViewSpacing
    }
    
    private func setupPageVC() {
        
        pageVC = UIPageViewController(transitionStyle: .scroll,
                                      navigationOrientation: .horizontal,
                                      options: nil)
        pageVC.delegate = self
        pageVC.dataSource = self
        
        containView.parrentViewController = self
        containView.setViewController(controller: pageVC)
        setVC(index: currentIndex)
    }
    
    private func setupScrollView() {
        
        scrollView.alwaysBounceHorizontal = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.alwaysBounceVertical = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentInset = .scrollViewContentInset
    }
    
    private func setupStackView() {
        
        for (index, value) in childControllers.enumerated() {
            
            switch segmentItemType {
            case .filled:
                let item = FilledSegmentTitleItem(viewModel: FilledSegmentTitleItemViewModel(text: value.title, delegate: self))
                item.tag = index
                segmentTitleItems.append(item)
                stackView.addArrangedSubview(item)
                
            case .line:
                let item = LineSegmentTitleItem(viewModel: LineSegmentTitleItemViewModel(text: value.title, delegate: self))
                item.tag = index
                segmentTitleItems.append(item)
                stackView.addArrangedSubview(item)
            }
        }
    }
    
    private func addConstraints() {
        
        scrollViewTopConstraint = scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor)
        
        NSLayoutConstraint.activate([
        
            scrollViewTopConstraint,
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.heightAnchor.constraint(equalToConstant: .scrollViewHeight),
            
            separatorView.topAnchor.constraint(equalTo: scrollView.bottomAnchor),
            separatorView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: 0.5),
            
            stackView.centerYAnchor.constraint(equalTo: scrollView.centerYAnchor),
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            
            containView.topAnchor.constraint(equalTo: scrollView.bottomAnchor),
            containView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            containView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            containView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    private func setVC(index: Int) {
        
        itemsUpdateIfNeeded(index: index)
        
        pageVC.setViewControllers([childControllers[index]],
                                  direction: index > currentIndex ? .forward : .reverse,
                                  animated: false,
                                  completion: nil)
        currentIndex = index
    }
    
    private func itemsUpdateIfNeeded(index: Int) {
        
        guard segmentTitleItems.indices.contains(index) else { return }
        
        scrollView.scrollRectToVisible(segmentTitleItems[index].frame, animated: true)
        
        for (indexItem, value) in segmentTitleItems.enumerated() {
            
            value.isActive = indexItem == index
        }
    }
    
    // MARK: - Animations
    
    private func hideBar() {
        
        guard !isHiddenBar else { return }
        
        isHiddenBar = true
        view.layoutIfNeeded()
        
        UIView.animate(withDuration: Durations.Default.single, animations: { [weak self] in
            
            guard let `self` = self else { return }

            self.scrollViewTopConstraint.constant = -self.scrollView.frame.height
            self.view.layoutIfNeeded()
        })
    }
    
    private func showBar() {
        
        guard isHiddenBar else { return }
        
        isHiddenBar = false
        view.layoutIfNeeded()
        
        UIView.animate(withDuration: Durations.Default.single, animations: { [weak self] in
            
            guard let `self` = self else { return }

            self.scrollViewTopConstraint.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
}

// MARK: - PageTitleItemProtocol

extension SegmentControlViewController: SegmentTitleItemOutput {
    
    public func buttonDidSelect(index: Int) {
        
        setVC(index: index)
    }
    
}

// MARK: - PageView protocols

extension SegmentControlViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    public func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard
            let index = childControllers.firstIndex(of: viewController),
            index != 0,
            index != NSNotFound
            else { return nil }
                
        return childControllers[index - 1]
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard
            let index = childControllers.firstIndex(of: viewController),
            index != NSNotFound,
            index + 1 < childControllers.count
            else { return nil }
        
        return childControllers[index + 1]
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController,
                                   didFinishAnimating finished: Bool, previousViewControllers: [UIViewController],
                                   transitionCompleted completed: Bool) {
        
        guard
            let viewController: UIViewController = pageViewController.viewControllers?.last,
            let index = childControllers.firstIndex(of: viewController),
            completed
            else { return }
        
        currentIndex = index
        itemsUpdateIfNeeded(index: index)
    }
    
}

// MARK: - PageMenuProtocol

extension SegmentControlViewController: SegmentControlProtocol {
    
    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        lastContentOffsets[currentIndex] = scrollView.contentOffset.y
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0 {
            showBar()
        } else {
            hideBar()
        }
    }
    
}

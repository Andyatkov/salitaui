//
//  TextField.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 16.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

/// Simple TextField
public class TextField: UIView {

    // MARK: - Public Properties
    
    open weak var delegate: UITextFieldDelegate? {
        didSet { textField.delegate = delegate }
    }
    
    /// The text displayed by the text field.
    open var text: String? {
        get {
            return textField.text
        }
        set {
            textField.text = newValue
            setNeedsLayout()
        }
    }
    
    /// The string that is displayed when there is no other text in the text field.
    open var placeholder: String? {
        get {
            return textField.attributedPlaceholder?.string
        }
        set {
            textField.attributedPlaceholder = NSAttributedString(string: newValue ?? "", attributes: [:])
        }
    }
    
    /// The textContentType property is to provide the keyboard with extra information about the semantic intent of the text document.
    open var textContentType: UITextContentType {
        get { return textField.textContentType }
        set { textField.textContentType = newValue }
    }
    
    /// Text Alignment of keyboard.
    open var textAlignment: NSTextAlignment {
        get { return textField.textAlignment }
        set { textField.textAlignment = newValue }
    }
    
    /// The font of the text.
    open var font: UIFont? {
        get {
            return textField.font
        }
        set {
            textField.font = newValue
            setNeedsLayout()
        }
    }
    
    /// Keyboard type.
    open var keyboardType: UIKeyboardType {
        get { return textField.keyboardType }
        set { textField.keyboardType = newValue }
    }
    
    /// Set and get text color.
    open var textColor: UIColor? {
        get { return textField.textColor }
        set { textField.textColor = newValue }
    }
    
    /// Set and get textField tint color.
    open var textFieldTintColor: UIColor? {
        get { return textField.tintColor }
        set { textField.tintColor = newValue }
    }
    
    /// Set and get textfield return key type.
    open var returnKeyType: UIReturnKeyType  {
        get { return textField.returnKeyType}
        set { textField.returnKeyType = newValue}
    }

    public let textField = UITextField()
    
    // MARK: - Internal Properties

    var parameters = Layout.Parameters()
    
    // MARK: - Initializers
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    @discardableResult
    public override func becomeFirstResponder() -> Bool {
        super.becomeFirstResponder()
        return textField.becomeFirstResponder()
    }
    
    // MARK: - Private methods
    
    private func commonInit() {
        observe(theme: \DefaultTheme.textfield.textField, on: WBLibThemeManager.wblibuiManager)
        
        addGestures()
        
        layer.cornerRadius = 4.0
        layer.masksToBounds = true
        
        textField.borderStyle = .none
        //TODO: - Change Font
        textField.font = UIFont.systemFont(ofSize: 16)
        addSubview(textField)
    }
    
    private func addGestures() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(becomeFirstResponder))
        addGestureRecognizer(tap)
    }
    
    @objc func clearText() {
        let range = NSRange(location: 0, length: textField.text?.count ?? 0)
        
        if textField.delegate?.textField?(textField, shouldChangeCharactersIn: range, replacementString: "") ?? true {
            textField.text = ""
        }
    }
    
    // MARK: - Layout
    
    public override var intrinsicContentSize: CGSize {
        return sizeThatFits(frame.size)
    }
    
    public override func sizeThatFits(_ size: CGSize) -> CGSize {
        let params = parameters
        let tfSize = textField.sizeThatFits(size)
        
        let width = params.textfield.leftSpacing +
            tfSize.width +
            params.spacing.left +
            params.spacing.right
        
        let height = tfSize.height + 2.0 * params.textfield.topSpacing
        
        return CGSize(width: width, height: height)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        layoutViews()
    }
    
}

extension TextField {

    struct Layout {
        struct Parameters {

            struct TextField {
                var topSpacing: CGFloat = 4.0
                var bottomSpacing: CGFloat = 4.0
                var leftSpacing: CGFloat = 16.0
                var rightSpacing: CGFloat = 16.0
            }
            
            struct Spacing {
                var left: CGFloat = 8.0
                var right: CGFloat = 8.0
            }
            
            var textfield = TextField()
            var spacing = Spacing()
        }
        
        struct Frames {
            let textField: CGRect
        }
    }
    
    func layoutViews() {
        
        textField.frame.size.width = bounds.width
            - parameters.textfield.leftSpacing
            - parameters.textfield.rightSpacing
        textField.frame.size.height = bounds.height
            - parameters.textfield.topSpacing
            - parameters.textfield.bottomSpacing
        
        textField.frame.alignmentX(to: bounds,
                                   fromAnchor: .left,
                                   toAnchor: .left,
                                   offset: parameters.textfield.leftSpacing)
        textField.frame.alignmentY(to: bounds,
                                   fromAnchor: .top,
                                   toAnchor: .top,
                                   offset: parameters.textfield.topSpacing)
    }

}

// MARK: - Theme

public class TextFieldTheme: ColorTheme {
    
    struct TextField {
        let textColor: UIColor
        let tintColor: UIColor
    }
    
    struct Background {
        let color: UIColor
    }
    
    let textField: TextField
    let background: Background

    public init(palette: Palette) {
        textField = TextField(textColor: palette.colors.dynamic.wbBlack,
                              tintColor: palette.colors.dynamic.wbBrand)
        background = Background(color: palette.colors.dynamic.wbGray)
    }
    
}

extension TextField: Themeable {
    
    public typealias Theme = TextFieldTheme

    public func apply(theme: Theme) {
        tintColor = theme.textField.tintColor
        backgroundColor = theme.background.color
    }
    
}

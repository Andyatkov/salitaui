//
//  RegionTextField.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 14.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit
import PhoneNumberKit

public protocol RegionTextFieldDelegate: class {
    func tapRegion()
    func change(_ phone: String?, isValid: Bool)
}

/// Simple TextField
public class RegionTextField: UIView {

    // MARK: - Public Properties
    
    open weak var delegate: RegionTextFieldDelegate?
    
    /// The text displayed by the text field.
    open var text: String? {
        get {
            return phoneTextField.text
        }
        set {
            phoneTextField.text = newValue
            setNeedsLayout()
        }
    }
    
    /// The string that is displayed when there is no other text in the text field.
    open var placeholder: String? {
        get {
            return phoneTextField.attributedPlaceholder?.string
        }
        set {
            phoneTextField.attributedPlaceholder = NSAttributedString(string: newValue ?? "", attributes: [:])
        }
    }
    
    /// The textContentType property is to provide the keyboard with extra information about the semantic intent of the text document.
    open var textContentType: UITextContentType {
        get { return phoneTextField.textContentType }
        set { phoneTextField.textContentType = newValue }
    }
    
    /// Text Alignment of keyboard.
    open var textAlignment: NSTextAlignment {
        get { return phoneTextField.textAlignment }
        set { phoneTextField.textAlignment = newValue }
    }
    
    /// The font of the text.
    open var font: UIFont? {
        get {
            return phoneTextField.font
        }
        set {
            phoneTextField.font = newValue
            setNeedsLayout()
        }
    }
    
    /// Keyboard type.
    open var keyboardType: UIKeyboardType {
        get { return phoneTextField.keyboardType }
        set { phoneTextField.keyboardType = newValue }
    }
    
    /// Set and get text color.
    open var textColor: UIColor? {
        get { return phoneTextField.textColor }
        set { phoneTextField.textColor = newValue }
    }
    
    /// Set and get textField tint color.
    open var textFieldTintColor: UIColor? {
        get { return phoneTextField.tintColor }
        set { phoneTextField.tintColor = newValue }
    }
    
    /// Set and get textfield return key type.
    open var returnKeyType: UIReturnKeyType  {
        get { return phoneTextField.returnKeyType}
        set { phoneTextField.returnKeyType = newValue}
    }
    
    /// Set and get region image.
    open var regionImage: UIImage? {
        get { return regionImageView.image }
        set { regionImageView.image = newValue }
    }
    
    /// Set current region code.
    public var currentRegion: String = "" {
        didSet {
            regionLabel.text = currentRegion
        }
    }
    
    public var autoreplaceRussianCountryCode: Bool = false
    
    /// Return current phone number.
    public var phone: PhoneNumber? {
        guard let text = phoneTextField.text else { return nil }
        
        var formattedNumber = partialFormatter.formatPartial(text)
        formattedNumber = removePlusIfNeeded(string: formattedNumber)
        return try? phoneNumberKit.parse(currentRegion + formattedNumber)
    }
    
    /// Check text in current text field and return true if number is correct.
    public var isValidNumber: Bool {
        return phone?.numberString == nil ? false : true
    }
    
    /// Phone number kit.
    public var phoneNumberKit = PhoneNumberKit()
    
    // MARK: - Internal Properties
    
    let phoneTextField = UITextField()
    
    /// Return a user’s default region code.
    var defaultRegion = PhoneNumberKit.defaultRegionCode() {
        didSet {
            partialFormatter.defaultRegion = defaultRegion
        }
    }
    
    var finalPhone: String? {
        if let phone = phone {
            return currentRegion + phone.adjustedNationalNumber()
        }
        
        return nil
    }
    
    struct CursorPosition {
        let numberAfterCursor: String
        let repetitionCountFromEnd: Int
    }

    lazy var partialFormatter: PartialFormatter = {
        return PartialFormatter(phoneNumberKit: phoneNumberKit, defaultRegion: defaultRegion, withPrefix: false)
    }()

    let nonNumericSet: NSCharacterSet = {
        var mutableSet = NSMutableCharacterSet.decimalDigit().inverted
        mutableSet.remove(charactersIn: "+＋")
        return mutableSet as NSCharacterSet
    }()
    
    let regionLabel = UILabel()
    let regionImageView = UIImageView()

    var parameters = Layout.Parameters()
    
     /// Color theme.
    var theme: RegionTextFieldTheme?
    
    // MARK: = Private Properties
    
    private let regionView = UIView()
    private let regionArrowImageView = UIImageView()
    private let regionSeparatorView = UIImageView()
    
    // MARK: - Initializers
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    @discardableResult
    public override func becomeFirstResponder() -> Bool {
        super.becomeFirstResponder()
        return phoneTextField.becomeFirstResponder()
    }
    
    // MARK: - Private methods
    
    private func commonInit() {
        observe(theme: \DefaultTheme.textfield.regionTextFieldTheme, on: WBLibThemeManager.wblibuiManager)
        
        setView()
        addSubviews()
        addGestures()
        setImageViews()
        setRegionLabel()
        setupTextField()
    }
    
    private func setupTextField() {
        
        phoneTextField.delegate = self
    }
    
    private func addSubviews() {
        
        addSubview(regionView)
        addSubview(phoneTextField)
        
        regionView.addSubview(regionImageView)
        regionView.addSubview(regionLabel)
        regionView.addSubview(regionArrowImageView)
        regionView.addSubview(regionSeparatorView)
    }
    
    private func setView() {
        
        layer.cornerRadius = 4.0
        layer.masksToBounds = true
    }
    
    private func addGestures() {
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapRegionView))
        regionView.addGestureRecognizer(tap)
    }
    
    private func setRegionLabel() {
        
        regionLabel.textAlignment = .center
        regionLabel.font = UIFont.systemFont(ofSize: 17.0, weight: .regular)
    }
    
    private func setImageViews() {
        
        regionImageView.contentMode = .scaleAspectFit
        
        regionArrowImageView.contentMode = .scaleAspectFit
        regionArrowImageView.image = Asset.Textfield.icFillDownArrow.image
    }
    
    private func setPhoneTextField() {
        
        phoneTextField.borderStyle = .none
        phoneTextField.font = UIFont.systemFont(ofSize: 16)
    }
    
    private func updateBorderColor() {
        if !phoneTextField.isFirstResponder {
            layer.borderWidth = 0
            layer.borderColor = nil
        } else {
            layer.borderWidth = 1
            layer.borderColor = theme?.borderColor.cgColor
        }
    }
    
    @objc private func tapRegionView() {
        delegate?.tapRegion()
    }
    
    // MARK: - Layout
    
    public override var intrinsicContentSize: CGSize {
        return frame.size
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        layoutViews()
    }
    
}

extension RegionTextField {

    struct Layout {
        struct Parameters {

            struct TextField {
                var topSpacing: CGFloat = 4.0
                var bottomSpacing: CGFloat = 4.0
                var leftSpacing: CGFloat = 16.0
                var rightSpacing: CGFloat = 16.0
            }
            
            struct RegionView {
                var width: CGFloat = 100.0
            }
            
            struct RegionImageView {
                var size: CGSize = CGSize(width: 22.0, height: 22.0)
                var leftSpacing: CGFloat = 12.0
             }
            
            struct RegionLabel {
                var size: CGSize = CGSize(width: 60.0, height: 22.0)
                var leftSpacing: CGFloat = 4.0
                var rightSpacing: CGFloat = 4.0
             }
            
            struct ArrowImageView {
                var size: CGSize = CGSize(width: 14.0, height: 14.0)
                var leftSpacing: CGFloat = 8.0
                var rightSpacing: CGFloat = 8.0
            }
            
            struct RegionSeparatorView {
                var width: CGFloat = 1.0
            }
            
            var textfield = TextField()
            var regionView = RegionView()
            var regionImageView = RegionImageView()
            var regionLabel = RegionLabel()
            var arrowImageView = ArrowImageView()
            var reegionSeparatorView = RegionSeparatorView()
        }
        
        struct Frames {
            let textField: CGRect
        }
    }
    
    func layoutViews() {
        
        // Region View
        
        regionView.frame.size.width = parameters.regionView.width
        regionView.frame.size.height = bounds.height
        
        // Region Image View
        
        regionImageView.frame.size = parameters.regionImageView.size
        
        regionImageView.frame.alignmentX(to: regionView.frame,
                                         fromAnchor: .left,
                                         toAnchor: .left,
                                         offset: parameters.regionImageView.leftSpacing)
        regionImageView.frame.alignmentY(to: regionView.frame,
                                         fromAnchor: .center,
                                         toAnchor: .center)
        
        // Region Label
        
        regionLabel.frame.size = parameters.regionLabel.size
        
        regionLabel.frame.alignmentX(to: regionView.frame,
                                     fromAnchor: .center,
                                     toAnchor: .center,
                                     offset: parameters.regionLabel.leftSpacing)
        regionLabel.frame.alignmentY(to: regionView.frame,
                                     fromAnchor: .center,
                                     toAnchor: .center)
        
        // Arrow Image View
        
        regionArrowImageView.frame.size = parameters.arrowImageView.size
        
        regionArrowImageView.frame.alignmentX(to: regionView.frame,
                                              fromAnchor: .right,
                                              toAnchor: .right,
                                              offset: -parameters.arrowImageView.rightSpacing)
        regionArrowImageView.frame.alignmentY(to: regionView.bounds,
                                              fromAnchor: .center,
                                              toAnchor: .center)
        
        // Separator View
        
        regionSeparatorView.frame.size = CGSize(width: parameters.reegionSeparatorView.width,
                                                height: regionView.frame.height)
        
        regionSeparatorView.frame.alignmentX(to: regionView.frame,
                                             fromAnchor: .right,
                                             toAnchor: .right)
        regionSeparatorView.frame.alignmentY(to: regionView.frame,
                                             fromAnchor: .top,
                                             toAnchor: .top)
        
        // Text Field
        
        phoneTextField.frame.size.width = bounds.width
            - parameters.regionView.width
            - parameters.textfield.leftSpacing
            - parameters.textfield.rightSpacing
        phoneTextField.frame.size.height = bounds.height
        
        phoneTextField.frame.alignmentX(to: regionView.frame,
                                        fromAnchor: .left,
                                        toAnchor: .right,
                                        offset: parameters.textfield.leftSpacing)
        phoneTextField.frame.alignmentY(to: bounds,
                                        fromAnchor: .top,
                                        toAnchor: .top)
    }

}

// MARK: - UITextFieldDelegate

extension RegionTextField: UITextFieldDelegate {
    
    @objc public func textFieldDidBeginEditing(_ textField: UITextField) {
        updateBorderColor()
    }
    
    @objc public func textFieldDidEndEditing(_ textField: UITextField) {
        updateBorderColor()
    }
    
    @objc public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // This allows for the case when a user autocompletes a phone number:
        if range == NSRange(location: 0, length: 0) && string == " " {
            return true
        }

        guard let text = text else {
            return false
        }

        let textAsNSString = text as NSString
        let changedRange = textAsNSString.substring(with: range) as NSString
        let modifiedTextField = textAsNSString.replacingCharacters(in: range, with: string)

        let filteredCharacters = modifiedTextField.filter {
            return String($0).rangeOfCharacter(from: nonNumericSet as CharacterSet) == nil
        }

        let rawNumberString = String(filteredCharacters)

        var formattedNationalNumber = partialFormatter.formatPartial(rawNumberString)
        var selectedTextRange: NSRange?
        
        formattedNationalNumber = removePlusIfNeeded(string: formattedNationalNumber)

        let nonNumericRange = (changedRange.rangeOfCharacter(from: nonNumericSet as CharacterSet).location != NSNotFound)
        
        if range.length == 1 && string.isEmpty && nonNumericRange {
            selectedTextRange = selectionRangeForNumberReplacement(textField: textField, formattedText: modifiedTextField)
            textField.text = modifiedTextField
        } else {
            selectedTextRange = selectionRangeForNumberReplacement(textField: textField, formattedText: formattedNationalNumber)
            textField.text = formattedNationalNumber
        }

        if let selectedTextRange = selectedTextRange,
            let selectionRangePosition = textField.position(from: textField.beginningOfDocument, offset: selectedTextRange.location) {
            let selectionRange = textField.textRange(from: selectionRangePosition, to: selectionRangePosition)
            
            textField.selectedTextRange = selectionRange
        }
        
        delegate?.change(finalPhone, isValid: isValidNumber)
        
        return false
    }
    
    // MARK: - Private Methods
    
    private func extractCursorPosition() -> CursorPosition? {
        var repetitionCountFromEnd = 0
        
        // Check that there is text in the UITextField
        guard let text = text, let selectedTextRange = phoneTextField.selectedTextRange else {
            return nil
        }
        
        let textAsNSString = text as NSString
        let cursorEnd = phoneTextField.offset(from: phoneTextField.beginningOfDocument, to: selectedTextRange.end)
        
        // Look for the next valid number after the cursor, when found return a CursorPosition struct
        for i in cursorEnd ..< textAsNSString.length {
            let cursorRange = NSRange(location: i, length: 1)
            let candidateNumberAfterCursor: NSString = textAsNSString.substring(with: cursorRange) as NSString
            
            if candidateNumberAfterCursor.rangeOfCharacter(from: nonNumericSet as CharacterSet).location == NSNotFound {
                
                for j in cursorRange.location ..< textAsNSString.length {
                    
                    let candidateCharacter = textAsNSString.substring(with: NSRange(location: j, length: 1))
                    if candidateCharacter == candidateNumberAfterCursor as String {
                        repetitionCountFromEnd += 1
                    }
                }
                
                return CursorPosition(numberAfterCursor: candidateNumberAfterCursor as String, repetitionCountFromEnd: repetitionCountFromEnd)
            }
        }
        
        return nil
    }
    
    // Finds position of previous cursor in new formatted text
    private func selectionRangeForNumberReplacement(textField: UITextField, formattedText: String) -> NSRange? {
        let textAsNSString = formattedText as NSString
        var countFromEnd = 0
        
        guard let cursorPosition = extractCursorPosition() else {
            return nil
        }
        
        for i in stride(from: (textAsNSString.length - 1), through: 0, by: -1) {
            let candidateRange = NSRange(location: i, length: 1)
            let candidateCharacter = textAsNSString.substring(with: candidateRange)
            
            if candidateCharacter == cursorPosition.numberAfterCursor {
                countFromEnd += 1
                
                if countFromEnd == cursorPosition.repetitionCountFromEnd {
                    return candidateRange
                }
            }
        }
        
        return nil
    }
    
    private func removePlusIfNeeded(string: String) -> String {
        if let index = string.firstIndex(where: { $0 == "+" }) {
            var text = string
            text.remove(at: index)
            return text
        } else {
            return string
        }
    }
    
}

// MARK: - Theme

public class RegionTextFieldTheme: ColorTheme {
    
    struct TextField {
        let textColor: UIColor
        let tintColor: UIColor
    }
    
    struct Background {
        let color: UIColor
    }
    
    let textField: TextField
    let background: Background
    
    let regionLabelColor: UIColor
    let regionSeparatorColor: UIColor
    
    let borderColor: UIColor

    public init(palette: Palette) {
        textField = TextField(textColor: palette.colors.dynamic.wbBlack,
                              tintColor: palette.colors.dynamic.wbBrand)
        background = Background(color: palette.colors.dynamic.wbGray)
        
        regionLabelColor = palette.colors.dynamic.wbBlack
        regionSeparatorColor = palette.colors.dynamic.wbDarkGray
        
        borderColor = palette.colors.dynamic.wbBrand
    }
    
}

extension RegionTextField: Themeable {
    
    public typealias Theme = RegionTextFieldTheme

    public func apply(theme: Theme) {
        self.theme = theme
        
        tintColor = theme.textField.tintColor
        backgroundColor = theme.background.color
        
        regionLabel.textColor = theme.regionLabelColor
        regionSeparatorView.backgroundColor = theme.regionSeparatorColor
    }
    
}


//
//  SearchCancelableTextField.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

/// The textfield with slide cancel button.
final public class SearchCancelableTextField: UIView {
    
    // MARK: - Public Properties
    
    /// This
    public var showCancelButton: Bool = false {
        didSet {
            setNeedsLayout()
            UIView.animate(withDuration: Durations.Default.half, delay: 0, options: .curveEaseInOut, animations: {
                self.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    /// The button that cancel action.
    public let cancelButton = UIButton()
    
    /// The input textfield.
    public let textField = SearchTextField()
    
    // MARK: - Internal Properties
    
    var parameters = Layout.Parameters()
    
    // MARK: - Initializers
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    public override var intrinsicContentSize: CGSize {
        return textField.sizeThatFits(frame.size)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        layoutViews()
    }
    
    func commonInit() {
        observe(theme: \DefaultTheme.textfield.searchCancelableTextField, on: WBLibThemeManager.wblibuiManager)
        
        addSubview(cancelButton)
        addSubview(textField)
        
        //TODO: - Change Font
        cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        cancelButton.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        
        textField.textField.addTarget(self, action: #selector(editingDidBegin), for: .editingDidBegin)
        textField.returnKeyType = .search
    }
    
    @discardableResult
    public override func becomeFirstResponder() -> Bool {
        super.becomeFirstResponder()
        return textField.becomeFirstResponder()
    }

    // MARK: - Private Methods
    
    private func updateClearButton() {
        UIView.animate(withDuration: Durations.Default.half) {
            self.textField.updateClearButton()
        }
    }
    
    @objc private func cancel() {
        textField.endEditing(true)
        showCancelButton = false
    }
    
    @objc private func editingDidBegin() {
        showCancelButton = true
    }
        
}

// MARK: - Layout

extension SearchCancelableTextField {
    
    struct Layout {
        struct Parameters {
            struct CancelButton {
                var height: CGFloat = 18.0
                var leftSpacing: CGFloat = 12.0
            }
            
            var cancelButton = CancelButton()
        }
    }
    
    func layoutViews() {
        cancelButton.frame.size.height = parameters.cancelButton.height
        cancelButton.sizeToFit()
        cancelButton.frame.size.height = parameters.cancelButton.height
        
        let widthFieldWithCancelButton: CGFloat =
            bounds.width - parameters.cancelButton.leftSpacing - cancelButton.frame.width
        textField.frame.size.width = showCancelButton ? widthFieldWithCancelButton : bounds.size.width
        textField.frame.size.height = bounds.height
        
        cancelButton.frame.alignmentX(
            to: bounds,
            fromAnchor: .right,
            toAnchor: .right)
        cancelButton.frame.alignmentY(
            to: textField.frame,
            fromAnchor: .center,
            toAnchor: .center)
    }
    
}

// MARK: - Theme

public class SearchCancelableTextFieldTheme: ColorTheme {
    
    struct TextField {
        let textColor: UIColor
        let tintColor: UIColor
    }
    
    struct Background {
        let color: UIColor
    }
    
    struct CancelButtonTitleColor {
        let normal: UIColor
        let highlighted: UIColor
    }
    
    let textFieldTheme: SearchTextFieldTheme
    let background: Background
    let cancelButtonTitleColor: CancelButtonTitleColor

    public init(palette: Palette) {
        textFieldTheme = SearchTextFieldTheme(palette: palette)
        background = Background(color: palette.colors.dynamic.wbWhite)
        cancelButtonTitleColor = CancelButtonTitleColor(normal: palette.colors.dynamic.wbBrand,
                                                        highlighted: palette.colors.dynamic.wbBrand.withAlphaComponent(0.7))
    }
    
}

extension SearchCancelableTextField: Themeable {
    
    public typealias Theme = SearchCancelableTextFieldTheme

    public func apply(theme: Theme) {
        textField.apply(theme: theme.textFieldTheme)
        cancelButton.setTitleColor(theme.cancelButtonTitleColor.normal, for: .normal)
        cancelButton.setTitleColor(theme.cancelButtonTitleColor.highlighted, for: .highlighted)
        backgroundColor = theme.background.color
    }
    
}


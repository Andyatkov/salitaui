//
//  SearchBar.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 03.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public protocol SearchBarDelegate: UITextFieldDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: SearchBar)
}

public extension SearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: SearchBar) {}
}

/// The SearchBar with canceble text field.
final public class SearchBar: UIView {
    
    // MARK: - Public Properties

    /// Cancelable Text Field.
    public let cancelableTextField = SearchCancelableTextField()
    
    /// Search Bar Delegate.
    public weak var delegate: SearchBarDelegate?
    
    /// Bottom View.
    public let bottomView = UIView()
    
    // MARK: - Internal Properties
    
    var parameters = Layout.Parameters()
    
    // MARK: - Initializers
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        layoutViews()
    }
    
    func commonInit() {
        observe(theme: \DefaultTheme.textfield.searchBar, on: WBLibThemeManager.wblibuiManager)
        
        // TODO: - Add Localizable
        cancelableTextField.textField.placeholder = "Поиск"
        cancelableTextField.cancelButton.setTitle("Отменить", for: [.normal])
        
        cancelableTextField.cancelButton.addTarget(self, action: #selector(tapCancelButton), for: .touchUpInside)
        
        addSubview(bottomView)
        addSubview(cancelableTextField)
    }
    
    // MARK: - Private Methods
   
    @objc private func tapCancelButton() {
        cancelableTextField.textField.text = nil
        delegate?.searchBarCancelButtonClicked(self)
    }
    
}

// MARK: - Layout

extension SearchBar {
    
    struct Layout {
        struct Parameters {
            struct CancelableTextField {
                var height: CGFloat = 36.0
                var leftSpacing: CGFloat = 16.0
                var rightSpacing: CGFloat = 16.0
            }
            struct BottomView {
                var height: CGFloat = 1.0
            }
            
            let cancelableTextField = CancelableTextField()
            let bottomView = BottomView()
        }
    }
    
    func layoutViews() {
        cancelableTextField.frame.size.height = parameters.cancelableTextField.height
        cancelableTextField.frame.size.width = bounds.width
            - parameters.cancelableTextField.leftSpacing
            - parameters.cancelableTextField.rightSpacing
        
        cancelableTextField.frame.alignmentX(
            to: bounds,
            fromAnchor: .left,
            toAnchor: .left,
            offset: parameters.cancelableTextField.leftSpacing)
        
        bottomView.frame.size.height = parameters.bottomView.height
        bottomView.frame.size.width = bounds.width
        bottomView.frame.alignmentY(
            to: bounds,
            fromAnchor: .bottom,
            toAnchor: .bottom)
    }
    
}

// MARK: - Theme

public class SearchBarTheme: ColorTheme {
    
    struct Background {
        let color: UIColor
    }
    
    let background: Background
    let bottomViewBackgroundColor: UIColor
    let searchCancelableTextFieldTheme: SearchCancelableTextFieldTheme

    public init(palette: Palette) {
        background = Background(color: palette.colors.dynamic.wbWhite)
        bottomViewBackgroundColor = palette.colors.dynamic.wbGray
        searchCancelableTextFieldTheme = SearchCancelableTextFieldTheme(palette: palette)
    }
    
}

extension SearchBar: Themeable {
    
    public typealias Theme = SearchBarTheme

    public func apply(theme: Theme) {
        cancelableTextField.apply(theme: theme.searchCancelableTextFieldTheme)
        bottomView.backgroundColor = theme.bottomViewBackgroundColor
        backgroundColor = theme.background.color
    }
    
}

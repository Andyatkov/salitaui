//
//  SearchTextField.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

/// Search textField
public class SearchTextField: UIView {

    // MARK: - Public Properties
    
    open weak var delegate: UITextFieldDelegate? {
        didSet { textField.delegate = delegate }
    }
    
    /// The text displayed by the text field.
    open var text: String? {
        get {
            return textField.text
        }
        set {
            textField.text = newValue
            setNeedsLayout()
        }
    }
    
    /// The string that is displayed when there is no other text in the text field.
    open var placeholder: String? {
        get {
            return textField.attributedPlaceholder?.string
        }
        set {
            textField.attributedPlaceholder = NSAttributedString(string: newValue ?? "", attributes: [:])
        }
    }
    
    /// The font of the text.
    open var font: UIFont? {
        get {
            return textField.font
        }
        set {
            textField.font = newValue
            setNeedsLayout()
        }
    }
    
    /// Set and get text color.
    open var textColor: UIColor? {
        get { return textField.textColor }
        set { textField.textColor = newValue }
    }
    
    /// Set and get textField tint color.
    open var textFieldTintColor: UIColor? {
        get { return textField.tintColor }
        set { textField.tintColor = newValue }
    }
    
    /// Set and get textfield return key type.
    open var returnKeyType: UIReturnKeyType  {
        get { return textField.returnKeyType}
        set { textField.returnKeyType = newValue}
    }
    
    /// Set and get textfield return autocorrection type .
    open var autocorrectionType: UITextAutocorrectionType  {
        get { return textField.autocorrectionType }
        set { textField.autocorrectionType = newValue }
    }
    
    /// Set and get textfield keyboard type.
    open var keyboardType: UIKeyboardType {
        get { return textField.keyboardType }
        set { textField.keyboardType = newValue }
    }
    
    /// The left image view.
    public let imageView = UIImageView()
    
    /// The button that clear the text.
    public let clearButton = CustomButton()

    // MARK: - Internal Properties
    
    let textField = UITextField()

    var parameters = Layout.Parameters()
    
    // MARK: - Initializers
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    @discardableResult
    public override func becomeFirstResponder() -> Bool {
        super.becomeFirstResponder()
        return textField.becomeFirstResponder()
    }
    
    // MARK: - Private methods
    
    private func commonInit() {
        observe(theme: \DefaultTheme.textfield.searchTextField, on: WBLibThemeManager.wblibuiManager)
        
        addGestures()
        
        layer.cornerRadius = 8.0
        layer.masksToBounds = true
        
        textField.borderStyle = .none
        //TODO: - Change Font
        textField.font = UIFont.systemFont(ofSize: 18)
        
        clearButton.animationDuration = Durations.Default.half
        clearButton.addTarget(self, action: #selector(clearText), for: .touchUpInside)
        textField.addTarget(self, action: #selector(updateClearButtonAnimated), for: .editingChanged)
        
        imageView.image = UIImage(asset: Asset.Textfield.icSearch)
        
        addSubview(textField)
        addSubview(imageView)
        
        updateClearButton()
    }
    
    private func addGestures() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(becomeFirstResponder))
        addGestureRecognizer(tap)
    }
    
    func updateClearButton() {
        clearButton.alpha = (textField.text ?? "").isEmpty ? 0.0 : 1.0
    }
    
    @objc func updateClearButtonAnimated() {
        UIView.animate(
            withDuration: Durations.Default.half / 2.0,
            delay: 0,
            options: [.curveEaseInOut],
            animations: {
                self.updateClearButton()
            }, completion: nil)
    }
    
    @objc func clearText() {
        let range = NSRange(location: 0, length: textField.text?.count ?? 0)
        
        if textField.delegate?.textField?(textField, shouldChangeCharactersIn: range, replacementString: "") ?? true {
            textField.text = ""
            updateClearButtonAnimated()
        }
    }
    
    // MARK: - Layout
    
    public override var intrinsicContentSize: CGSize {
        return sizeThatFits(frame.size)
    }
    
    public override func sizeThatFits(_ size: CGSize) -> CGSize {
        let params = parameters
        let tfSize = textField.sizeThatFits(size)
        
        let width = params.imageView.maxWidth +
            params.textfield.leftSpacing +
            tfSize.width +
            params.clearButton.leftSpacing +
            params.clearButton.width +
            params.spacing.left +
            params.spacing.right
        
        let height = tfSize.height + 2.0 * params.textfield.verticalInset
        
        return CGSize(width: width, height: height)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        layoutViews()
    }
    
}

extension SearchTextField {

    struct Layout {
        struct Parameters {
            struct ImageView {
                var maxWidth: CGFloat = 24.0
            }
            
            struct TextField {
                var verticalInset: CGFloat = 8
                var leftSpacing: CGFloat = 8.0
            }
            
            struct ClearButton {
                var width: CGFloat = 24.0
                var leftSpacing: CGFloat = 8.0
            }
            
            struct Spacing {
                var left: CGFloat = 8.0
                var right: CGFloat = 8.0
            }
            
            var imageView = ImageView()
            var textfield = TextField()
            var clearButton = ClearButton()
            var spacing = Spacing()
        }
        
        struct Frames {
            let imageView: CGRect
            let textField: CGRect
            let clearButton: CGRect
        }
    }
    
    func calculateLayout(size: CGSize) -> Layout.Frames {
        let parameters = self.parameters
        
        // Image View
        let imageSize = CGSize(width: 22, height: 22)
        
        var ivSize = CGSize.zero
        ivSize.width = min(imageSize.height, parameters.imageView.maxWidth)
        ivSize.height = min(size.height, imageSize.height)
        
        let ivSpacingY = (size.height - ivSize.height) / 2.0
        
        var ivFrame = CGRect.zero
        ivFrame.origin = CGPoint(x: parameters.spacing.left, y: ivSpacingY)
        
        ivFrame.size = ivSize
        
        // Clear Button
        let cbSize = CGSize(width: parameters.clearButton.width, height: size.height)
        
        var cbFrame = CGRect.zero
        
        cbFrame.origin.x = size.width - cbSize.width - parameters.spacing.right
        cbFrame.origin.y = 0
        
        cbFrame.size = cbSize
        
        // TextField
        let tfHeight = textField.sizeThatFits(.zero).height
        let tfSpacingY = (size.height - tfHeight) / 2.0
        
        var tfFrame = CGRect.zero
        
        tfFrame.origin.x = parameters.spacing.left + parameters.imageView.maxWidth + parameters.textfield.leftSpacing
        tfFrame.origin.y = tfSpacingY
        
        tfFrame.size.width = size.width - tfFrame.origin.x - cbFrame.width - parameters.spacing.right - parameters.clearButton.leftSpacing
        tfFrame.size.height = tfHeight
        
        return Layout.Frames(imageView: ivFrame, textField: tfFrame, clearButton: cbFrame)
    }
    
    func layoutViews() {
        let frames = calculateLayout(size: frame.size)
        
        imageView.frame = frames.imageView
        textField.frame = frames.textField
        clearButton.frame = frames.clearButton
    }

}

// MARK: - Theme

public class SearchTextFieldTheme: ColorTheme {
    
    struct TextField {
        let textColor: UIColor
        let tintColor: UIColor
    }
    
    struct Background {
        let color: UIColor
    }
    
    struct ImageView {
        let tintColor: UIColor
    }
    
    struct ClearButton {
        let tintColor: UIColor
    }
    
    let textField: TextField
    let background: Background
    let imageView: ImageView
    let clearButton: ClearButton

    public init(palette: Palette) {
        textField = TextField(textColor: palette.colors.dynamic.wbBlack,
                              tintColor: palette.colors.dynamic.wbBrand)
        background = Background(color: palette.colors.dynamic.wbGray)
        imageView = ImageView(tintColor: palette.colors.dynamic.wbDarkGray)
        clearButton = ClearButton(tintColor: palette.colors.dynamic.wbDarkGray)
    }
    
}

extension SearchTextField: Themeable {
    
    public typealias Theme = SearchTextFieldTheme

    public func apply(theme: Theme) {
        tintColor = theme.textField.tintColor
        backgroundColor = theme.background.color
        imageView.tintColor = theme.imageView.tintColor
        clearButton.tintColor = theme.clearButton.tintColor
    }
    
}


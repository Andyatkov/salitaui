//
//  MaterialTextField.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 07.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

/// Text field delegate like standard UITextFieldDelegate of UIKit framework.
@objc public protocol  MaterialTextFieldDelegate {
    @objc optional func textFieldDidChange(_ textField: MaterialTextField)
    @objc optional func textFieldShouldBeginEditing(_ textField: MaterialTextField) -> Bool
    @objc optional func textFieldDidBeginEditing(_ textField: MaterialTextField)
    @objc optional func textFieldShouldEndEditing(_ textField: MaterialTextField) -> Bool
    @objc optional func textFieldDidEndEditing(_ textField: MaterialTextField)
    @objc optional func textFieldDidEndEditing(_ textField: MaterialTextField, reason: UITextField.DidEndEditingReason)
    @objc optional func textField(_ textField: MaterialTextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    @objc optional func textFieldShouldReturn(_ textField: MaterialTextField) -> Bool
    @objc optional func textFieldDeleteBackward(_ textField: MaterialTextField, previousString: String?)
    @objc optional func textFieldShouldClear(_ textField: MaterialTextField) -> Bool
}

/// Base material textField class.
open class MaterialTextField: UIView, Themeable {
    
    public var selectedTextRange: UITextRange?
    
    // MARK: Public Properties
    
    /// The receiver’s delegate.
    open weak var delegate: MaterialTextFieldDelegate?
    
    /// The font of the text.
    open var font: UIFont? {
        get {
            textField.font
        }
        set {
            textField.font = newValue
            setNeedsLayout()
        }
    }
    
    /// The font of information label.
    open var infoFont: UIFont? {
        get {
            return infoLabel.font
        }
        set {
            infoLabel.font = newValue
            setNeedsLayout()
        }
    }
    
    /// The font of placeholder label.
    open var placeholderFont: UIFont? {
        get {
            return placeholderLabel.font
        }
        set {
            var collapsedFont: UIFont?
            
            if let font = newValue {
                collapsedFont = font.withSize(font.pointSize - 5.0)
            }
            placeholderLabel.font = newValue
            placeholderCollapsedLabel.font = collapsedFont
            
            setNeedsLayout()
        }
    }
    
    /// The string that is displayed on the top of text field.
    open var placeholder: String? {
        get {
            return placeholderLabel.text
        }
        set {
            placeholderLabel.text = newValue
            placeholderCollapsedLabel.text = newValue
            setNeedsLayout()
        }
    }
    
    /// The text displayed by the text field.
    open var text: String? {
        get {
            return textField.text
        }
        set {
            textField.text = newValue
            updateAnimated()
            setNeedsLayout()
        }
    }
    
    /// Information message, that can be displayed on the bottom of the text field.
    open var infoText: String? {
        get {
            return infoLabel.text
        }
        set {
            infoLabel.text = newValue
            updateAccessoryView()
            setNeedsLayout()
        }
    }
    
    /// The maximum number of lines to use for rendering text.
    open var infoNumberOfLines: Int {
        get {
            return infoLabel.numberOfLines
        }
        set {
            infoLabel.numberOfLines = newValue
            updateAnimated()
            setNeedsLayout()
        }
    }
    
    /// A Boolean value indicating whether the text field is enabled.
    open var isEnabled = true {
        didSet {
            updateAccessoryView()
            updateAnimated()
        }
    }
    
    /// A Boolean value to force hide clear button.
    open var showClearButton = true {
        didSet { updateAccessoryView() }
    }
    
    /// Detect the state of text field. Text field is collapsed on disable state and if text is empty.
    open var isCollapsed: Bool {
        if !isEnabled {
            return true
        } else if !(text ?? "").isEmpty {
            return false
        } else {
            return !isTextFieldFocused
        }
    }
    
    /// The custom accessory view to display when the text field becomes the first responder.
    override open var inputAccessoryView: UIView? {
        get {
            return _inputAccessoryView
        }
        set {
            _inputAccessoryView = newValue
        }
    }
    
    private var _inputAccessoryView: UIView?
    
    /// Do not set textField delegate. It can brake internal logic. Use MaterialTextFieldDelegate.
    
    /// Minimum Accessory view tap area.
    /// You can modify this property to modify touch area of accessory views.
    /// For example if button have a small size 22x22 and minAccessoryTapArea is 44x44, the tap area of the button
    /// will be 44x44.
    open var minAccessoryTouchArea = CGSize(width: 44.0, height: 44.0)
    
    /// The textField.
    public let textField: UITextField = DeleteBackwardTextField()
    
    /// Color theme.
   var theme: MaterialTextFieldTheme?
    
    /// The button that remove all text field text.
    let clearButton = UIButton(type: .system)
    
    /// The button that displayed when isEnabled property is false.
    let lockButton = UIButton(type: .system)
    
    // MARK: Internal Properties
    
    /// The View that contains a accessory view like lockButton or clearButton.
    let accessoryContainerView = UIView()
    
    /// The Placeholder view.
    let placeholderLabel = UILabel()
    
    /// The Background view.
    let backgroundView = UIView()
    
    /// The label that show information.
    let infoLabel = UILabel()
    
    /// The helper label for layout calculation.
    let placeholderCollapsedLabel = UILabel()
    
    /// The color that will fill views on enable state.
    var activeColor: UIColor = .gray
    
    /// The color that will fill views on disable state.
    var inactiveColor: UIColor = .white
    
    /// The primary color.
    var primaryColor: UIColor = .white
    
    /// The minor color.
    var minorColor: UIColor = .white
    
    /// Helper property to indicate UITextField focus state.
    var isTextFieldFocused = false
    
    /// Base animation duration.
    var animationDuration: TimeInterval = 0.22
    
    /// Layout parameters like insets and offsets between views. Used by layout calculation.
    /// Can be set by subclasses to change internal views positions.
    var parameters = Layout.Parameters()
    
    /// Accessory view that will be show depended from text field states.
    /// Can be override by subclasses to add more business logic.
    var currentAccessoryView: UIView? {
        let text = textField.text ?? ""
        
        if showClearButton && isEnabled && isTextFieldFocused && !text.isEmpty {
            return clearButton
        } else if !isEnabled {
            return lockButton
        }
        
        return nil
    }
    
    // MARK: - Initializers
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    // MARK: - Private methods
    
    private func commonInit() {
        observe(theme: \DefaultTheme.textfield.materialTextFieldTheme, on: WBLibThemeManager.wblibuiManager)
        
        clipsToBounds = true
        
        setupFonts()
        
        addSubviews()
        addTapGesture()
        
        setupPlaceholder()
        setupTextField()
        setupInfoLabel()
        setupButtons()
        setupBackgroundView()
        
        updateTextField()
        updatePlaceholder()
        updateInfoLabelView()
    }
    
    private func addSubviews() {
        addSubview(backgroundView)
        addSubview(infoLabel)
        
        backgroundView.addSubview(placeholderLabel)
        backgroundView.addSubview(textField)
        backgroundView.addSubview(accessoryContainerView)
    }
    
    private func setupPlaceholder() {
        placeholderLabel.layer.anchorPoint = .zero
        
        placeholderCollapsedLabel.numberOfLines = 1
        placeholderLabel.numberOfLines = 1
    }
    
    private func setupBackgroundView() {
        backgroundView.clipsToBounds = true
        backgroundView.layer.cornerRadius = 4.0
    }
    
    private func setupTextField() {
        textField.delegate = self
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        (textField as? DeleteBackwardTextField)?.deleteBackwardHandler = { [weak self] prevString in
            guard let `self` = self else { return }
            
            self.delegate?.textFieldDeleteBackward?(self, previousString: prevString)
        }
    }
    
    private func setupInfoLabel() {
        infoLabel.contentMode = .topLeft
        infoLabel.numberOfLines = 1
    }
    
    private func setupButtons() {
        clearButton.setImage(Asset.Cell.Accessory.icClose.image, for: .normal)
        
        clearButton.addTarget(self, action: #selector(clearText), for: .touchUpInside)
    }
    
    private func addTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        addGestureRecognizer(tap)
    }
    
    private func setupFonts() {
        font = UIFont.systemFont(ofSize: 17.0, weight: .regular)
        placeholderFont = UIFont.systemFont(ofSize: 17.0, weight: .regular)
        infoFont = UIFont.systemFont(ofSize: 12.0, weight: .regular)
    }
    
    private func updateBorderColor() {
        if !textField.isFirstResponder {
            backgroundView.layer.borderWidth = 0
            backgroundView.layer.borderColor = nil
        } else {
            backgroundView.layer.borderWidth = 1
            backgroundView.layer.borderColor = theme?.borderColor.cgColor
        }
    }
    
    private func updatePlaceholder() {
        if !isCollapsed {
            let scale = placeholderCollapsedLabel.font.pointSize / placeholderLabel.font.pointSize
            placeholderLabel.transform = CGAffineTransform(scaleX: scale, y: scale)
        } else {
            placeholderLabel.transform = .identity
        }
    }
    
    private func updateTextField() {
        // WORKAROUND:
        // Text field need be visible in collapsed state for working with textContentType autofill feature.
        // If textField will be hidden, autocompletion ignore this textField
        // Without autocompletion it must be "if isEnabled && !isCollapsed."
        
        if isEnabled {
            textField.isHidden = false
        } else {
            textField.isHidden = true
        }
    }
    
    private func updateInfoLabelView() {
        if isEnabled {
            infoLabel.textColor = activeColor
        } else {
            infoLabel.textColor = inactiveColor
        }
    }
    
    // MARK: - Internal Methods
    
    /// Update accessory view using currentAccessoryView property.
    func updateAccessoryView() {
        setAccessoryView(contentView: currentAccessoryView)
        layoutWithoutPlaceholder()
    }
    
    /// Update text field state changes.
    func updateAnimated() {
        if !textField.isHidden {
            updateTextField()
        }
        UIView.animate(
            withDuration: animationDuration,
            delay: 0,
            options: [.curveEaseInOut],
            animations: {
                self.animationUpdates()
            }, completion: { _ in
                self.animationCompletionUpdates()
            })
    }
    
    /// Method that will be called in updateAnimated animation block. Override to add some animation updates.
    func animationUpdates() {
        layoutSubviews()
        updatePlaceholder()
        updateBorderColor()
        updateInfoLabelView()
    }
    
    /// Method that will be called in updateAnimated animation block.
    func animationCompletionUpdates() {
        updateTextField()
    }
    
    /// Add view to accessory container view.
    ///
    /// - Parameter contentView: View to add.
    func setAccessoryView(contentView: UIView?) {
        let currentContentView = accessoryContainerView.subviews.first
        
        guard currentContentView != contentView else {
            return
        }
        
        currentContentView?.removeFromSuperview()
        
        if let view = contentView {
            accessoryContainerView.addSubview(view)
        }
    }
    
    override open var intrinsicContentSize: CGSize {
        return sizeThatFits(frame.size)
    }
    
    @discardableResult
    open override func becomeFirstResponder() -> Bool {
        super.becomeFirstResponder()
        return textField.becomeFirstResponder()
    }
    
    @discardableResult
    open override func resignFirstResponder() -> Bool {
        super.resignFirstResponder()
        return textField.resignFirstResponder()
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        accessoryContainerView.subviews.first?.sizeToFit()
        
        layoutViews()
        invalidateIntrinsicContentSize()
    }
    
    func layoutWithoutPlaceholder() {
        let plFrame = placeholderLabel.frame
        layoutSubviews()
        
        placeholderLabel.frame = plFrame
    }
    
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        var avWidth: CGFloat = 0
        
        if let view = accessoryContainerView.subviews.first {
            avWidth = view.sizeThatFits(.zero).width
        }
        let isAccessoryEmpty = !(avWidth > 0)
        
        let avInsets = isAccessoryEmpty ? 0 : parameters.accessoryView.right + parameters.accessoryView.left
        let horInsets = parameters.insets.left + parameters.insets.right
        
        let minWidth = avInsets + horInsets + avWidth
        let maxWidth = max(size.width, minWidth)
        
        let height = calculateLayout(size: CGSize(width: maxWidth, height: .greatestFiniteMagnitude)).totalHeight
        
        return CGSize(width: maxWidth, height: height)
    }
    
    override open func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        for view in accessoryContainerView.subviews {
            let dx = max(minAccessoryTouchArea.width - view.frame.size.width, 0.0)
            let dy = max(minAccessoryTouchArea.height - view.frame.size.height, 0.0)
            
            if  view.convert(view.frame, to: self).insetBy(dx: -dx, dy: -dy).contains(point) {
                return view
            }
        }
        
        return super.hitTest(point, with: event)
    }
    
    // MARK: - Theme
    
    public typealias Theme = MaterialTextFieldTheme

    public func apply(theme: MaterialTextFieldTheme) {
        self.theme = theme
        
        primaryColor = theme.primaryColor
        
        minorColor = theme.minorColor
        activeColor = theme.activeColor
        inactiveColor = theme.inactiveColor
        
        clearButton.tintColor = minorColor
        lockButton.tintColor = inactiveColor
        
        textField.textColor = theme.textFieldColor.textColor
        textField.tintColor = theme.textFieldColor.tintColor
        
        placeholderLabel.textColor = theme.placeholderColor
        
        infoLabel.textColor = theme.statusColor.normal
        
        backgroundView.backgroundColor = theme.backgroundViewColor
        backgroundColor = theme.backgroundColor
    }

}

// MARK: - UITextInputTraits

extension MaterialTextField: UITextInputTraits {

    @objc open var hasText: Bool {
        return textField.hasText
    }
    
    @objc open func insertText(_ text: String) {
        textField.insertText(text)
    }
    
    @objc open func deleteBackward() {
        textField.deleteBackward()
    }
    
    @objc open var autocapitalizationType: UITextAutocapitalizationType {
        get { return textField.autocapitalizationType }
        set { textField.autocapitalizationType = newValue }
    }
    
    @objc open var autocorrectionType: UITextAutocorrectionType {
        get { return textField.autocorrectionType }
        set { textField.autocorrectionType = newValue }
    }
    
    @objc open var spellCheckingType: UITextSpellCheckingType {
        get { return textField.spellCheckingType }
        set { textField.spellCheckingType = newValue }
    }
    
    @objc open var smartQuotesType: UITextSmartQuotesType {
        get { return textField.smartQuotesType }
        set { textField.smartQuotesType = newValue }
    }
    
    @objc open var smartDashesType: UITextSmartDashesType {
        get { return textField.smartDashesType }
        set { textField.smartDashesType = newValue }
    }
    
    @objc open var smartInsertDeleteType: UITextSmartInsertDeleteType {
        get { return textField.smartInsertDeleteType }
        set { textField.smartInsertDeleteType = newValue }
    }
    
    @objc open var keyboardType: UIKeyboardType {
        get { return textField.keyboardType }
        set { textField.keyboardType = newValue }
    }
    
    @objc open var keyboardAppearance: UIKeyboardAppearance {
        get { return textField.keyboardAppearance }
        set { textField.keyboardAppearance = newValue }
    }
    
    @objc open var enablesReturnKeyAutomatically: Bool {
        get { return textField.enablesReturnKeyAutomatically }
        set { textField.enablesReturnKeyAutomatically = newValue }
    }
    
    @objc open var isSecureTextEntry: Bool {
        get { return textField.isSecureTextEntry }
        set { textField.isSecureTextEntry = newValue }
    }
    
    @objc open var textContentType: UITextContentType {
        get { return textField.textContentType }
        set { textField.textContentType = newValue }
    }

}

// MARK: -

extension MaterialTextField {
    
    var beginningOfDocument: UITextPosition {
        return textField.beginningOfDocument
    }
    
    func offset(from: UITextPosition, to toPosition: UITextPosition) -> Int {
        return textField.offset(from: from, to: toPosition)
    }
    
    func position(from position: UITextPosition, offset: Int) -> UITextPosition? {
        return textField.position(from: position, offset: offset)
    }
    
    func textRange(from fromPosition: UITextPosition, to toPosition: UITextPosition) -> UITextRange? {
        return textField.textRange(from: fromPosition, to: toPosition)
    }
    
}

// MARK: - Text field gesture actions.

extension MaterialTextField {

    @objc func handleTap() {
        guard isEnabled && isCollapsed else {
            return
        }
        
        updateAnimated()
        becomeFirstResponder()
    }
    
    @objc func clearText() {
        let range = NSRange(location: 0, length: text?.count ?? 0)
        
        if delegate?.textField?(self, shouldChangeCharactersIn: range, replacementString: "") ?? true {
            text = ""
            delegate?.textFieldDidChange?(self)
        }
        
        updateAccessoryView()
    }

}

// MARK: - Layout

extension MaterialTextField {

    struct Layout {
        struct Parameters {
            struct AccessoryView {
                var left: CGFloat = 8.0
                var right: CGFloat = 16.0
            }
            
            struct Placeholder {
                var fontSizeDiff: CGFloat = 5.0
                var top: CGFloat = 8.0
                var left: CGFloat = 16.0
                var bottom: CGFloat = 8.0
            }
            
            struct TextField {
                var top: CGFloat = 4.0
                var left: CGFloat = 16.0
            }
            
            struct InfoLabel {
                var fontSizeDiff: CGFloat = 4.0
                var top: CGFloat = 8.0
            }
            
            var insets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
            var accessoryView = AccessoryView()
            var placeholder = Placeholder()
            var textField = TextField()
            var infoLabel = InfoLabel()
        }
        
        struct Frames {
            let accessoryView: CGRect
            let placeholderLabel: CGRect
            let textField: CGRect
            let backgroundView: CGRect
            let infoLabel: CGRect
            let totalHeight: CGFloat
        }
    }
    
    func calculateLayout(size: CGSize) -> Layout.Frames {
        var contentFrame = CGRect(
            x: parameters.insets.left,
            y: parameters.insets.top,
            width: frame.width - parameters.insets.left - parameters.insets.right,
            height: frame.height - parameters.insets.top - parameters.insets.bottom)
        
        contentFrame = ceil(contentFrame)
        
        let insetsHorizontal = parameters.insets.left + parameters.insets.right
        contentFrame.size.width = max(contentFrame.width, insetsHorizontal)
        
        // Accessory view
        var avSize: CGSize = .zero
        
        if let view = accessoryContainerView.subviews.first {
            avSize = view.sizeThatFits(.zero)
        }
        
        let avHeight = avSize.height
        let avWidth = avSize.width
        let avRight = parameters.accessoryView.right
        let avLeft = parameters.accessoryView.left
        
        let isAccessoryEmpty = !(avWidth > 0)
        
        let avX = contentFrame.origin.x + contentFrame.width - avWidth - avRight
        
        var width = contentFrame.width
        
        if !isAccessoryEmpty {
            width = avX - avLeft - parameters.insets.left
        }
        
        // TextField view
        
        let placeholderLabelFont = placeholderLabel.font !! preconditionFailure()
        let plHeightNotCollapsedFont = placeholderCollapsedLabel.font !! preconditionFailure()
        let textFiledFont = textField.font !! preconditionFailure()
        
        let plHeightCollapsed = ceil(placeholderLabelFont.lineHeight)
        let plHeightNotCollapsed = ceil(plHeightNotCollapsedFont.lineHeight + parameters.placeholder.fontSizeDiff)
        
        let tX = parameters.placeholder.left
        let tY = contentFrame.origin.y + parameters.textField.top + max(plHeightCollapsed, plHeightNotCollapsed)
        
        let tHeight = ceil(textFiledFont.lineHeight)
        let tWidth = width
        
        let tYOffset = tY + tHeight
        
        // Info label
        
        let iX = contentFrame.origin.x
        let iY = tYOffset + parameters.infoLabel.top
        
        let iHeight = ceil(infoLabel.preferredSize(for: contentFrame.size).height)
        let iWidth = contentFrame.width
        
        // Placeholder
        
        let plWidth = width
        let plX = parameters.placeholder.left
        
        let plyNotCollapsed = parameters.placeholder.top
        let plYCollapsed = tY - plHeightNotCollapsed / 2
        
        let isInfoEmpty = (infoLabel.text ?? "").isEmpty
        
        var totalHeight = isInfoEmpty ?  tY + tHeight + parameters.placeholder.bottom : iY + iHeight
        totalHeight += parameters.insets.bottom
        
        let avY = (contentFrame.height - iHeight - avHeight) / 2
        
        let plncFrame = CGRect(x: plX, y: plyNotCollapsed, width: plWidth, height: plHeightNotCollapsed)
        let plcFrame = CGRect(x: plX, y: plYCollapsed, width: plWidth, height: plHeightNotCollapsed)
        let bkFrame = CGRect(x: 0, y: 0, width: contentFrame.width, height: contentFrame.height - iHeight)
        let avFrame = CGRect(x: avX, y: avY, width: avWidth, height: avHeight)
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tHeight)
        let iFrame = CGRect(x: iX, y: iY, width: iWidth, height: iHeight)
        
        return Layout.Frames(
            accessoryView: avFrame,
            placeholderLabel: isCollapsed ? plcFrame : plncFrame,
            textField: tFrame,
            backgroundView: bkFrame,
            infoLabel: iFrame,
            totalHeight: totalHeight)
    }
    
    func layoutViews() {
        let frames = calculateLayout(size: frame.size)
        
        backgroundView.frame = frames.backgroundView
        
        accessoryContainerView.frame = frames.accessoryView
        placeholderLabel.frame = frames.placeholderLabel
        textField.frame = frames.textField
        infoLabel.frame = frames.infoLabel
    }

}


// MARK: - UITextFieldDelegate

extension MaterialTextField: UITextFieldDelegate {

    @objc public func textFieldDidChange(_ textField: UITextField) {
        updateAccessoryView()
        updateAnimated()
        
        delegate?.textFieldDidChange?(self)
    }
    
    @objc public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return delegate?.textFieldShouldBeginEditing?(self) ?? true
    }
    
    @objc public func textFieldDidBeginEditing(_ textField: UITextField) {
        isTextFieldFocused = true
        
        updateAnimated()
        updateAccessoryView()
        
        delegate?.textFieldDidBeginEditing?(self)
    }
    
    @objc public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return delegate?.textFieldShouldEndEditing?(self) ?? true
    }
    
    @objc public func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        isTextFieldFocused = false
        
        updateAnimated()
        updateAccessoryView()
        
        if let textFieldDidEndEditing = delegate?.textFieldDidEndEditing(_:) {
            textFieldDidEndEditing(self)
        } else {
            delegate?.textFieldDidEndEditing?(self, reason: reason)
        }
    }
    
    @objc public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return delegate?.textField?(self, shouldChangeCharactersIn: range, replacementString: string) ?? true
    }
    
    @objc public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return delegate?.textFieldShouldReturn?(self) ?? true
    }
    
    @objc public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return delegate?.textFieldShouldClear?(self) ?? true
    }

}

// MARK: - Theme

public class MaterialTextFieldTheme: ColorTheme {
    
    var primaryColor: UIColor
    var minorColor: UIColor
    var activeColor: UIColor
    var inactiveColor: UIColor
    
    struct TextField {
        var textColor: UIColor
        var tintColor: UIColor
    }
    
    struct Status {
        let success: UIColor
        let normal: UIColor
        let error: UIColor
    }
    
    var statusColor: Status
    
    var textFieldColor: TextField
    var placeholderColor: UIColor
    
    var backgroundViewColor: UIColor
    var backgroundColor: UIColor
    
    var borderColor: UIColor
    
    public init(palette: Palette) {
        primaryColor = palette.colors.dynamic.wbBlack
        minorColor = palette.colors.dynamic.wbBlack
        
        activeColor = palette.colors.dynamic.wbDarkGray
        inactiveColor = palette.colors.dynamic.wbGray
        
        textFieldColor = TextField(textColor: palette.colors.dynamic.wbBlack,
                                   tintColor: palette.colors.dynamic.wbBrand)
        
        statusColor = Status(success: palette.colors.dynamic.wbGreen,
                             normal: palette.colors.dynamic.wbDarkGray,
                             error: palette.colors.dynamic.wbRed)
        
        placeholderColor = palette.colors.dynamic.wbDarkGray
        
        borderColor = palette.colors.dynamic.wbBrand
        
        backgroundViewColor = palette.colors.dynamic.wbGray
        backgroundColor = palette.colors.dynamic.wbWhite
    }
    
}


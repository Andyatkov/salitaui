//
//  StrictTextField.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 09.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

/// Material text field that can indicate status.
open class StrictTextField: MaterialTextField {

    // MARK: - Public Properties
    
    public enum Status {
        case normal
        case error
        case success
    }
    
    /// Button that will be displayed on success status.
    public let successImageView: UIImageView = SuccessImageView(frame: .zero)

    /// Current text field status.
    open var status: Status = .normal {
        didSet {
            updateAccessoryView()
            updateAnimated()
        }
    }
    
    // MARK: - Initializers
    
    override var currentAccessoryView: UIView? {
        if status == .success && self.isEnabled {
            return successImageView
        }

        return super.currentAccessoryView
    }
    
    override func animationUpdates() {
        super.animationUpdates()
        
        guard isEnabled else {
            return
        }
        
        switch self.status {
        case .success:
            infoLabel.textColor = theme?.statusColor.success
        case .normal:
            infoLabel.textColor = theme?.statusColor.normal
        case .error:
            backgroundView.layer.borderWidth = 1
            backgroundView.layer.borderColor = theme?.statusColor.error.cgColor
            infoLabel.textColor = theme?.statusColor.error
            
            TactileSignals.error.tactileSignals()
        }
    }
    
    

}

fileprivate final class SuccessImageView: UIImageView {

    let radius: CGFloat = 12.0

    override init(frame: CGRect) {
        super.init(frame: frame)

        isUserInteractionEnabled = false

        layer.masksToBounds = true
        layer.cornerRadius = radius

        contentMode = .center
        image = Asset.Textfield.icSuccess.image
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: 2.0 * radius, height: 2.0 * radius)
    }

}

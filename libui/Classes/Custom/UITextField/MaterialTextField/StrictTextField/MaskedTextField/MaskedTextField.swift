//
//  MaskedTextField.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 13.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

/// The secure text field designed to enter secure data like passwords.
open class MaskedTextField: StrictTextField {

    /// Identifies whether the text object should disable text copying and in some cases hide the text being entered.
    open var isMasked: Bool = true {
        didSet {
            
            let imageAsset = isMasked ? Asset.Textfield.icMasked : Asset.Textfield.icNotMasked
            maskedButton.setImage(UIImage(asset: imageAsset), for: .normal)
            textField.isSecureTextEntry = isMasked
        }
    }
    
    let maskedButton = UIButton(type: .system)
    
    // MARK: - Initializers
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        maskedButton.showsTouchWhenHighlighted = true
        maskedButton.addTarget(self, action: #selector(maskButtonTap), for: .touchUpInside)
        maskedButton.addTactile(.warning, forControleEvents: .touchUpInside)
        
        updateAccessoryView()
        
        isMasked = true
    }
    
    override var currentAccessoryView: UIView {
        guard isEnabled else { return lockButton }
        
        guard let text = text, !text.isEmpty else { return UIView() }
        
        return maskedButton
    }
    
    @objc func maskButtonTap() {
        isMasked = !isMasked
    }

}


//
//  PhoneTextField.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 10.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit
import PhoneNumberKit

open class PhoneTextField: StrictTextField, MaterialTextFieldDelegate {
    
    // MARK: - Public Properties
    
    override open var text: String? {
        set {
            if let newValue = newValue {
                var formattedNumber = partialFormatter.formatPartial(newValue)
                formattedNumber = appendPlusIfNeeded(string: formattedNumber)
                if autoreplaceRussianCountryCode {
                  formattedNumber = replaceEight(string: formattedNumber)
                }
                super.text = formattedNumber
                
            } else {
                super.text = newValue
            }
        }
        get {
            return super.text
        }
    }
    
    /// Return a user’s default region code.
    public var defaultRegion = PhoneNumberKit.defaultRegionCode() {
        didSet {
            partialFormatter.defaultRegion = defaultRegion
        }
    }
    
    /// Return current region code.
    public var currentRegion: String {
        return partialFormatter.currentRegion
    }
    
    public var autoreplaceRussianCountryCode: Bool = false
    
    /// Return current phone number.
    public var phone: PhoneNumber? {
        return try? phoneNumberKit.parse(text ?? "")
    }
    
    /// Check text in current text field and return true if number is correct.
    public var isValidNumber: Bool {
        return (try? phoneNumberKit.parse(text ?? "", withRegion: currentRegion)) == nil ? false : true
    }
    
    /// Phone number kit.
    public var phoneNumberKit = PhoneNumberKit()
    
    // MARK: - Internal Properties
    
    struct CursorPosition {
        let numberAfterCursor: String
        let repetitionCountFromEnd: Int
    }

    lazy var partialFormatter: PartialFormatter = {
        return PartialFormatter(phoneNumberKit: phoneNumberKit, defaultRegion: defaultRegion, withPrefix: true)
    }()

    let nonNumericSet: NSCharacterSet = {
        var mutableSet = NSMutableCharacterSet.decimalDigit().inverted
        mutableSet.remove(charactersIn: "+＋")
        return mutableSet as NSCharacterSet
    }()

    // MARK: - Initializers

    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        textContentType = .telephoneNumber
        keyboardType = .phonePad
    }

    public override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // This allows for the case when a user autocompletes a phone number:
        if range == NSRange(location: 0, length: 0) && string == " " {
            return true
        }

        guard let text = text else {
            return false
        }

        guard delegate?.textField?(self, shouldChangeCharactersIn: range, replacementString: string) ?? true else {
            return false
        }

        let textAsNSString = text as NSString
        let changedRange = textAsNSString.substring(with: range) as NSString
        let modifiedTextField = textAsNSString.replacingCharacters(in: range, with: string)

        let filteredCharacters = modifiedTextField.filter {
            return  String($0).rangeOfCharacter(from: nonNumericSet as CharacterSet) == nil
        }

        let rawNumberString = String(filteredCharacters)

        var formattedNationalNumber = partialFormatter.formatPartial(rawNumberString)
        var selectedTextRange: NSRange?
        
        formattedNationalNumber = appendPlusIfNeeded(string: formattedNationalNumber)
        if autoreplaceRussianCountryCode {
           formattedNationalNumber = replaceEight(string: formattedNationalNumber)
        }

        let nonNumericRange = (changedRange.rangeOfCharacter(from: nonNumericSet as CharacterSet).location != NSNotFound)
        
        if range.length == 1 && string.isEmpty && nonNumericRange {
            selectedTextRange = selectionRangeForNumberReplacement(textField: textField, formattedText: modifiedTextField)
            textField.text = modifiedTextField
        } else {
            selectedTextRange = selectionRangeForNumberReplacement(textField: textField, formattedText: formattedNationalNumber)
            textField.text = formattedNationalNumber
        }

        if let selectedTextRange = selectedTextRange,
            let selectionRangePosition = textField.position(from: beginningOfDocument, offset: selectedTextRange.location) {
            let selectionRange = textField.textRange(from: selectionRangePosition, to: selectionRangePosition)
            
            textField.selectedTextRange = selectionRange
        }
        
        updateAccessoryView()
        updateAnimated()
        
        delegate?.textFieldDidChange?(self)

        return false
    }
    
    // MARK: - Private Methods
    
    private func extractCursorPosition() -> CursorPosition? {
        var repetitionCountFromEnd = 0
        
        // Check that there is text in the UITextField
        guard let text = text, let selectedTextRange = textField.selectedTextRange else {
            return nil
        }
        
        let textAsNSString = text as NSString
        let cursorEnd = offset(from: beginningOfDocument, to: selectedTextRange.end)
        
        // Look for the next valid number after the cursor, when found return a CursorPosition struct
        for i in cursorEnd ..< textAsNSString.length {
            let cursorRange = NSRange(location: i, length: 1)
            let candidateNumberAfterCursor: NSString = textAsNSString.substring(with: cursorRange) as NSString
            
            if candidateNumberAfterCursor.rangeOfCharacter(from: nonNumericSet as CharacterSet).location == NSNotFound {
                
                for j in cursorRange.location ..< textAsNSString.length {
                    
                    let candidateCharacter = textAsNSString.substring(with: NSRange(location: j, length: 1))
                    if candidateCharacter == candidateNumberAfterCursor as String {
                        repetitionCountFromEnd += 1
                    }
                }
                
                return CursorPosition(numberAfterCursor: candidateNumberAfterCursor as String, repetitionCountFromEnd: repetitionCountFromEnd)
            }
        }
        
        return nil
    }
    
    // Finds position of previous cursor in new formatted text
    private func selectionRangeForNumberReplacement(textField: UITextField, formattedText: String) -> NSRange? {
        let textAsNSString = formattedText as NSString
        var countFromEnd = 0
        
        guard let cursorPosition = extractCursorPosition() else {
            return nil
        }
        
        for i in stride(from: (textAsNSString.length - 1), through: 0, by: -1) {
            let candidateRange = NSRange(location: i, length: 1)
            let candidateCharacter = textAsNSString.substring(with: candidateRange)
            
            if candidateCharacter == cursorPosition.numberAfterCursor {
                countFromEnd += 1
                
                if countFromEnd == cursorPosition.repetitionCountFromEnd {
                    return candidateRange
                }
            }
        }
        
        return nil
    }
    
    private func appendPlusIfNeeded(string: String) -> String {
        if !string.isEmpty && !string.hasPrefix("+") && !string.hasPrefix("＋") {
            return "+" + string
        } else {
            return string
        }
    }
    
    private func replaceEight(string: String) -> String {
        if string.hasPrefix("+8") {
            return string.replacingOccurrences(of: "+8", with: "+7")
        } else {
            return string
        }
    }
    
}

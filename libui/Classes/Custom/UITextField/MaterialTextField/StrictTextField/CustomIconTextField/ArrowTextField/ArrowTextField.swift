//
//  ArrowTextField.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 13.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public class ArrowTextField: CustomIconTextField {
    
    // MARK: - Initializers
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        commontInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commontInit()
    }
    
    // MARK: - Private Methods
    
    private func commontInit() {
        customButton.setImage(Asset.Textfield.icDownArrow.image, for: .normal)
    }
    
}

//
//  CustomIconTextField.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 13.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

/// The text field with custom button accessory view.
public class CustomIconTextField: StrictTextField {

    /// Custom button
    public var customButton = UIButton(type: .system)
    
    override var currentAccessoryView: UIView {
        guard isEnabled else {
            return lockButton
        }

        return customButton
    }
    
    // MARK: - Initializers
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        updateAccessoryView()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        updateAccessoryView()
    }

}


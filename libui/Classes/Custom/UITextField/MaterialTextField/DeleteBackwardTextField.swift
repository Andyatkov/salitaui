//
//  DeleteBackwardTextField.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 07.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

class DeleteBackwardTextField: UITextField {
    
    var deleteBackwardHandler: ((String?) -> Void)?
    
    override func deleteBackward() {
        let oldValue = self.text
        super.deleteBackward()
        
        deleteBackwardHandler?(oldValue)
    }

}


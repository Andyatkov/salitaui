//
//  SeparatorView.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 06.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

class SeparatorView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        observe(theme: \DefaultTheme.view.separatirView,
                on: WBLibThemeManager.wblibuiManager)
    }
    
}

public struct SeparatorViewTheme: ColorTheme {
    let backgroundColor: UIColor
    
    public init(palette: Palette) {
        backgroundColor = palette.colors.dynamic.wbGray
    }
    
}

extension SeparatorView: Themeable {
    
    public typealias Theme = SeparatorViewTheme

    public func apply(theme: Theme) {
        
        backgroundColor = theme.backgroundColor
    }
    
}

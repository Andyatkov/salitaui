//
//  MaterialSegmentedControl.swift
//  SalitaUI
//
//  Created by Andrey Dyatkov on 24.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import Foundation
import UIKit

public class MaterialSegmentedControl: CustomMaterialSegmentedControl {
    
    
    // MARK: - Private variable
    
    private var titles: [String]?
    private var index: Int?
    
    // MARK: - Public method
    
    open func setTitles(_ titles: [String], selectedIndex: Int = 0) {
        guard titles.count > 0 else {
            return
        }

        self.titles = titles
        self.index = selectedIndex
    }
    
    // MARK: - Initializers
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        self.commonInit()
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        guard let titles = self.titles, let selectedIndex = self.index else {
            return
        }
        
        self.setTitles(titles: titles, style: .fixedWidth(self.frame.width / CGFloat(titles.count)))
        self.selectedIndex = selectedIndex
    }
    
    private func commonInit() {
        self.insets = .zero
        self.textFont = .systemFont(ofSize: 17, weight: .semibold)
        observe(theme: \DefaultTheme.view.materialSegmentedControlTheme,
                on: WBLibThemeManager.wblibuiManager)
    }
    
}

public struct MaterialSegmentedControlTheme: ColorTheme {
    
    let backgroundColor: UIColor
    let textSelectedColor: UIColor
    let textColor: UIColor
    
    struct Slider {
        let color: UIColor
        let backgroundColor: UIColor
    }
    
    let slider: Slider
    
    public init(pallete: Palette) {
        backgroundColor = pallete.colors.dynamic.wbWhite
        textSelectedColor = pallete.colors.dynamic.wbBlack
        textColor = pallete.colors.dynamic.wbDarkGray
        slider = Slider(color: pallete.colors.dynamic.wbBlack,
                        backgroundColor: pallete.colors.dynamic.wbWhite)
    }
    
}

extension MaterialSegmentedControl: Themeable {
    
    public typealias Theme = MaterialSegmentedControlTheme
    
    public func apply(theme: MaterialSegmentedControlTheme) {
        self.backgroundColor = theme.backgroundColor
        self.textSelectedColor = theme.textSelectedColor
        self.textColor = theme.textColor
        setSilder(
            color: theme.slider.color,
            position: .bottomWithHight(2),
            widthStyle: .adaptiveSpace(0),
            backgroundColor: theme.slider.backgroundColor)
    }
    
    
}


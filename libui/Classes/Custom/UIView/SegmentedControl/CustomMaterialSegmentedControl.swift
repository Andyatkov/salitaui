//
//  CustomMaterialSegmentedControl.swift
//  SalitaUI
//
//  Created by Andrey Dyatkov on 24.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

/// Responsible for interacting with CustomMaterialSegmentedControl
public protocol CustomMaterialSegmentedControlSelectedProtocol: class {
    func segmentedControlSelectedIndex(_ index: Int, animated: Bool, segmentedControl: CustomMaterialSegmentedControl)
}

/// Custom Segmented Control class.
public class CustomMaterialSegmentedControl: UIView {
    
    /// Both image & text style
    public enum HybridStyle {
        case normalWithSpace(CGFloat)
        case imageTopWithSpace(CGFloat)
        case imageBottomWithSpace(CGFloat)
    }
    
    /// Slider podition
    public enum SliderPositionStyle {
        case bottomWithHight(CGFloat)
        case topWidthHeight(CGFloat)
    }
    
    /// Only text style
    public enum WidthStyle {
        case fixedWidth(CGFloat)
        case adaptiveSpace(CGFloat)
    }

    // MARK: - Public Properties
    
    public var scrollView = UIScrollView()
    
    /// Segment control delegate
    public weak var delegate: CustomMaterialSegmentedControlSelectedProtocol?
    
    /// Count items
    public var countItems: Int {
        return self.itemsArray.count
    }
    /// Items Text Font
    public var textFont: UIFont = UIFont.systemFont(ofSize: 15) {
        didSet { self.itemsArray.forEach { $0.titleLabel?.font = textFont } }
    }
    /// Text items color
    public var textColor: UIColor = UIColor.gray {
        didSet {
            if self.itemsArray.count == 0 { return }
            self.itemsArray.forEach { $0.setTitleColor(self.textColor, for: .normal) }
            let index = min(max(self.selectedIndex, 0), self.itemsArray.count - 1)
            let button = self.itemsArray[index]
            button.setTitleColor(self.textSelectedColor, for: .normal)
        }
    }
    /// Bounces past edge of content and back again. Default `false`. if `true`,
    public var bounces: Bool = false {
        didSet { scrollView.bounces = bounces }
    }
    /// Selected index, default `0`
    public var selectedIndex: Int = 0 {
        didSet {
            self.updateScrollViewOffset()
        }
    }
    /// Segment control selected scale, default value is`1.0`
    public var selectedScale: CGFloat = 1.0
    /// Segment control Insets
    public var insets: UIEdgeInsets = .zero
    
    // MARK: - Private Properties
    
    fileprivate var itemsArray = [UIButton]()
    fileprivate var coverView = UIView()
    fileprivate var slider = UIView()
    fileprivate var backroundSlider = UIView()
    
    fileprivate var totalItemsCount: Int = 0
    
    fileprivate var titleSources = [String]()
    fileprivate var imageSources: ([UIImage], [UIImage]) = ([], [])
    fileprivate var hybridSources: ([String?], [UIImage?], [UIImage?]) = ([], [], [])
    
    fileprivate var hybridStyle: HybridStyle = .normalWithSpace(0)
    fileprivate var resourceType: ResourceType = .text
    
    fileprivate var coverUpDownSpace: CGFloat = 0
    
    fileprivate var sliderConfig: (SliderPositionStyle, WidthStyle) = (.bottomWithHight(2), .adaptiveSpace(0))
    fileprivate var contentScrollViewWillDragging: Bool = false
    
    fileprivate var isSelectedItemButton: Bool = false
    
    fileprivate enum ResourceType {
        case text
        case image
        case hybrid
    }
    
    // MARK: - Initializers
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.setupContentView()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupContentView()
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        self.viewLayoutSubviews()
    }
    
    // MARK: - Public method
    
    // Set items titles
    /// Only text
    ///
    /// - Parameters:
    ///   - titles: text group
    ///   - style: The width style of the text is an enumeration, fixed width or adaptive
    public func setTitles(titles: [String], style: WidthStyle) {
        self.setTitleItems(titles: titles, style: style)
    }
    
    /// Selected items text color
    public var textSelectedColor: UIColor = UIColor.blue {
        didSet {
            if self.itemsArray.count == 0 { return }
            self.itemsArray.forEach { $0.setTitleColor(self.textSelectedColor, for: .normal) }
            let index = min(max(self.selectedIndex, 0), self.itemsArray.count - 1)
            let button = self.itemsArray[index]
            button.setTitleColor(self.textSelectedColor, for: .normal)
        }
    }
    
    /// Setup cover
    ///
    /// - Parameters:
    ///   - color: color backgroundColor
    ///   - upDowmSpace: the distance of cover's up/down from item's up/down
    ///   - cornerRadius: radius
    public func setCover(color: UIColor, upDowmSpace: CGFloat = 0, cornerRadius: CGFloat = 0) {
        self.coverView.isHidden = false
        self.coverView.layer.cornerRadius = cornerRadius
        self.coverView.backgroundColor = color
        self.coverUpDownSpace = upDowmSpace
        self.updateCoverAndSliderFrame(originFrame: self.coverView.frame, upSpace: upDowmSpace)
    }
    
    /// Use in contentScrollView `scrollViewDidScroll`
    ///
    /// - Parameter scrollView: content scrollView
    public func contentScrollViewDidScroll(_ scrollView: UIScrollView) {
        self.updateCoverAndSliderByContentScrollView(scrollView)
    }
    
    /// Use in contentScroll `scrollViewWillBeginDragging`
    public func contentScrollViewWillBeginDragging() {
        self.contentScrollViewWillDragging = true
    }
    
    /// Select segment on Segment Control.
    ///
    /// - Parameters:
    ///   - index: Selected segment index.
    ///   - animated: Flag select change animation.
    public func selectIndex(index: Int, animated: Bool) {
        guard index < self.itemsArray.count else { return }
        
        if animated {
            self.contentScrollViewWillDragging = false
            self.isSelectedItemButton = true
        }

        self.selectedIndex = index
    }
    
    /// Set slider
    ///
    /// - Parameters:
    ///   - color: slider color
    ///   - position: Deciding on the slider position up or down, an enumeration
    ///   - widthStyle: The width of the slider is an enumeration, fixed width or adaptive
    ///   - backgroundColor: slider backgroundColor
    public func setSilder(color: UIColor, position: SliderPositionStyle, widthStyle: WidthStyle, backgroundColor: UIColor = .clear) {
        var sliderFrame = self.slider.frame
        
        switch position {
        case .bottomWithHight(let height):
            sliderFrame.origin.y = frame.size.height - height
            sliderFrame.size.height = height
        case .topWidthHeight(let height):
            sliderFrame.origin.y = 0
            sliderFrame.size.height = height
        }
        
        self.slider.frame = sliderFrame
        self.slider.isHidden = false
        self.slider.backgroundColor = color
        
        self.backroundSlider.isHidden = false
        self.backroundSlider.backgroundColor = backgroundColor
        self.sliderConfig = (position, widthStyle)
    }
    
    // MARK: - Private Methods
    
    private func setTitleItems(titles: [String], style: WidthStyle) {
        self.resourceType = .text
        self.titleSources = titles
        self.totalItemsCount = titles.count
        
        switch style {
        case .fixedWidth(let width):
            self.setupItems(fixedWidth: width)
        case .adaptiveSpace(let space):
            self.setupItems(fixedWidth: 0, leading: space)
        }
    }
    
}

/// MARK: - Setup UI
extension CustomMaterialSegmentedControl {
    
    fileprivate func setupContentView() {
        self.scrollView.frame = bounds
        self.scrollView.showsHorizontalScrollIndicator = false
        self.scrollView.bounces = false
        
        self.addSubview(self.scrollView)
        self.scrollView.addSubview(self.coverView)
        self.scrollView.addSubview(self.backroundSlider)
        self.scrollView.addSubview(self.slider)
        
        self.slider.isHidden = true
        self.backroundSlider.isHidden = true
        self.coverView.isHidden = true
    }
    
    fileprivate func setupItems(fixedWidth: CGFloat, leading: CGFloat? = nil) {
        self.itemsArray.forEach { $0.removeFromSuperview() }
        self.itemsArray.removeAll()
        
        var contentSizeWidth: CGFloat = 0
        for i in 0..<self.totalItemsCount {
            var width = fixedWidth
            if let leading = leading {
                let text = self.titleSources[i] as NSString
                width = text.size(withAttributes: [.font: self.textFont]).width + leading * 2
            }
            
            let x = contentSizeWidth
            let height = frame.size.height
            let button = UIButton(type: .custom)
            
            button.frame = CGRect(x: x, y: 0, width: width, height: height)
            button.clipsToBounds = true
            button.tag = i
            button.addTarget(self, action: #selector(selectedButton(sender:)), for: .touchUpInside)
            
            self.scrollView.addSubview(button)
            self.itemsArray.append(button)
            
            switch resourceType {
            case .text:
                button.setTitle(self.titleSources[i], for: .normal)
                button.titleLabel?.numberOfLines = 2
                button.titleLabel?.textAlignment = .center
                button.setTitleColor(self.textColor, for: .normal)
                button.titleLabel?.font = textFont
            case .image:
                button.setImage(self.imageSources.0[i], for: .normal)
                button.setImage(self.imageSources.1[i], for: .selected)
            case .hybrid:
                guard let text = self.hybridSources.0[i], let image = hybridSources.1[i] else {
                    return
                }
                
                button.setTitleColor(self.textColor, for: .normal)
                button.titleLabel?.font = textFont
                button.setTitle(text, for: .normal)
                button.titleLabel?.numberOfLines = 2
                button.titleLabel?.textAlignment = .center
                button.setImage(image, for: .normal)
                button.setImage(hybridSources.2[i], for: .selected)
                
                switch hybridStyle {
                case .normalWithSpace(let space):
                    let distance = space / 2
                    button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -distance, bottom: 0, right: distance)
                    button.titleEdgeInsets = UIEdgeInsets(top: 0, left: distance, bottom: 0, right: -distance)
                case .imageTopWithSpace(let space):
                    let distance = space / 2
                    let titleWidth = text.size(withAttributes: [.font: textFont]).width
                    let titleHeight = text.size(withAttributes: [.font: textFont]).height
                    button.imageEdgeInsets = UIEdgeInsets(top: -titleHeight - distance, left: 0, bottom: 0, right: -titleWidth)
                    button.titleEdgeInsets = UIEdgeInsets(top: 0, left: -image.size.width, bottom: -image.size.height - distance, right: 0)
                case .imageBottomWithSpace(let space):
                    let distance = space / 2
                    let titleWidth = text.size(withAttributes: [.font: textFont]).width
                    let titleHeight = text.size(withAttributes: [.font: textFont]).height
                    button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: -titleHeight - distance, right: -titleWidth)
                    button.titleEdgeInsets = UIEdgeInsets(top: -image.size.height - distance, left: -image.size.width, bottom: 0, right: 0)
                }
            }
            contentSizeWidth += width
        }
        
        self.setScrollView(contentSizeWidth: contentSizeWidth)
    }
    
    private func setScrollView(contentSizeWidth: CGFloat) {
        let verticalInsets = self.insets.left + self.insets.right
        self.scrollView.contentSize.width = max(
            contentSizeWidth + verticalInsets * 2.0,
            UIScreen.main.bounds.width + verticalInsets * 4.0)
        
        self.scrollView.contentSize.height = 0
        
        if #available(iOS 11.0, *) {
            self.scrollView.contentInset = UIEdgeInsets(top: 0, left: insets.left, bottom: 0, right: insets.right)
        } else {
            self.scrollView.contentInset = UIEdgeInsets(top: 0, left: insets.left, bottom: 0, right: insets.right * 4)
        }
        
        let index = min(max(selectedIndex, 0), itemsArray.count - 1)
        if index >= 0 {
            let selectedButton = itemsArray[index]
            selectedButton.isSelected = true
            self.updateCoverAndSliderFrame(originFrame: selectedButton.frame, upSpace: coverUpDownSpace)
        }
    }
    
    @objc private func selectedButton(sender: UIButton) {
        self.contentScrollViewWillDragging = false
        self.isSelectedItemButton = true
        self.selectedIndex = sender.tag
    }
    
    private func viewLayoutSubviews() {
        self.scrollView.frame = self.bounds
        
        self.backroundSlider.frame.size.width = self.bounds.width
        self.backroundSlider.frame.size.height = 1.0
        self.backroundSlider.frame.alignmentX(
            to: self.bounds,
            fromAnchor: .left,
            toAnchor: .left)
        self.backroundSlider.frame.alignmentY(
            to: self.bounds,
            fromAnchor: .bottom,
            toAnchor: .bottom)
    }
    
}

/// update offset
extension CustomMaterialSegmentedControl {
    
    fileprivate func updateScrollViewOffset() {
        if self.itemsArray.count == 0 { return }
        
        let index = min(max(self.selectedIndex, 0), self.itemsArray.count - 1)
        self.delegate?.segmentedControlSelectedIndex(index, animated: isSelectedItemButton, segmentedControl: self)
        
        let currentButton = self.itemsArray[index]
        let offset = self.getScrollViewCorrectOffset(by: currentButton, index: index)
        let duration = self.isSelectedItemButton || self.contentScrollViewWillDragging
            ? 0.3 : 0
        self.isSelectedItemButton = false
        
        UIView.animate(withDuration: duration, animations: {
            self.itemsArray.forEach({ (button) in
                button.setTitleColor(self.textColor, for: .normal)
                button.isSelected = false
                button.transform = CGAffineTransform(scaleX: 1, y: 1)
            })
            
            self.updateCoverAndSliderFrame(originFrame: currentButton.frame, upSpace: self.coverUpDownSpace)
            currentButton.setTitleColor(self.textSelectedColor, for: .normal)
            currentButton.isSelected = true
            
            let scale = self.selectedScale
            currentButton.transform = CGAffineTransform(scaleX: scale, y: scale)
            
            let animated = duration == 0 ? false:true
            self.scrollView.setContentOffset(offset, animated: animated)
        })
    }
    
    fileprivate func getScrollViewCorrectOffset(by item: UIButton, index: Int) -> CGPoint {
        if self.scrollView.contentSize.width < self.scrollView.frame.size.width {
            return CGPoint.zero
        }
        
        var offsetx = item.center.x - frame.size.width / 2
        let offsetMax = self.scrollView.contentSize.width - frame.size.width - (insets.left + self.insets.right) * 2
        
        if offsetx < 0 {
            offsetx = 0
        } else if offsetx > offsetMax {
            offsetx = offsetMax
        }
        if index == 0 {
            offsetx -= self.insets.left
        } else if index == self.itemsArray.count - 1 {
            offsetx += self.insets.right
        }
        
        let offset = CGPoint(x: offsetx, y: 0)
        
        return offset
    }
    
}

// MARK: - Update Cover And Slider By Content
extension CustomMaterialSegmentedControl {
    
    fileprivate func updateCoverAndSliderByContentScrollView(_ scrollView: UIScrollView) {
        if !self.contentScrollViewWillDragging { return }
        
        let offset = scrollView.contentOffset.x / scrollView.frame.size.width
        let percent = offset - CGFloat(Int(offset))
        let currentIndex = Int(offset)
        var targetIndex = currentIndex
        
        if percent < 0 && currentIndex > 0 {
            targetIndex = currentIndex - 1
        } else if percent > 0 && currentIndex < itemsArray.count - 1 {
            targetIndex = currentIndex + 1
        } else {
            return
        }
        
        let currentButton = self.itemsArray[currentIndex]
        let targentButton = self.itemsArray[targetIndex]
        currentButton.transform = CGAffineTransform(scaleX: 1, y: 1)
        targentButton.transform = CGAffineTransform(scaleX: 1, y: 1)
        
        let centerXChange = (targentButton.center.x - currentButton.center.x) * abs(percent)
        let widthChange = (targentButton.frame.size.width - currentButton.frame.size.width) * abs(percent)
        
        var frame = currentButton.frame
        frame.size.width += widthChange
        self.updateCoverAndSliderFrame(originFrame: frame, upSpace: coverUpDownSpace)
        
        var center = currentButton.center
        center.x += centerXChange
        self.coverView.center = center
        
        var sliderCenter = self.slider.center
        sliderCenter.x = self.coverView.center.x
        self.slider.center = sliderCenter
        
        /// Scale
        let scale = (self.selectedScale - 1) * abs(percent)
        let targetTx = 1 + scale
        let currentTx = self.selectedScale - scale
        currentButton.transform = CGAffineTransform(scaleX: currentTx, y: currentTx)
        targentButton.transform = CGAffineTransform(scaleX: targetTx, y: targetTx)
        
        let currentColor = self.textSelectedColor.append(
            backgroundColor: self.textColor,
            alpha: abs(percent))
        let targetColor = self.textColor.append(
            backgroundColor: self.textSelectedColor,
            alpha: abs(percent))
        
        currentButton.setTitleColor(currentColor, for: .normal)
        targentButton.setTitleColor(targetColor, for: .normal)
    }
    
    fileprivate func updateCoverAndSliderFrame(originFrame: CGRect, upSpace: CGFloat) {
        var newFrame = originFrame
        newFrame.origin.y = upSpace
        newFrame.size.height -= upSpace * 2
        self.coverView.frame = newFrame
        
        switch sliderConfig.0 {
        case .topWidthHeight(let height):
            newFrame.origin.y = 0
            newFrame.size.height = height
        case .bottomWithHight(let height):
            newFrame.origin.y = originFrame.size.height - height
            newFrame.size.height = height
        }
        switch sliderConfig.1 {
        case .fixedWidth(let width):
            newFrame.size.width = width
        case .adaptiveSpace(let space):
            newFrame.size.width = originFrame.size.width - 2 * space
        }
        self.slider.frame = newFrame
        
        var sliderCenter = slider.center
        sliderCenter.x = coverView.center.x
        self.slider.center = sliderCenter
    }
    
}


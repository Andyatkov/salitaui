//
//  NewFeatureViewViewModel.swift
//  WBLibui
//
//  Created by Orlov Maxim on 19.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit
  
public struct NewFeatureViewViewModel {
    
    public let titleLabelText: String
    public let descriptionLabelText: String
    public let bottomLabelText: String
    public let image: UIImage?
    
    public init(titleLabelText: String,
                descriptionLabelText: String,
                bottomLabelText: String,
                image: UIImage?) {
        
        self.titleLabelText = titleLabelText
        self.descriptionLabelText = descriptionLabelText
        self.bottomLabelText = bottomLabelText
        self.image = image
    }
    
}

//
//  NewFeatureView.swift
//  WBLibui
//
//  Created by Orlov Maxim on 19.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public protocol NewFeatureViewDelegate: AnyObject {
    func continueButtonTapped()
}

public class NewFeatureView: UIView {
    
    // MARK: - Internal Properties
    
    open weak var delegate: NewFeatureViewDelegate?
    
    // MARK: - Private Properties
    
    private let titleLabel = UILabel()
    private let bottomLabel = UILabel()
    private let separatorView = UIView()
    private let imageView = UIImageView()
    private let descriptionLabel = UILabel()
    private let parameters = Layout.Parameters()
    
    // MARK: - Initializers
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        viewLayoutSubviews()
    }
    
    // MARK: - Internal Methods
    /// Sets configure view's labels with NewFeatureViewModel
    ///
    /// - Parameters:
    ///   - model: NewFeatureViewModel
    open func configureViewWith(_ model: NewFeatureViewViewModel) {
        
        titleLabel.text = model.titleLabelText
        descriptionLabel.text = model.descriptionLabelText
        bottomLabel.text = model.bottomLabelText
        imageView.image = model.image
    }
    
    // MARK: - Private Methods
    
    private func commonInit() {
        
        observe(theme: \DefaultTheme.view.newFeatureView,
                on: WBLibThemeManager.wblibuiManager)
        
        addSubviews()
        setupLabels()
    }
    
    private func addSubviews() {
        
        [imageView,
         titleLabel,
         separatorView,
         bottomLabel,
         descriptionLabel].forEach({
            
            addSubview($0)
        })
    }
    
    private func setupLabels() {
        
        titleLabel.numberOfLines = 1
        titleLabel.font = .boldSystemFont(ofSize: 20.0)
        
        descriptionLabel.numberOfLines = 2
        descriptionLabel.font = .systemFont(ofSize: 16.0)
        
        bottomLabel.numberOfLines = 1
        bottomLabel.textAlignment = .left
        bottomLabel.isUserInteractionEnabled = true
        bottomLabel.font = .systemFont(ofSize: 16.0, weight: .semibold)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(bottomLabelTapped))
        bottomLabel.addGestureRecognizer(tap)
    }
    
    @objc private func bottomLabelTapped() {
        delegate?.continueButtonTapped()
    }
    
}

extension NewFeatureView {
    
    struct Layout {
        struct Parameters {
            struct ImageView {
                let topSpacing: CGFloat = 16.0
                let rightSpacing: CGFloat = 16.0
                let width: CGFloat = 92.0
                let height: CGFloat = 72.0
            }
            
            struct TitleLabel {
                let topSpacing: CGFloat = 16.0
                let leftSpacing: CGFloat = 16.0
                let width: CGFloat = 92.0
                let height: CGFloat = 26.0
            }
            
            struct DescriptionLabel {
                let leftSpacing: CGFloat = 16.0
                let topSpacing: CGFloat = 4.0
                let height: CGFloat = 42.0
            }
            
            struct BottomLabel{
                let topSpacing: CGFloat = 4.0
                let leftSpacing: CGFloat = 16.0
                let rightSpacing: CGFloat = 39.0
                let height: CGFloat = 44.0
                let bottomSpacing: CGFloat = 8.0
            }
            
            struct SeparatorView {
                var height: CGFloat = 1.0
            }
            
            let titleLabel = TitleLabel()
            let descriptionLabel = DescriptionLabel()
            let imageView = ImageView()
            let separatorView = SeparatorView()
            let bottomLabel = BottomLabel()
            
            let insets = UIEdgeInsets.zero
        }
    }
    
    private func viewLayoutSubviews() {
        
        let params = parameters
        let insets = parameters.insets
        
        var bounds = CGRect(origin: .zero, size: frame.size)
        bounds = bounds.inset(by: insets)
        
        // Image View
        
        imageView.frame.size.width = params.imageView.width
        imageView.frame.size.height = params.imageView.height
        
        imageView.frame.alignmentX(to: bounds,
                                        fromAnchor: .right,
                                        toAnchor: .right,
                                        offset: -params.imageView.rightSpacing)
        imageView.frame.alignmentY(to: bounds,
                                        fromAnchor: .top,
                                        toAnchor: .top,
                                        offset: params.imageView.topSpacing)
        
        // Title Label
        
        titleLabel.frame.size.height = params.titleLabel.height
        titleLabel.frame.size.width = bounds.width
            - params.imageView.width
            - params.imageView.rightSpacing
            - params.titleLabel.leftSpacing
        
        titleLabel.frame.alignmentX(
            to: bounds,
            fromAnchor: .left,
            toAnchor: .left,
            offset: params.titleLabel.leftSpacing)
        titleLabel.frame.alignmentY(
            to: bounds,
            fromAnchor: .top,
            toAnchor: .top,
            offset: params.titleLabel.topSpacing)
        
        // Decription Label
        
        descriptionLabel.frame.size.height = params.descriptionLabel.height
        descriptionLabel.frame.size.width = bounds.width
        - params.imageView.width
        - params.imageView.rightSpacing
        - params.descriptionLabel.leftSpacing
        
        descriptionLabel.frame.alignmentX(
                   to: bounds,
                   fromAnchor: .left,
                   toAnchor: .left,
                   offset: params.descriptionLabel.leftSpacing)
        descriptionLabel.frame.alignmentY(
            to: titleLabel.frame,
            fromAnchor: .top,
            toAnchor: .bottom,
            offset: params.descriptionLabel.topSpacing)
        
        // Continue Button
        
        bottomLabel.frame.size.height = params.bottomLabel.height
        bottomLabel.frame.size.width = bounds.width
        - params.bottomLabel.rightSpacing
        - params.bottomLabel.leftSpacing
        
        bottomLabel.frame.alignmentX(
            to: bounds,
            fromAnchor: .left,
            toAnchor: .left,
            offset: params.bottomLabel.leftSpacing)
        bottomLabel.frame.alignmentY(
            to: descriptionLabel.frame,
            fromAnchor: .top,
            toAnchor: .bottom,
            offset: params.bottomLabel.topSpacing)
        
        // Separator View
        
        separatorView.frame.size.width = bounds.width
        separatorView.frame.size.height = parameters.separatorView.height
        separatorView.frame.alignmentY(to: bounds,
                                       fromAnchor: .bottom,
                                       toAnchor: .bottom)
    }
    
}

public struct NewFeatureViewTheme: ColorTheme {
    
    let backgroundColor: UIColor
    let separatorBackgroundColor: UIColor
    let titleLabelTextColor: UIColor
    let descriptionLabelTextColor: UIColor
    let bottomLabelTextColor: UIColor

    public init(palette: Palette) {
        
        backgroundColor = palette.colors.dynamic.wbWhite
        separatorBackgroundColor = palette.colors.dynamic.wbGray
        titleLabelTextColor = palette.colors.dynamic.wbBlack
        descriptionLabelTextColor = palette.colors.dynamic.wbDarkGray
        bottomLabelTextColor = palette.colors.dynamic.wbBrand
    }
    
}

extension NewFeatureView: Themeable {
    
    public typealias Theme = NewFeatureViewTheme

    public func apply(theme: Theme) {
        
        backgroundColor = theme.backgroundColor
        titleLabel.textColor = theme.titleLabelTextColor
        descriptionLabel.textColor = theme.descriptionLabelTextColor
        separatorView.backgroundColor = theme.separatorBackgroundColor
        bottomLabel.textColor = theme.bottomLabelTextColor
    }
    
}

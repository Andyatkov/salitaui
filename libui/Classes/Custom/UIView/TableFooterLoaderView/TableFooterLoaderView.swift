//
//  TableFooterLoaderView.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 30.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

/// View with loader for table view.
public class TableFooterLoaderView: UIView {
    
    // MARK: - Private Properties
    
    private var loaderView = UIActivityIndicatorView()
    
    //MARK: - Initializer
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commontInit()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        loaderView.frame.alignmentX(to: bounds,
                                    fromAnchor: .center,
                                    toAnchor: .center)
        loaderView.frame.alignmentY(to: bounds,
                                    fromAnchor: .center,
                                    toAnchor: .center)
    }
    
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private Methods
    
    private func commontInit() {
        setupActivityIndicatorView()
    }
    
    private func setupActivityIndicatorView() {
        
        addSubview(loaderView)
        
        loaderView.startAnimating()
        loaderView.style = .gray
    }
    
}

//
// PlainButton.swift
// libui
//
// Created by Andrey Dyatkov on 25.02.2020.
// Copyright © 2020 Andrey Dyatkov. All rights reserved.


import UIKit

open class PlainButton: CustomButton {
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        observe(theme: \DefaultTheme.button.plainButton, on: WBLibThemeManager.wblibuiManager)
        //TODO: - Change font
        titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
    }

}

// MARK: - Theme

public class PlainButtonTheme: ColorTheme {
    
    struct TitleColor {
        let normal: UIColor
        let disabled: UIColor
    }
    
    let titleColor: TitleColor

    public init(palette: Palette) {
        titleColor = TitleColor(normal: palette.colors.dynamic.wbBrand,
                                disabled: palette.colors.dynamic.wbDarkGray)
    }
    
}

extension PlainButton: Themeable {
    
    public typealias Theme = PlainButtonTheme

    public func apply(theme: Theme) {
        setTitleColor(theme.titleColor.normal, for: .normal)
        setTitleColor(theme.titleColor.disabled, for: .disabled)
    }
    
}

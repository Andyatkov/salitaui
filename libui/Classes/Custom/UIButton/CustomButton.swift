//
//  CustomButton.swift
//  libui
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

/// The base button class with extended properties for UIControl.State.
open class CustomButton: UIButton {

    // MARK: - Override states
    
    open override var isSelected: Bool {
        didSet { configure(for: state) }
    }
    
    open override var isHighlighted: Bool {
        get {
            return super.isHighlighted
        }
        set {
            highlight() { super.isHighlighted = newValue }
            configure(for: state)
        }
    }
    
    open override var isEnabled: Bool {
        didSet { configure(for: state) }
    }
    
    /// A Boolean value indicating whether the control highlight animated.
    open var shouldAnimateHighlighting: Bool = true
    
    /// Highlight animation duration
    open var animationDuration: TimeInterval = Durations.Default.single
    
    // MARK: - Private Properties
    
    private var buttonBackgroundColor = ButtonParameters<UIColor>()
    private var buttonShadowColor = ButtonParameters<UIColor>()
    private var buttonBorderColor = ButtonParameters<UIColor>()
    private var buttonAttributeTitleColor = ButtonParameters<UIColor>()
    private var buttonTintColor = ButtonParameters<UIColor>()
    
    private var buttonCornerRadius = ButtonParameters<CGFloat>()
    private var buttonBorderWidth = ButtonParameters<CGFloat>()
    private var buttonShadowOpacity = ButtonParameters<CGFloat>()
    private var buttonShadowRadius = ButtonParameters<CGFloat>()
    private var buttonShadowOffset = ButtonParameters<CGSize>()
    private var buttonShadowPath = ButtonParameters<CGPath>()
    
    // MARK: - Set Colors
    
    /// Sets the background color to use for the specified states.
    ///
    /// - Parameters:
    ///   - color: The background color to use for the specified state.
    ///   - states: The states that uses the specified background color. The possible values are described in UIControl.State.
    open func setBackgroundColor(_ color: UIColor?, for states: [UIControl.State]) {
        buttonBackgroundColor.set(parameter: color, for: states)
        updateIfNeeded(for: states)
    }
    
    /// Sets the shadow color to use for the specified states.
    ///
    /// - Parameters:
    ///   - color: The shadow color to use for the specified state.
    ///   - states: The states that uses the specified shadow color. The possible values are described in UIControl.State.
    open func setShadowColor(_ color: UIColor?, for states: [UIControl.State]) {
        buttonShadowColor.set(parameter: color, for: states)
        updateIfNeeded(for: states)
    }
    
    /// Sets the border color to use for the specified states.
    ///
    /// - Parameters:
    ///   - color: The border color to use for the specified state.
    ///   - states: The states that uses the specified border color. The possible values are described in UIControl.State.
    open func setBorderColor(_ color: UIColor?, for states: [UIControl.State]) {
        buttonBorderColor.set(parameter: color, for: states)
        updateIfNeeded(for: states)
    }
    
    /// Sets the attributed title color to use for the specified states.
    ///
    /// - Parameters:
    ///   - color: The attributed title color to use for the specified state.
    ///   - states: The states that uses the specified attributed title color. The possible values are described in UIControl.State.
    open func setAttributeTitleColor(_ color: UIColor?, for states: [UIControl.State]) {
        buttonAttributeTitleColor.set(parameter: color, for: states)
        updateIfNeeded(for: states)
    }
    
    // MARK: - Set Radius & Width
    
    /// Sets the corner radius to use for the specified states.
    ///
    /// - Parameters:
    ///   - radius: The corner radius to use for the specified state.
    ///   - states: The states that uses the specified corner radius. The possible values are described in UIControl.State.
    open func setCornerRadius(_ radius: CGFloat, for states: [UIControl.State]) {
        buttonCornerRadius.set(parameter: radius, for: states)
        updateIfNeeded(for: states)
    }
    
    /// Sets the border width to use for the specified states.
    ///
    /// - Parameters:
    ///   - width: The border width to use for the specified state.
    ///   - states: The states that uses the specified border width. The possible values are described in UIControl.State.
    open func setBorderWidth(_ width: CGFloat, for states: [UIControl.State]) {
        buttonBorderWidth.set(parameter: width, for: states)
        updateIfNeeded(for: states)
    }
    
    /// Sets the shadow opacity to use for the specified states.
    ///
    /// - Parameters:
    ///   - opacity: The shadow opacity to use for the specified state.
    ///   - states: The states that uses the specified shadow opacity. The possible values are described in UIControl.State.
    open func setShadowOpacity(_ opacity: CGFloat, for states: [UIControl.State]) {
        buttonShadowOpacity.set(parameter: opacity, for: states)
        updateIfNeeded(for: states)
    }
    
    /// Sets the shadow radius to use for the specified states.
    ///
    /// - Parameters:
    ///   - radius: The shadow radius to use for the specified state.
    ///   - states: The states that uses the specified shadow radius. The possible values are described in UIControl.State.
    open func setShadowRadius(_ radius: CGFloat, for states: [UIControl.State]) {
        buttonShadowRadius.set(parameter: radius, for: states)
        updateIfNeeded(for: states)
    }
    
    /// Sets the shadow offset to use for the specified states.
    ///
    /// - Parameters:
    ///   - offset: The shadow offset to use for the specified state.
    ///   - states: The states that uses the specified shadow offset. The possible values are described in UIControl.State.
    open func setShadowOffset(_ offset: CGSize, for states: [UIControl.State]) {
        buttonShadowOffset.set(parameter: offset, for: states)
        updateIfNeeded(for: states)
    }
    
    /// Sets the shadow path to use for the specified states.
    ///
    /// - Parameters:
    ///   - path: The shadow path to use for the specified state.
    ///   - states: The states that uses the specified shadow path. The possible values are described in UIControl.State.
    open func setShadowPath(_ path: CGPath, for states: [UIControl.State]) {
        buttonShadowPath.set(parameter: path, for: states)
        updateIfNeeded(for: states)
    }
    
    // MARK: - Set Title & Title Color & BackgroundImage
    
    /// Sets the title to use for the specified states.
    ///
    /// - Parameters:
    ///   - title: The title to use for the specified state.
    ///   - states: The states that uses the specified title. The possible values are described in UIControl.State.
    open func setTitle(_ title: String?, for states: [UIControl.State]) {
        states.forEach { super.setTitle(title, for: $0) }
    }
    
    
    /// Sets the attributed title to use for the specified states.
    ///
    /// - Parameters:
    ///   - title: The attributed title to use for the specified state.
    ///   - states: The states that uses the specified attributed title. The possible values are described in UIControl.State.
    open func setAttributedTitle(_ title: NSAttributedString?, for states: [UIControl.State]) {
        states.forEach { super.setAttributedTitle(title, for: $0) }
    }
    
    /// Sets the title color to use for the specified states.
    ///
    /// - Parameters:
    ///   - color: The title color to use for the specified state.
    ///   - states: The states that uses the specified title color. The possible values are described in UIControl.State.
    open func setTitleColor(_ color: UIColor?, for states: [UIControl.State]) {
        states.forEach { super.setTitleColor(color, for: $0) }
    }
    
    /// Sets background image to use for the specified states.
    ///
    /// - Parameters:
    ///   - image: The background image to use for the specified state.
    ///   - states: The states that uses the specified background image. The possible values are described in UIControl.State.
    open func setBackgroundImage(_ image: UIImage?, for states: [UIControl.State]) {
        states.forEach { super.setBackgroundImage(image, for: $0) }
    }
    
    /// Sets the tint color to use for the specified states.
    ///
    /// - Parameters:
    ///   - color: The tint color to use for the specified state.
    ///   - states: The states that uses the specified attributed title color. The possible values are described in UIControl.State.
    open func setTintColor(_ color: UIColor?, for states: [UIControl.State]) {
        buttonTintColor.set(parameter: color, for: states)
        updateIfNeeded(for: states)
    }
    
    // MARK: - Get Colors
    
    /// Returns the background color associated with the specified state.
    ///
    /// - Parameter state: The state that uses the background color. The possible values are described in UIControl.State.
    /// - Returns: The background color to use for the specified state.
    open func backgroundColor(for state: UIControl.State) -> UIColor? {
        return buttonBackgroundColor.parameter(for: state)
    }
    
    /// Returns the shadow color associated with the specified state.
    ///
    /// - Parameter state: The state that uses the shadow color. The possible values are described in UIControl.State.
    /// - Returns: The shadow color to use for the specified state.
    open func shadowColor(for state: UIControl.State) -> UIColor? {
        return buttonShadowColor.parameter(for: state)
    }
    
    /// Returns the border color associated with the specified state.
    ///
    /// - Parameter state: The state that uses the border color. The possible values are described in UIControl.State.
    /// - Returns: The border color to use for the specified state.
    open func borderColor(for state: UIControl.State) -> UIColor? {
        return buttonBorderColor.parameter(for: state)
    }
    
    /// Returns the attribute title color associated with the specified state.
    ///
    /// - Parameters state: The state that uses the attribute title. The possible values are described in UIControl.State.
    /// - Returns: The attribute title to use for the specified state.
    open func attributeTitleColor(for state: UIControl.State) -> UIColor? {
        return buttonAttributeTitleColor.parameter(for: state)
    }
    
    /// Returns the tint color associated with the specified state.
    ///
    /// - Parameters state: The state that uses the attribute title. The possible values are described in UIControl.State.
    /// - Returns: The tint color to use for the specified state.
    open func tintColor(for states: UIControl.State) -> UIColor {
        return buttonTintColor.parameter(for: state) ?? tintColor
    }
    
    // MARK: - Get Radius & Width
    
    /// Returns the corner radius associated with the specified state.
    ///
    /// - Parameter state: The state that uses the corner radius. The possible values are described in UIControl.State.
    /// - Returns: The corner radius to use for the specified state.
    open func cornerRadius(for state: UIControl.State) -> CGFloat {
        return buttonCornerRadius.parameter(for: state) ?? 0
    }
    
    /// Returns the border width associated with the specified state.
    ///
    /// - Parameter state: The state that uses the border width. The possible values are described in UIControl.State.
    /// - Returns: The border width to use for the specified state.
    open func borderWidth(for state: UIControl.State) -> CGFloat {
        return buttonBorderWidth.parameter(for: state) ?? 0
    }
    
    /// Returns the shadow opacity associated with the specified state.
    ///
    /// - Parameter state: The state that uses the shadow opacity. The possible values are described in UIControl.State.
    /// - Returns: The shadow opacity to use for the specified state.
    open func shadowOpacity(for state: UIControl.State) -> CGFloat {
        return buttonShadowOpacity.parameter(for: state) ?? 0
    }
    
    /// Returns the shadow radius associated with the specified state.
    ///
    /// - Parameter state: The state that uses the shadow radius. The possible values are described in UIControl.State.
    /// - Returns: The shadow radius to use for the specified state.
    open func shadowRadius(for state: UIControl.State) -> CGFloat {
        return buttonShadowRadius.parameter(for: state) ?? 0
    }
    
    /// Returns the shadow offset associated with the specified state.
    ///
    /// - Parameter state: The state that uses the shadow offset. The possible values are described in UIControl.State.
    /// - Returns: The shadow offset to use for the specified state.
    open func shadowOffset(for state: UIControl.State) -> CGSize {
        return buttonShadowOffset.parameter(for: state) ?? .zero
    }
    
    /// Returns the shadow path with the specified state.
    ///
    /// - Parameter state: The state that uses the shadow path. The possible values are described in UIControl.State.
    /// - Returns: The shadow path to use for the specified state.
    open func shadowPath(for state: UIControl.State) -> CGPath? {
        return buttonShadowPath.parameter(for: state)
    }
    
    // MARK: - Configure Methods
    
    func configure(for state: UIControl.State) {
        layer.backgroundColor = backgroundColor(for: state)?.cgColor
        layer.shadowColor = shadowColor(for: state)?.cgColor
        layer.borderColor = borderColor(for: state)?.cgColor
        layer.cornerRadius = cornerRadius(for: state)
        layer.borderWidth = borderWidth(for: state)
        layer.shadowRadius = shadowRadius(for: state)
        layer.shadowOffset = shadowOffset(for: state)
        layer.shadowOpacity = Float(shadowOpacity(for: state))
        layer.shadowPath = shadowPath(for: state)
        
        tintColor = tintColor(for: state)
        
        configureAttributedTitle(for: state)
    }
    
    func updateIfNeeded(for newStates: [UIControl.State]) {
        guard newStates.contains(state) else { return }
        configure(for: state)
    }
    
    func highlight(animation: @escaping () -> Void) {
        guard shouldAnimateHighlighting else {
            animation()
            return
        }
        
        UIView.transition(
            with: self,
            duration: animationDuration,
            options: [.transitionCrossDissolve, .allowAnimatedContent, .allowUserInteraction],
            animations: animation,
            completion: nil)
    }
    
    // MARK: - Private Methods
    
    private func configureAttributedTitle(for state: UIControl.State) {
        guard
            let color = buttonAttributeTitleColor.parameter(for: state),
            let attributedTitle = attributedTitle(for: state)
        else { return }
        
        let attributes = [NSAttributedString.Key.foregroundColor: color]
        setAttributedTitle(NSAttributedString(string: attributedTitle.string, attributes: attributes), for: state)
    }

}


// MARK: - ButtonParameters

extension CustomButton {

    struct ButtonParameters<Parameter> {
        var normal: Parameter?
        var highlighted: Parameter?
        var selected: Parameter?
        var disabled: Parameter?
        
        init(normal: Parameter? = nil,
             highlighted: Parameter? = nil,
             disabled: Parameter? = nil,
             selected: Parameter? = nil) {
            self.normal = normal
            self.highlighted = highlighted
            self.disabled = disabled
            self.selected = selected
        }
        
        mutating func set(parameter: Parameter?, for states: [UIControl.State]) {
            for state in states {
                switch state {
                case .normal:
                    normal = parameter
                case .highlighted:
                    highlighted = parameter
                case .selected:
                    selected = parameter
                case .disabled:
                    disabled = parameter
                default:
                    break
                }
            }
        }
        
        func parameter(for state: UIControl.State) -> Parameter? {
            switch state {
            case .normal:
                return normal
            case .highlighted:
                return highlighted ?? normal
            case .selected:
                return selected ?? normal
            case .disabled:
                return disabled ?? normal
            default:
                return nil
            }
        }
    }

}

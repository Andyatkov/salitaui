//
//  FillButton.swift
//  libui
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

open class FillButton: CustomButton {
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    func commonInit() {
        
        observe(theme: \DefaultTheme.button.fillButton, on: WBLibThemeManager.wblibuiManager)
        setCornerRadius(.defaultCornerRadius, for: [.normal])
    }

}

// MARK: - Theme

public class FillButtonTheme: ColorTheme {
    
    struct TitleColor {
        let normal: UIColor
        let disabled: UIColor
    }
    
    struct BackgroundColor {
        let normal: UIColor
        let highlighted: UIColor
        let disabled: UIColor
    }
    
    let titleColor: TitleColor
    let backgroundColor: BackgroundColor

    public init(palette: Palette) {
        titleColor = TitleColor(normal: palette.colors.dynamic.wbWhite,
                                disabled: palette.colors.dynamic.wbDarkGray)
        backgroundColor = BackgroundColor(normal: palette.colors.dynamic.wbBrand,
                                          highlighted: palette.colors.dynamic.wbBrand.lighter(by: 0.3),
                                          disabled: palette.colors.dynamic.wbGray)
    }
    
}

extension FillButton: Themeable {
    
    public typealias Theme = FillButtonTheme

    public func apply(theme: Theme) {
        setTitleColor(theme.titleColor.normal, for: .normal)
        setTitleColor(theme.titleColor.disabled, for: .disabled)
        
        setBackgroundColor(theme.backgroundColor.normal, for: [.normal])
        setBackgroundColor(theme.backgroundColor.highlighted, for: [.highlighted])
        setBackgroundColor(theme.backgroundColor.disabled, for: [.disabled])
    }
    
}

//
//  BorderButton.swift
//  libui
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

open class BorderButton: CustomButton {
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        
        observe(theme: \DefaultTheme.button.borderButton, on: WBLibThemeManager.wblibuiManager)
        setBorderWidth(1.0, for: [.normal])
        setCornerRadius(.defaultCornerRadius, for: [.normal])
    }

}

// MARK: - Theme

public class BorderButtonTheme: ColorTheme {
    
    struct TitleColor {
        let normal: UIColor
        let highlighted: UIColor
        let disabled: UIColor
    }
    
    struct BackgroundColor {
        let normal: UIColor
    }
    
    struct BorderColor {
        let normal: UIColor
        let highlighted: UIColor
        let disabled: UIColor
    }
    
    let titleColor: TitleColor
    let backgroundColor: BackgroundColor
    let borderColor: BorderColor

    public init(palette: Palette) {
        titleColor = TitleColor(normal: palette.colors.dynamic.wbBrand,
                                highlighted: palette.colors.dynamic.wbBrand.lighter(by: 0.3),
                                disabled: palette.colors.dynamic.wbDarkGray)
        backgroundColor = BackgroundColor(normal: palette.colors.dynamic.wbWhite)
        borderColor = BorderColor(normal: palette.colors.dynamic.wbBrand,
                                  highlighted: palette.colors.dynamic.wbBrand.lighter(by: 0.3),
                                  disabled: palette.colors.dynamic.wbDarkGray)
    }
    
}

extension BorderButton: Themeable {
    
    public typealias Theme = BorderButtonTheme

    public func apply(theme: Theme) {
        setTitleColor(theme.titleColor.normal, for: .normal)
        setTitleColor(theme.titleColor.highlighted, for: .highlighted)
        setTitleColor(theme.titleColor.disabled, for: .disabled)
        
        setBackgroundColor(theme.backgroundColor.normal, for: [.normal])
        
        setBorderColor(theme.borderColor.normal, for: [.normal])
        setBorderColor(theme.borderColor.highlighted, for: [.highlighted])
        setBorderColor(theme.borderColor.disabled, for: [.disabled])
    }
    
}


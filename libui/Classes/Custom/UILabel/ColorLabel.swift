//
//  ColorLabel.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 16.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

/// The label having basic and highlighted colors.
open class ColorLabel: UILabel {
    
    public enum Style {
        case `default`
    }
    
    // MARK: - Colored
    
    override open var font: UIFont! {
        get {
            return super.font
        }
        set {
            super.font = newValue
            update()
        }
    }
    
    // MARK: Public Properties
    
    open var style: Style = .default {
        didSet { update() }
    }
    
    override open var text: String? {
        didSet { update() }
    }

    // The text that will be highlighted by different color.
    open var extraText: String? {
        didSet { update() }
    }
    
    open override var isEnabled: Bool {
        didSet { update() }
    }
    
    // MARK: - Initializers
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Internal Methods
    
    func update() {}
    
}

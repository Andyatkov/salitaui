//
//  CurrencyLabel.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 16.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

// MARK: - Private Extension

private extension String {
    static let point = "."
    static let comma = ","
}

/// The label that display currency. It can highlight currency (separated by point or comma) by two colors.
open class CurrencyLabel: ColorLabel {
    
    // MARK: - Public Properties
    
    open override var text: String? {
        didSet { updateText() }
    }
    
    // MARK: Internal Properties
    
    var theme: CurrencyLabelTheme? {
        didSet { update() }
    }
    
    // MARK: - Initializers
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override func update() {
        guard let text = text,
            !text.isEmpty,
            let font = font,
            let theme = theme else {
            return
        }
        
        var mainColor: UIColor
        var extraColor: UIColor
        
        switch style {
        case .default:
            mainColor = isEnabled ? theme.mainColor.enabled : theme.mainColor.disabled
            extraColor = isEnabled ? theme.extraColor.enabled : theme.extraColor.disabled
        }
        
        let attributes: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.foregroundColor: mainColor,
            NSAttributedString.Key.font: font]
        
        let attributedString = NSMutableAttributedString(string: text, attributes: attributes)
        
        if let extraText = extraText, !extraText.isEmpty {
            attributedString.setColorForText(textForAttribute: extraText, withColor: extraColor)
        }
        
        attributedText = attributedString
    }
    
    // MARK: - Private Methods
    
    private func commonInit() {
        
        observe(theme: \DefaultTheme.label.currencyLabelTheme, on: WBLibThemeManager.wblibuiManager)
        font = UIFont.systemFont(ofSize: 17.0, weight: .regular)
    }
    
    // Find separator and set extraText
    private func updateText() {
        guard let text = text, !text.isEmpty else {
            return
        }
    
        let separationByPointStrings = text.components(separatedBy: String.point)
        let separationByСommaStrings = text.components(separatedBy: String.comma)
        
        if separationByPointStrings.count > 1 {
            extraText = .point + separationByPointStrings[1]
        } else if separationByСommaStrings.count > 1 {
            extraText = .comma + separationByСommaStrings[1]
        }
    }
       
}

// MARK: - Theme

public class CurrencyLabelTheme: ColorTheme {
    
    struct MainColor {
        let enabled: UIColor
        let disabled: UIColor
    }
    
    struct ExtraColor {
        let enabled: UIColor
        let disabled: UIColor
    }
    
    let mainColor: MainColor
    let extraColor: ExtraColor

    public init(palette: Palette) {
        mainColor = MainColor(enabled: palette.colors.dynamic.wbGreen, disabled: palette.colors.dynamic.wbDarkGray)
        extraColor = ExtraColor(enabled: palette.colors.dynamic.wbGreen.lighter(by: 0.5), disabled: palette.colors.dynamic.wbGray)
    }
    
}

extension CurrencyLabel: Themeable {
    
    public typealias Theme = CurrencyLabelTheme

    public func apply(theme: Theme) {
        self.theme = theme
    }
    
}


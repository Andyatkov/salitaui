//
//  CollectionTextFieldAction.swift
//  WBLibui
//
//  Created by Orlov Maxim on 16.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import Foundation

public enum CollectionTextFieldAction {
    
    case beginEditing
    case change(text: String)
    case endEditing
    case shouldReturn
    case other(data: Any)
    
}

//
//  StrictTextFieldCollectionCellViewModel.swift
//  WBLibui
//
//  Created by Orlov Maxim on 16.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

/// ViewModel fot StrictTextFieldCollectionCell
public struct StrictTextFieldCollectionCellViewModel {
    
    public let placeholder: String
    public let contentText: String?
    public let isEnabled: Bool
    public let keyboardType: UIKeyboardType
    
    public init(placeholder: String,
                contentText: String?,
                isEnabled: Bool = true,
                keyboardType: UIKeyboardType = .default) {
        
        self.placeholder = placeholder
        self.contentText = contentText
        self.isEnabled = isEnabled
        self.keyboardType = keyboardType
    }
    
}

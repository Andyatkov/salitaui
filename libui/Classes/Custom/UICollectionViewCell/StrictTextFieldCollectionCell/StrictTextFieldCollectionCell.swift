//
//  StrictTextFieldCollectionCell.swift
//  WBLibui
//
//  Created by Orlov Maxim on 15.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

/// CollectionViewCell with StrictTextField inside
public class StrictTextFieldCollectionCell: UICollectionViewCell {
    
    // MARK: - Private Properties
    
    private let textField = StrictTextField()
    private var actionHandler: CollectionActionClosure?
    
    // MARK: - Initializers
    
    override public var isSelected: Bool {
        didSet {
            guard isSelected else { return }
            
            textField.becomeFirstResponder()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private Methods
    
    private func setup() {
        
        addSubviews()
        setTextField()
        addConstraints()
    }
    
    private func addSubviews() {
        
        [textField].forEach({
            
            contentView.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        })
    }
    
    private func addConstraints() {
        
        NSLayoutConstraint.activate([
        
            textField.topAnchor.constraint(equalTo: contentView.topAnchor),
            textField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            textField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            textField.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
    
    private func setTextField() {
        textField.delegate = self
    }
    
}

// MARK: - ReusableViewProtocol

extension StrictTextFieldCollectionCell: ReusableViewProtocol {
    
    public func configure(viewModel: StrictTextFieldCollectionCellViewModel) {
        
        UIView.performWithoutAnimation {
            self.textField.keyboardType = viewModel.keyboardType
            self.textField.placeholder = viewModel.placeholder
            self.textField.text = viewModel.contentText
            self.textField.isEnabled = viewModel.isEnabled
        }
    }
    
}

// MARK: - MaterialTextFieldDelegate

extension StrictTextFieldCollectionCell: MaterialTextFieldDelegate {
    
    public func textFieldDidBeginEditing(_ textField: MaterialTextField) {
        actionHandler?(CollectionTextFieldAction.beginEditing)
    }
    
    public func textField(_ textField: MaterialTextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard
            let textFieldText = textField.text,
            let textRange = Range(range, in: textFieldText)
            else { return true }
        
        let updatedText = textFieldText.replacingCharacters(in: textRange, with: string)
        
        actionHandler?(CollectionTextFieldAction.change(text: updatedText))
        
        return true
    }
    
    public func textFieldDidEndEditing(_ textField: MaterialTextField) {
        actionHandler?(CollectionTextFieldAction.endEditing)
    }
    
    public func textFieldShouldReturn(_ textField: MaterialTextField) -> Bool {
        actionHandler?(CollectionTextFieldAction.shouldReturn)
        return true
    }
    
}

// MARK: - CollectionActionHandlerProtocol

extension StrictTextFieldCollectionCell: CollectionActionHandlerProtocol {
    
    public func set(actionHandler: CollectionActionClosure?) {
        self.actionHandler = actionHandler
    }
    
}
 
// MARK: - ColorTheme

public struct StrictTextFieldCollectionCellCustomTheme: ColorTheme {
    let backgroundColor: UIColor
    
    public init(palette: Palette) {
        backgroundColor = palette.colors.dynamic.wbWhite
    }
    
}

extension StrictTextFieldCollectionCell: Themeable {
    
    public typealias Theme = SeparatorViewTheme

    public func apply(theme: Theme) {
        
        backgroundColor = theme.backgroundColor
    }
    
}

//
//  PhoneTextFieldCollectionCell.swift
//  WBLibui
//
//  Created by Orlov Maxim on 16.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public class PhoneTextFieldCollectionCell: UICollectionViewCell {
    
    // MARK: - Private Properties
    
    private let textField = PhoneTextField()
    private var actionHandler: CollectionActionClosure?
    
    // MARK: - Initializers
    
    override public var isSelected: Bool {
        didSet {
            guard isSelected else { return }
            
            textField.becomeFirstResponder()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods
    
    private func setup() {
        
        addSubviews()
        setTextField()
        addConstraints()
    }
    
    private func addSubviews() {
        
        [textField].forEach({
            
            contentView.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        })
    }
    
    private func addConstraints() {
        
        NSLayoutConstraint.activate([
        
            textField.topAnchor.constraint(equalTo: contentView.topAnchor),
            textField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            textField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            textField.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
    
    private func setTextField() {
        
        textField.autoreplaceRussianCountryCode = true
        textField.delegate = self
    }
    
}

// MARK: - ReusableViewProtocol

extension PhoneTextFieldCollectionCell: ReusableViewProtocol {
    
    public func configure(viewModel: PhoneTextFieldCollectionCellViewModel) {
        
        UIView.performWithoutAnimation {
            self.textField.placeholder = viewModel.placeholder
            self.textField.text = viewModel.contentText
            self.textField.isEnabled = viewModel.isEnabled
        }
    }
    
}

// MARK: - MaterialTextFieldDelegate

extension PhoneTextFieldCollectionCell: MaterialTextFieldDelegate {
    
    public func textFieldDidBeginEditing(_ textField: MaterialTextField) {
        actionHandler?(CollectionTextFieldAction.beginEditing)
    }
    
    public func textField(_ textField: MaterialTextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard
            let textFieldText = textField.text,
            let textRange = Range(range, in: textFieldText)
            else { return true }
        
        let updatedText = textFieldText.replacingCharacters(in: textRange, with: string)
        
        actionHandler?(CollectionTextFieldAction.change(text: updatedText))
        
        return true
    }
    
    public func textFieldDidEndEditing(_ textField: MaterialTextField) {
        
        actionHandler?(CollectionTextFieldAction.endEditing)
    }
    
}

// MARK: - CollectionActionHandlerProtocol

extension PhoneTextFieldCollectionCell: CollectionActionHandlerProtocol {
    
    public func set(actionHandler: CollectionActionClosure?) {
        self.actionHandler = actionHandler
    }
    
}

// MARK: - ColorTheme

public struct PhoneTextFieldCollectionCellCustomTheme: ColorTheme {
    let backgroundColor: UIColor
    
    public init(palette: Palette) {
        backgroundColor = palette.colors.dynamic.wbWhite
    }
    
}

extension PhoneTextFieldCollectionCell: Themeable {
    
    public typealias Theme = SeparatorViewTheme

    public func apply(theme: Theme) {
        
        backgroundColor = theme.backgroundColor
    }
    
}

//
//  PhoneTextFieldCollectionCellViewModel.swift
//  WBLibui
//
//  Created by Orlov Maxim on 16.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import Foundation

/// ViewModel fot StrictTextField
public struct PhoneTextFieldCollectionCellViewModel {
    
    public let placeholder: String
    public let contentText: String?
    public let isEnabled: Bool
    
    public init(placeholder: String,
                contentText: String?,
                isEnabled: Bool = true) {
        self.placeholder = placeholder
        self.contentText = contentText
        self.isEnabled = isEnabled
    }
    
}

//
//  CustomSwitch.swift
//  libui
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

/// A control that offers a binary choice, such as On/Off.
open class CustomSwitch: UISwitch {
    
    // MARK: - Initializers
    
    override public init(frame: CGRect) {
        super.init(frame: .zero)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    // MARK: - Private Methods
    
    private func commonInit() {
        
        observe(theme: \DefaultTheme.switch.customSwitch, on: WBLibThemeManager.wblibuiManager)
        clipsToBounds = true
    }
    
    // MARK: - Internal Methods
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        
        let diameter = min(frame.size.height, frame.size.width)
        layer.cornerRadius = ceil(diameter / 2.0)
    }

}

// MARK: - Theme

public class CustomSwitchTheme: ColorTheme {

    let tintColor: UIColor
    let onTintColor: UIColor
    
    public init(palette: Palette) {
        tintColor = palette.colors.dynamic.wbDarkGray
        onTintColor = palette.colors.dynamic.wbBrand
    }
    
}

extension CustomSwitch: Themeable {
    
    public typealias Theme = CustomSwitchTheme

    public func apply(theme: Theme) {
        tintColor = theme.tintColor
        onTintColor = theme.onTintColor
    }
    
}


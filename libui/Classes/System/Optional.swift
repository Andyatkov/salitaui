//
//  Optional.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import Foundation

infix operator !! : NilCoalescingPrecedence

/// Force-unwraps `value` with a custom error message.
/// - parameters:
///   - value: The value to be force-unwrapped.
///   - panic: The panic function to be called in case of failure.
/// - returns: The unwrapped value.
public func !! <Value>(_ value: Value?, _ panic: @autoclosure () -> Never) -> Value {
    
    guard let value = value else {
        panic()
    }
    
    return value
}

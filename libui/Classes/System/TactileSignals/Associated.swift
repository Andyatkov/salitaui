//
//  Associated.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import Foundation

private class Associated<T>: NSObject {
    
    let value: T
    
    init(value: T) {
        self.value = value
    }
    
}

protocol Associable {
    //
}

extension Associable where Self: AnyObject {
    
    func getAssociatedObject<T> (key: UnsafeRawPointer) -> T? {
        return (objc_getAssociatedObject(self, key) as? Associated<T>).map { $0.value }
    }
    
    func setAssociatedObject<T> (key: UnsafeRawPointer, value: T?) {
        (objc_setAssociatedObject(self,
                                  key,
                                  value.map { Associated<T>(value: $0) },
                                  .OBJC_ASSOCIATION_RETAIN))
    }
    
}

extension NSObject: Associable {
    //
}

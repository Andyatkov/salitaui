//
//  Tactile+Hashable.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

extension UIControl.Event: Hashable {
        
    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.rawValue)
    }
    
}

func == (lhs: UIControl.Event, rhs: UIControl.Event) -> Bool {
    return lhs.rawValue == rhs.rawValue
}

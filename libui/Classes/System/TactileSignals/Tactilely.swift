//
//  Tactilely.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

private var tactileKey: Void?
private var eventKey: Void?
private var targetsKey: Void?

 /// Protocol for adding a tactile event
 public protocol Tactilely: class {
     func trigger(_ sender: Any)
}

extension Tactilely where Self: UIControl {
    
    public var isTactile: Bool {
        get {
            guard let actions = actions(forTarget: self, forControlEvent: tactileControlEvents ?? .touchDown ) else {
                return false
            }
            
            return !actions.filter { $0 == #selector(trigger).description }.isEmpty
            
        }
    }
    
    public var tactileType: TactileSignals? {
        get { return getAssociatedObject(key: &tactileKey) }
        set { setAssociatedObject(key: &tactileKey, value: newValue) }
    }
    
    public var tactileControlEvents: UIControl.Event? {
        get { return getAssociatedObject(key: &eventKey) }
        set { setAssociatedObject(key: &eventKey, value: newValue) }
    }
    
    private var tactileTargets: [UIControl.Event: TactileTarget] {
        get { return getAssociatedObject(key: &targetsKey) ?? [:] }
        set { setAssociatedObject(key: &eventKey, value: newValue) }
    }
    
    public func addTactile(_ tactile: TactileSignals, forControleEvents event: UIControl.Event) {
        let tactileTarget = TactileTarget(tactile: tactile)
        tactileTargets[event] = tactileTarget
        addTarget(tactileTarget, action: #selector(tactileTarget.trigger), for: event)
    }
    
    public func removeTactile(forControlEvents event: UIControl.Event) {
        guard let tactileTarget = tactileTargets[event] else { return }
        tactileTargets[event] = nil
        removeTarget(tactileTarget, action: #selector(tactileTarget.trigger), for: event)
    }
}

extension UIControl: Tactilely {
    @objc public func trigger(_ sender: Any) {
        tactileType?.tactileSignals()
    }
}


private class TactileTarget {
    let tactile: TactileSignals
   
    init(tactile: TactileSignals) {
        self.tactile = tactile
    }
    
    @objc func trigger(_ sender: Any) {
        tactile.tactileSignals()
    }
}


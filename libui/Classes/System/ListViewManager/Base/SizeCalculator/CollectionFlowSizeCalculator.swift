//
//  CollectionFlowSizeCalculator.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public class CollectionFlowSizeCalculator {
    
    public weak var collectionView: UICollectionView?
    
    public var cells: [String: UICollectionViewCell] = [:]
    public var supplementaries: [String: UICollectionReusableView] = [:]
    
    public var forceLayout: Bool = true
    public var stretchToWidth: Bool = true
    public var ceilResult: Bool = true
    
    
    public init(collectionView: UICollectionView? = nil) {
        self.collectionView = collectionView
    }
    
    public func register(cellTypes: [UICollectionViewCell.Type]) {
        cellTypes.forEach() { self.register(cellType: $0) }
    }
    
    public func register(supplementaryTypes: [(SupplementaryKind, UICollectionReusableView.Type)]) {
        supplementaryTypes.forEach() { self.register(supplementaryType: $0.1, kind: $0.0) }
    }
    
    public func register(cellType: UICollectionViewCell.Type) {
        guard let collectionView = collectionView else {
            preconditionFailure("CollectionFlowSizeCalculator couldn't register cells, because collectionView is nil.")
        }
        
        let reuseIdentifier = String(describing: cellType)
        let cell = cellType.loadFromNib() ?? cellType.init(frame: .zero)
    
        cells[reuseIdentifier] = cell
        collectionView.registerCell(cellType, reuseIdentifier: reuseIdentifier)
    }
    
    public func register(supplementaryType: UICollectionReusableView.Type, kind: SupplementaryKind) {
        guard let collectionView = collectionView else {
            preconditionFailure("CollectionFlowSizeCalculator couldn't register cells, because collectionView is nil.")
        }
        
        let kind = kind.kind(type: .collection)
        let reuseIdentifier = String(describing: supplementaryType)
        let reuseIdentifierKey = reuseIdentifier + kind
        
        let view = supplementaryType.loadFromNib() ?? supplementaryType.init(frame: .zero)

        self.supplementaries[reuseIdentifierKey] = view
        collectionView.registerSupplementary(supplementaryType, kind: kind, reuseIdentifier: reuseIdentifier)
    }
    
    public func calculateCellSize(for item: ListItem) -> CGSize {
        guard let cell = self.cells[item.reuseIdentifier] else { return .zero }
        
        return self.calculateSize(for: item, view: cell)
    }
    
    public func calculateSupplementarySize(for item: ListItem, kind: SupplementaryKind) -> CGSize {
        let reuseIdentifierKey = item.reuseIdentifier + kind.kind(type: .collection)
        guard let view = self.supplementaries[reuseIdentifierKey] else { return .zero }
        
        return self.calculateSize(for: item, view: view)
    }
    
    func calculateSize(for item: ListItem, view: UIView) -> CGSize {
        guard let collectionView = collectionView else { return .zero }
        
        item.configure(instance: view)
        
        if forceLayout {
            view.setNeedsLayout()
            view.layoutIfNeeded()
        }
        
        var size = CGSize(width: collectionView.frame.width, height: 0)
        
        if stretchToWidth {
            size.height = view.systemLayoutSizeFitting(size).height
        } else {
            size = view.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        }
        
        if ceilResult {
            return ceil(size)
        }
        
        return size
    }
    
}

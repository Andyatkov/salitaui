//
//  TableSizeCalculator.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public class TableSizeCalculator {
    
    public weak var tableView: UITableView?
    
    public var cells: [String: UITableViewCell] = [:]
    public var supplementaries: [String: UITableViewHeaderFooterView] = [:]
    
    public var forceLayout: Bool = true
    public var ceilResult: Bool = true
    
    public init(tableView: UITableView? = nil) {
        self.tableView = tableView
    }
    
    public func register(cellTypes: [UITableViewCell.Type]) {
        cellTypes.forEach() { register(cellType: $0) }
    }
    
    public func register(supplementaryTypes: [UITableViewHeaderFooterView.Type]) {
        supplementaryTypes.forEach() { register(supplementaryType: $0) }
    }
    
    public func register(cellType: UITableViewCell.Type) {
        guard let tableView = tableView else {
            preconditionFailure("TableSizeCalculator couldn't register cells, because tableView is nil.")
        }
        
        let reuseIdentifier = String(describing: cellType)
        let cell = cellType.loadFromNib() ?? cellType.init(frame: .zero)
        
        cells[reuseIdentifier] = cell
        tableView.registerCell(cellType, reuseIdentifier: reuseIdentifier)
    }
    
    public func register(supplementaryType: UITableViewHeaderFooterView.Type) {
        guard let tableView = tableView else {
            preconditionFailure("TableSizeCalculator couldn't register cells, because tableView is nil.")
        }
        
        let reuseIdentifier = String(describing: supplementaryType)
        let view = supplementaryType.loadFromNib() ?? supplementaryType.init(frame: .zero)
        
        supplementaries[reuseIdentifier] = view
        tableView.registerSupplementary(supplementaryType, reuseIdentifier: reuseIdentifier)
    }
    
    public func calculateCellSize(for item: ListItem) -> CGSize {
        guard let cell = cells[item.reuseIdentifier] else { return .zero }
        return calculateSize(for: item, view: cell)
    }
    
    public func calculateSupplementarySize(for item: ListItem) -> CGSize {
        guard let view = supplementaries[item.reuseIdentifier] else { return .zero }
        return calculateSize(for: item, view: view)
    }
    
    func calculateSize(for item: ListItem, view: UIView) -> CGSize {
        guard let tableView = tableView else { return .zero }
        
        item.configure(instance: view)
        
        if forceLayout {
            view.setNeedsLayout()
            view.layoutIfNeeded()
        }
        
        var size = CGSize(width: tableView.frame.width, height: 0)
        size.height = view.systemLayoutSizeFitting(size).height
        
        if ceilResult {
            return ceil(size)
        }
        
        return size
    }
    
}



//
//  TableViewManager.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

@objc public protocol TableViewManagerDelegate: UIScrollViewDelegate {
    // Data Source
    @objc optional func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
    @objc optional func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath)
    @objc optional func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    @objc optional func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool
    @objc optional func tableView(_ tableView: UITableView, willBeginEditingRowAt: IndexPath)
    @objc optional func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?)
    @objc optional func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt: IndexPath) -> String?
    
    // Delegate
    @objc optional func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool
    @objc optional func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    @objc optional func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath)
    @objc optional func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    @objc optional func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    @objc optional func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int)
    @objc optional func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath)
    @objc optional func tableView(_ tableView: UITableView, didEndDisplayingHeaderView view: UIView, forSection section: Int)
    @objc optional func tableView(_ tableView: UITableView, didEndDisplayingFooterView view: UIView, forSection section: Int)
    @objc optional func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    @objc optional func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    @objc optional func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    @objc optional func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    @objc optional func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle
    @objc optional func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool
}

/// Base collection manager class to simplify work with UITableView.
public class TableViewManager<Storage>: ListViewManager<Storage>, UITableViewDelegate, UITableViewDataSource where
             Storage: ListStorageProtocol {

    /// Delegate that handle all UITableViewDelegate methods forwarded by TableViewManager.
    public weak var tableViewDelegate: TableViewManagerDelegate?
    
    override var scrollViewDelegate: UIScrollViewDelegate? {
        return self.tableViewDelegate
    }
    
    /// The collection view.
    /// After setting this property collection manager will be automaticly it delegate and dataSource
    public weak var tableView: UITableView?
    
    /// The object that performs Collection View updates.
    public let updater = TableViewUpdater()
    
    public let sizeCalculator = TableSizeCalculator()
    
    public var beforeDequeCellInterceptors: [TablePrepareViewIntercepter] = []
    
    public var afterDequeCellInterceptors: [TablePrepareViewIntercepter] = [
        ActionHandlerTableIntercepter()]
    
    public var beforeDequeSupplementaryInterceptors: [TablePrepareViewIntercepter] = []
    
    public var afterDequeSupplementaryInterceptors: [TablePrepareViewIntercepter] = [
        ActionHandlerTableIntercepter()]
    
    /// Initializer.
    ///
    /// - Parameter storage: Collection view strorage.
    public override init(storage: Storage) {
        super.init(storage: storage)
        self.storage.delegate = self.updater
    }
    
    /// Prepare manager to start working with table view.
    ///
    /// - Parameter tableView: Table view.
    open func configure(for tableView: UITableView) {
        self.tableView = tableView
        self.updater.tableView = tableView
        self.sizeCalculator.tableView = tableView
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func createData(tableView: UITableView, view: UIView? = nil, indexPath: IndexPath, kind: SupplementaryKind? = nil) -> TableIntercepterData {
        return TableIntercepterData(
            storage: self.storage,
            tableView: tableView,
            view: view,
            indexPath: indexPath,
            kind: kind)
    }
    
    func performInterceptors(
        _ interceptors: [TablePrepareViewIntercepter],
        tableView: UITableView,
        view: UIView? = nil,
        indexPath: IndexPath,
        kind: SupplementaryKind? = nil) {
        
        let data = self.createData(tableView: tableView, view: view, indexPath: indexPath, kind: kind)
        interceptors.forEach() { $0.performActions(for: data) }
    }
    
    // MARK: - UICollectionViewDataSource


    public func numberOfSections(in tableView: UITableView) -> Int {
        return self.storage.sections.count
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.storage.sections[section].items.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = self.storage.sections[indexPath.section]
        let item = section.items[indexPath.row]

        section.setIndex(indexPath.section)
        item.setIndexPath(indexPath: indexPath)

        self.performInterceptors(self.beforeDequeCellInterceptors, tableView: tableView, indexPath: indexPath)

        let cell = tableView.dequeueReusableCell(withIdentifier: item.reuseIdentifier, for: indexPath)
        item.configure(instance: cell)

        self.performInterceptors(self.afterDequeCellInterceptors, tableView: tableView, view: cell, indexPath: indexPath)

        return cell
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.tableView(tableView, viewForSupplementaryInSection: section, kind: .sectionHeader)
    }
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return self.tableView(tableView, viewForSupplementaryInSection: section, kind: .sectionFooter)
    }
    
    func tableView(_ tableView: UITableView, viewForSupplementaryInSection section: Int, kind: SupplementaryKind) -> UIView? {
        guard let supplementary = self.storage.sections[section].supplementaryElements[kind] else { return nil }
        
        let indexPath = IndexPath(row: 0, section: section)
        let identifier = supplementary.reuseIdentifier
        
        self.performInterceptors(self.beforeDequeSupplementaryInterceptors, tableView: tableView, indexPath: indexPath, kind: kind)
        
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: identifier) else { return nil }
        supplementary.configure(instance: view)
        
        self.performInterceptors(self.afterDequeSupplementaryInterceptors, tableView: tableView, view: view, indexPath: indexPath, kind: kind)

        return view
    }
    
    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        self.tableViewDelegate?.tableView?(tableView, commit: editingStyle, forRowAt: indexPath)
    }
    
    public func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        self.tableViewDelegate?.tableView?(tableView, moveRowAt: sourceIndexPath, to: destinationIndexPath)
    }
    
    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return self.tableViewDelegate?.tableView?(tableView, canEditRowAt: indexPath) ?? false
    }
    
    public func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return self.tableViewDelegate?.tableView?(tableView, canMoveRowAt: indexPath) ?? false
    }
    
    public func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
        self.tableViewDelegate?.tableView?(tableView, willBeginEditingRowAt: indexPath)
    }
    
    public func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
        self.tableViewDelegate?.tableView?(tableView, didEndEditingRowAt: indexPath)
    }
    
    public func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt: IndexPath) -> String? {
        return self.tableViewDelegate?.tableView?(tableView, titleForDeleteConfirmationButtonForRowAt: titleForDeleteConfirmationButtonForRowAt)
    }
    
    // MARK: - TableViewDelegate
    
    public func manager(heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let item = self.storage.item(at: indexPath) else { return 0 }
        return self.sizeCalculator.calculateCellSize(for: item).height
    }
    
    public func manager(heightForHeaderInSection section: Int) -> CGFloat {
        guard let item = self.storage.sections[section].supplementaryElements[.sectionHeader] else { return 0 }
        return self.sizeCalculator.calculateSupplementarySize(for: item).height
    }
    
    public func manager(heightForFooterInSection section: Int) -> CGFloat {
        guard let item = self.storage.sections[section].supplementaryElements[.sectionFooter] else { return 0 }
        return self.sizeCalculator.calculateSupplementarySize(for: item).height
    }
    
    public func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        if let highlight = self.tableViewDelegate?.tableView?(tableView, shouldHighlightRowAt: indexPath) {
            return highlight
        } else {
            return self.performItemSouldHighlightHandler(indexPath: indexPath)
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableViewDelegate?.tableView?(tableView, didSelectRowAt: indexPath)
        self.performItemDidSelectHandler(indexPath: indexPath)
    }
    
    public func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.tableViewDelegate?.tableView?(tableView, didDeselectRowAt: indexPath)
        self.performItemDidDeselectHandler(indexPath: indexPath)
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.tableViewDelegate?.tableView?(tableView, willDisplay: cell, forRowAt: indexPath)
        self.performItemWillDisplayCellHandler(indexPath: indexPath)
    }

    public func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.tableViewDelegate?.tableView?(tableView, didEndDisplaying: cell, forRowAt: indexPath)
        self.performItemDidEndDisplayingCellHandler(indexPath: indexPath)
    }
    
    public func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        self.tableViewDelegate?.tableView?(tableView, willDisplayHeaderView: view, forSection: section)
        self.performItemWillDisplaySupplementaryHandler(indexPath: IndexPath(row: 0, section: section), kind: .sectionHeader)
    }
    
    public func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        self.tableViewDelegate?.tableView?(tableView, willDisplayFooterView: view, forSection: section)
        self.performItemWillDisplaySupplementaryHandler(indexPath: IndexPath(row: 0, section: section), kind: .sectionFooter)
    }
    
    public func tableView(_ tableView: UITableView, didEndDisplayingHeaderView view: UIView, forSection section: Int) {
        self.tableViewDelegate?.tableView?(tableView, didEndDisplayingHeaderView: view, forSection: section)
        self.performItemDidEndDisplayingSupplementaryHandler(indexPath: IndexPath(row: 0, section: section), kind: .sectionHeader)
    }
    
    public func tableView(_ tableView: UITableView, didEndDisplayingFooterView view: UIView, forSection section: Int) {
        self.tableViewDelegate?.tableView?(tableView, didEndDisplayingFooterView: view, forSection: section)
        self.performItemDidEndDisplayingSupplementaryHandler(indexPath: IndexPath(row: 0, section: section), kind: .sectionFooter)
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = self.tableViewDelegate?.tableView?(tableView, heightForRowAt: indexPath) { return height }
        return self.manager(heightForRowAt: indexPath)
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let height = self.tableViewDelegate?.tableView?(tableView, heightForHeaderInSection: section) { return height }
        return self.manager(heightForHeaderInSection: section)
    }
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if let height = self.tableViewDelegate?.tableView?(tableView, heightForFooterInSection: section) { return height }
        return self.manager(heightForFooterInSection: section)
    }
    
    public func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        return self.tableViewDelegate?.tableView?(tableView, editActionsForRowAt: indexPath)
    }
    
    public func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        guard let type = self.tableViewDelegate?.tableView?(tableView, editingStyleForRowAt: indexPath) else {
            return .none
        }
        
        return type
    }
    
    public func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return self.tableViewDelegate?.tableView?(tableView, shouldHighlightRowAt: indexPath) ?? false
    }
    
    
}


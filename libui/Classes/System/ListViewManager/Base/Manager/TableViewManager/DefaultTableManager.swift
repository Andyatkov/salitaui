//
//  DefaultTableManager.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit


/// Default table view manager.
public class DefaultTableManager: TableViewManager<InMemoryListStorage> {
    
    public override init(storage: InMemoryListStorage) {
        super.init(storage: storage)
    }
    
    public init() {
        super.init(storage: InMemoryListStorage())
    }
    
}


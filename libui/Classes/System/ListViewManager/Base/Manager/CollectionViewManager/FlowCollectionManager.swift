//
//  FlowCollectionManager.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public class FlowCollectionManager: CollectionViewManager<InMemoryListStorage>, UICollectionViewDelegateFlowLayout {
    
    public var sizeCalculator = CollectionFlowSizeCalculator()
    
    var flowDelegate: UICollectionViewDelegateFlowLayout? {
        return collectionViewDelegate as? UICollectionViewDelegateFlowLayout
    }
    
    public override init(storage: InMemoryListStorage) {
        super.init(storage: storage)
    }
    
    public init() {
        super.init(storage: InMemoryListStorage())
    }
    
    open override func configure(for collectionView: UICollectionView) {
        super.configure(for: collectionView)
        sizeCalculator.collectionView = collectionView
    }
    
    // MARK: - FlowLayout Delegate
    
    public func manager(layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let item = storage.item(at: indexPath) else { return .zero }
        return sizeCalculator.calculateCellSize(for: item)
    }
    
    public func manager(layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let kind: SupplementaryKind = .sectionHeader
        guard let supplementary = storage.supplementary(at: section, kind: kind) else { return .zero }
        return sizeCalculator.calculateSupplementarySize(for: supplementary, kind: kind)
    }
    
    public func manager(layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        let kind: SupplementaryKind = .sectionFooter
        guard let supplementary = storage.supplementary(at: section, kind: kind) else { return .zero }
        
        return sizeCalculator.calculateSupplementarySize(for: supplementary, kind: kind)
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let size = flowDelegate?.collectionView?(collectionView, layout: collectionViewLayout, sizeForItemAt: indexPath) { return size }
        return manager(layout: collectionViewLayout, sizeForItemAt: indexPath)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if let size = flowDelegate?.collectionView?(collectionView, layout: collectionViewLayout, referenceSizeForHeaderInSection: section) { return size }
        return manager(layout: collectionViewLayout, referenceSizeForHeaderInSection: section)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if let size = flowDelegate?.collectionView?(collectionView, layout: collectionViewLayout, referenceSizeForFooterInSection: section) { return size }
        return manager(layout: collectionViewLayout, referenceSizeForFooterInSection: section)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let sectionInset = (self.collectionView?.collectionViewLayout as? UICollectionViewFlowLayout)?.sectionInset ?? .zero
        return flowDelegate?.collectionView?(collectionView, layout: collectionViewLayout, insetForSectionAt: section) ?? sectionInset
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        let minimumLineSpacing = (self.collectionView?.collectionViewLayout as? UICollectionViewFlowLayout)?.minimumLineSpacing ?? 0.0
        return flowDelegate?.collectionView?(collectionView, layout: collectionViewLayout, minimumLineSpacingForSectionAt: section) ?? minimumLineSpacing
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        let spacing = (self.collectionView?.collectionViewLayout as? UICollectionViewFlowLayout)?.minimumInteritemSpacing ?? 0.0
        return flowDelegate?.collectionView?(collectionView, layout: collectionViewLayout, minimumInteritemSpacingForSectionAt: section) ?? spacing
    }
    
}


//
//  CollectionViewManager.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit


/// Base collection manager class to simplify work with UICollectionView.
public class CollectionViewManager<Storage>: ListViewManager<Storage>,
             UICollectionViewDelegate,
             UICollectionViewDataSource where
             Storage: ListStorageProtocol {
    
    /// Delegate that handle all UICollectionViewDelegate methods forwarded by CollectionViewManager.
    public weak var collectionViewDelegate: UICollectionViewDelegate?
    
    override var scrollViewDelegate: UIScrollViewDelegate? {
        return self.collectionViewDelegate
    }
    
    /// The collection view.
    /// After setting this property collection manager will be automaticly it delegate and dataSource
    public weak var collectionView: UICollectionView?
    
    /// The object that performs Collection View updates.
    public var updater = CollectionViewUpdater()
    
    public var beforeDequeCellInterceptors: [CollectionPrepareViewIntercepter] = []
    
    public var afterDequeCellInterceptors: [CollectionPrepareViewIntercepter] = [
        ActionHandlerCollectionIntercepter()]
    
    public var beforeDequeSupplementaryInterceptors: [CollectionPrepareViewIntercepter] = []
    
    public var afterDequeSupplementaryInterceptors: [CollectionPrepareViewIntercepter] = [
        ActionHandlerCollectionIntercepter()]
    
    /// Initializer.
    ///
    /// - Parameter storage: Collection view strorage.
    public override init(storage: Storage) {
        super.init(storage: storage)
        self.storage.delegate = self.updater
    }
    
    /// Prepare manager to start working with collection view.
    ///
    /// - Parameter for: Collection view.
    open func configure(for collectionView: UICollectionView) {
        self.collectionView = collectionView
        self.updater.collectionView = collectionView
        
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    // MARK: - Internal
    
    func createData(collectionView: UICollectionView, view: UICollectionReusableView? = nil, indexPath: IndexPath, kind: SupplementaryKind? = nil) -> CollectionIntercepterData {
        return CollectionIntercepterData(
            storage: self.storage,
            collectionView: collectionView,
            view: view,
            indexPath: indexPath,
            kind: kind)
    }
    
    func performInterceptors(
        _ interceptors: [CollectionPrepareViewIntercepter],
        collectionView: UICollectionView,
        view: UICollectionReusableView? = nil,
        indexPath: IndexPath,
        kind: SupplementaryKind? = nil) {
        
        let data = self.createData(collectionView: collectionView, view: view, indexPath: indexPath, kind: kind)
        interceptors.forEach() { $0.performActions(for: data) }
    }
    
    // MARK: - UICollectionViewDataSource
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.storage.sections.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.storage.sections[section].items.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let section = self.storage.sections[indexPath.section]
        let item = section.items[indexPath.row]
        
        section.setIndex(indexPath.section)
        item.setIndexPath(indexPath: indexPath)
        
        self.performInterceptors(self.beforeDequeCellInterceptors, collectionView: collectionView, indexPath: indexPath)
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: item.reuseIdentifier, for: indexPath)
        item.configure(instance: cell)
        
        self.performInterceptors(self.afterDequeCellInterceptors, collectionView: collectionView, view: cell, indexPath: indexPath)
        
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let elementKind = SupplementaryKind(kind: kind)
        
        guard let supplementary = self.storage.sections[indexPath.section].supplementaryElements[elementKind] else {
            fatalError("No availible supplementary element for kind: \(kind) at indexPath: \(indexPath)")
        }
        
        self.performInterceptors(self.beforeDequeSupplementaryInterceptors, collectionView: collectionView, indexPath: indexPath, kind: elementKind)
        
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: supplementary.reuseIdentifier, for: indexPath)
        supplementary.configure(instance: view)
        
        self.performInterceptors(self.afterDequeSupplementaryInterceptors, collectionView: collectionView, view: view, indexPath: indexPath, kind: elementKind)
        
        return view
    }
    
    // MARK: - CollectionViewDelegate
    
    public func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        if let highlight = self.collectionViewDelegate?.collectionView?(collectionView, shouldHighlightItemAt: indexPath) {
            return highlight
        } else {
            return self.storage.sections[indexPath.section].items[indexPath.row].shouldHighlight
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.collectionViewDelegate?.collectionView?(collectionView, didSelectItemAt: indexPath)
        self.performItemDidSelectHandler(indexPath: indexPath)
    }
    
    public func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        self.collectionViewDelegate?.collectionView?(collectionView, didDeselectItemAt: indexPath)
        self.performItemDidDeselectHandler(indexPath: indexPath)
    }
    
    public func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.collectionViewDelegate?.collectionView?(collectionView, willDisplay: cell, forItemAt: indexPath)
        self.performItemWillDisplayCellHandler(indexPath: indexPath)
    }
    
    public func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.collectionViewDelegate?.collectionView?(collectionView, didEndDisplaying: cell, forItemAt: indexPath)
        self.performItemDidEndDisplayingCellHandler(indexPath: indexPath)
    }
    
    public func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        self.collectionViewDelegate?.collectionView?(collectionView, willDisplaySupplementaryView: view, forElementKind: elementKind, at: indexPath)
        let kind = SupplementaryKind(kind: elementKind)
        self.performItemWillDisplaySupplementaryHandler(indexPath: indexPath, kind: kind)
    }
    
    public func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        self.collectionViewDelegate?.collectionView?(collectionView, didEndDisplayingSupplementaryView: view, forElementOfKind: elementKind, at: indexPath)
        let kind = SupplementaryKind(kind: elementKind)
        self.performItemDidEndDisplayingSupplementaryHandler(indexPath: indexPath, kind: kind)
    }
    
}


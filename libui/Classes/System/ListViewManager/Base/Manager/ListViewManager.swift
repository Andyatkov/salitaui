//
//  ListViewManager.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

/// Base collection manager class to simplify work with list view.
public class ListViewManager<Storage: ListStorageProtocol>: NSObject, UIScrollViewDelegate {
    
    /// The storage that syncronize updates with collection view.
    public let storage: Storage
        
    /// All cell ids registred by collection manager.
    public var registeredCellIDs: Set<String> = []
    
    /// All supplementary view ids registred by collection manager.
    public var registeredSupplementaryIDs: Set<String> = []
    
    var scrollViewDelegate: UIScrollViewDelegate? { return nil }
    
    /// Initializer.
    ///
    /// - Parameter storage: Collection view strorage.
    public init(storage: Storage) {
        self.storage = storage
    }
    
    // MARK: - Public methods
    
    public func performItemSouldHighlightHandler(indexPath: IndexPath) -> Bool {
        guard storage.isValid(indexPath: indexPath) else { return false }
        
        return storage.sections[indexPath.section].items[indexPath.row].shouldHighlight
    }
    
    /// Manual perform did select action closure for item at index path.
    /// If collection manager is not a delegate of collection view you need to manual perform items action colosures.
    /// Use this method to do it.
    ///
    /// - Parameter indexPath: Item index path.
    public func performItemDidSelectHandler(indexPath: IndexPath) {
        guard storage.isValid(indexPath: indexPath) else { return }
        
        storage.sections[indexPath.section].items[indexPath.row].didSelect?(indexPath)
    }
    
    /// Manual perform did deselect action closure for item at index path.
    /// If collection manager is not a delegate of collection view you need to manual perform items action colosures.
    /// Use this method to do it.
    ///
    /// - Parameter indexPath: Item index path.
    public func performItemDidDeselectHandler(indexPath: IndexPath) {
        guard storage.isValid(indexPath: indexPath) else { return }
        
        storage.sections[indexPath.section].items[indexPath.row].didDeselect?(indexPath)
    }
    
    /// Manual perform will display action closure for item at index path.
    /// If collection manager is not a delegate of collection view you need to manual perform items action colosures.
    /// Use this method to do it.
    ///
    /// - Parameter indexPath: Item index path.
    public func performItemWillDisplayCellHandler(indexPath: IndexPath) {
        guard storage.isValid(indexPath: indexPath) else { return }
        
        storage.sections[indexPath.section].items[indexPath.row].willDisplay?(indexPath)
    }
    
    /// Manual perform did end displaying action closure for item at index path.
    /// If collection manager is not a delegate of collection view you need to manual perform items action colosures.
    /// Use this method to do it.
    ///
    /// - Parameter indexPath: Item index path.
    public func performItemDidEndDisplayingCellHandler(indexPath: IndexPath) {
        guard storage.isValid(indexPath: indexPath) else { return }
        
        storage.sections[indexPath.section].items[indexPath.row].didEndDisplaying?(indexPath)
    }
    
    public func performItemWillDisplaySupplementaryHandler(indexPath: IndexPath, kind: SupplementaryKind) {
        guard storage.isValid(sectionIndex: indexPath.section) else { return }
        guard let item = storage.sections[indexPath.section].supplementaryElements[kind] else { return }
        
        item.willDisplay?(indexPath)
    }
    
    public func performItemDidEndDisplayingSupplementaryHandler(indexPath: IndexPath, kind: SupplementaryKind) {
        guard storage.isValid(sectionIndex: indexPath.section) else { return }
        guard let item = storage.sections[indexPath.section].supplementaryElements[kind] else { return }
        
        item.didEndDisplaying?(indexPath)
    }
        
    // MARK: - ScrollViewDelegate
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollViewDelegate?.scrollViewDidScroll?(scrollView)
    }
    
    public func scrollViewDidZoom(_ scrollView: UIScrollView) {
        scrollViewDelegate?.scrollViewDidZoom?(scrollView)
    }
    
    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollViewDelegate?.scrollViewWillBeginDragging?(scrollView)
    }
    
    public func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        scrollViewDelegate?.scrollViewWillEndDragging?(scrollView, withVelocity: velocity, targetContentOffset: targetContentOffset)
    }
    
    public func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollViewDelegate?.scrollViewDidEndDragging?(scrollView, willDecelerate: decelerate)
    }
    
    public func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        scrollViewDelegate?.scrollViewWillBeginDecelerating?(scrollView)
    }
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        scrollViewDelegate?.scrollViewDidEndDecelerating?(scrollView)
    }
    
    public func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        scrollViewDelegate?.scrollViewDidEndScrollingAnimation?(scrollView)
    }
    
    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return scrollViewDelegate?.viewForZooming?(in: scrollView)
    }
    
    public func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollViewDelegate?.scrollViewWillBeginZooming?(scrollView, with: view)
    }
    
    public func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        scrollViewDelegate?.scrollViewDidEndZooming?(scrollView, with: view, atScale: scale)
    }
    
    public func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
        return scrollViewDelegate?.scrollViewShouldScrollToTop?(scrollView) ?? true
    }
    
    public func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
        scrollViewDelegate?.scrollViewDidScrollToTop?(scrollView)
    }
    
}


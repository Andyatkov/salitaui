//
//  InMemoryListStorage.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import Foundation

open class InMemoryListStorage: ListStorage, ListStorageProtocol {
    
    public typealias Item = ListItem
    public typealias Section = ListSection
    public typealias BoolClosure = ((Bool) -> Void)
    
    open var sections = [Section]()
    
    open var defersDatasourceUpdates: Bool = true
        
    // MARK: - Sections
    
    open func sectionIndex(for section: Section) -> Int? {
        return sections.firstIndex() { $0 === section }
    }
    
    open func setSections(_ sections: [Section]) {
        self.sections = sections
        self.delegate?.storageNeedsReloading()
    }
    
    open func appendSection(_ section: Section, completion: BoolClosure? = nil) {
        self.startUpdate()
        self.performDatasourceUpdate { [weak self] update in
            guard let `self` = self else { return }
            
            let sectionIndex = self.sections.count
            
            self.sections.append(section)
            update.sectionChanges.append((.insert, [sectionIndex]))
            update.completion = completion
        }
        self.finishUpdate()
    }
    
    open func insertSection(_ section: Section, atIndex sectionIndex: Int, completion: BoolClosure? = nil) {
        guard sectionIndex <= self.sections.count else { return }
        
        self.startUpdate()
        self.performDatasourceUpdate { [weak self] update in
            guard let `self` = self else { return }
            
            self.sections.insert(section, at: sectionIndex)
            update.sectionChanges.append((.insert, [sectionIndex]))
            
            for item in 0..<section.numberOfItems {
                update.objectChanges.append((.insert, [IndexPath(item: item, section: sectionIndex)]))
            }
            update.completion = completion
        }
        self.finishUpdate()
    }
    
    open func replaceSection(atIndex sectionIndex: Int, with newSection: Section, completion: BoolClosure? = nil) {
        guard sectionIndex <= self.sections.count else { return }
        
        self.startUpdate()
        self.performDatasourceUpdate { [weak self] update in
            guard let `self` = self else { return }
            
            update.sectionChanges.append((.update, [sectionIndex]))
            update.completion = completion
            
            self.sections[sectionIndex] = newSection
        }
        self.finishUpdate()
    }
    
    open func reloadSection(_ indexes: IndexSet, completion: BoolClosure? = nil) {
        self.startUpdate()
        
        self.performDatasourceUpdate { update in
            update.sectionChanges.append((.update, Array(indexes)))
            update.completion = completion
        }
        
        self.finishUpdate()
    }
    
    open func removeSections(_ indexes: IndexSet, completion: BoolClosure? = nil) {
        self.startUpdate()
        self.performDatasourceUpdate { [weak self] update in
            guard let `self` = self else { return }
            
            var markedForDeletion = [Int]()
            
            for section in indexes where section < self.sections.count {
                markedForDeletion.append(section)
            }
            
            for section in markedForDeletion.sorted().reversed() {
                self.sections.remove(at: section)
            }
            
            markedForDeletion.forEach {
                update.sectionChanges.append((.delete, [$0]))
            }
            update.completion = completion
        }
        self.finishUpdate()
    }
    
    open func moveSection(_ sourceSectionIndex: Int, toSection destinationSectionIndex: Int, completion: BoolClosure? = nil) {
        self.startUpdate()
        self.performDatasourceUpdate { [weak self] update in
            guard let `self` = self else { return }
            
            let validSectionFrom = self.getValidSection(sourceSectionIndex, collectChangesIn: update)
            
            self.getValidSection(destinationSectionIndex, collectChangesIn: update)
            self.sections.remove(at: sourceSectionIndex)
            self.sections.insert(validSectionFrom, at: destinationSectionIndex)
            
            update.sectionChanges.append((.move, [sourceSectionIndex, destinationSectionIndex]))
            update.completion = completion
        }
        self.finishUpdate()
    }
    
    // MARK: - Items
    
    open func item(at indexPath: IndexPath) -> Item? {
        guard indexPath.section < sections.count else { return nil }
        guard indexPath.item < sections[indexPath.section].items.count else { return nil }
        
        return sections[indexPath.section].items[indexPath.item]
    }
    
    open func setItems(_ items: [Item], forSection index: Int) {
        self.finishUpdate()
        let section = self.getValidSection(index, collectChangesIn: nil)

        section.items.removeAll()
        section.items.removeAll(keepingCapacity: false)
        section.items = items.map { $0 }

        self.delegate?.storageNeedsReloading()
    }
    
    open func appendItems(_ items: [Item], toSection index: Int, completion: BoolClosure? = nil) {
        self.startUpdate()
        self.performDatasourceUpdate { [weak self] update in
            let section = self?.getValidSection(index, collectChangesIn: update)
            
            for item in items {
                let numberOfItems = section?.numberOfItems ?? 0
                section?.items.append(item)
                update.objectChanges.append((.insert, [IndexPath(item: numberOfItems, section: index)]))
            }
            
            update.completion = completion
        }
        self.finishUpdate()
    }
    
    open func insertItems(_ items: [Item], to indexPath: IndexPath, completion: BoolClosure? = nil) {
        self.startUpdate()
        if self.defersDatasourceUpdates {
            self.performDatasourceUpdate { [weak self] update in
                guard let `self` = self else { return }
                
                let indexPaths = (0..<items.count).map() { IndexPath(row: indexPath.row + $0, section: indexPath.section) }
                
                indexPaths.enumerated().forEach { arg in
                    let (itemIndex, indexPath) = arg
                    let section = self.getValidSection(indexPath.section, collectChangesIn: update)
                    
                    guard section.items.count >= indexPath.item else {
                        return
                    }
                    
                    section.items.insert(items[itemIndex], at: indexPath.item)
                    update.objectChanges.append((.insert, [indexPath]))
                }
                update.completion = completion
            }
        } else {
            self.performUpdates {
                let indexPaths = (0..<items.count).map() { IndexPath(row: $0, section: indexPath.section) }
                
                indexPaths.enumerated().forEach { arg in
                    let (itemIndex, indexPath) = arg
                    let section = self.getValidSection(indexPath.section, collectChangesIn: self.currentUpdate)
                    
                    guard section.items.count >= indexPath.item else {
                        return
                    }
                    
                    section.items.insert(items[itemIndex], at: indexPath.item)
                    self.currentUpdate?.objectChanges.append((.insert, [indexPath]))
                }
                self.currentUpdate?.completion = completion
            }
        }
        self.finishUpdate()
    }
    
    open func removeItems(at indexPaths: [IndexPath], completion: BoolClosure? = nil) {
        self.startUpdate()
        self.performDatasourceUpdate { [weak self] update in
            let reverseSortedIndexPaths = InMemoryListStorage.sortedArrayOfIndexPaths(indexPaths, ascending: false)
            
            for indexPath in reverseSortedIndexPaths {
                guard self?.item(at: indexPath) != nil else { continue }
                self?.getValidSection(indexPath.section, collectChangesIn: update).items.remove(at: indexPath.item)
                update.objectChanges.append((.delete, [indexPath]))
            }
            
            update.completion = completion
        }
        self.finishUpdate()
    }
    
    open func removeAllItems(fromSection sectionIndex: Int, completion: BoolClosure? = nil) {
        self.startUpdate()
        defer { self.finishUpdate() }
        
        self.performDatasourceUpdate { [weak self] update in
            guard let section = self?.sections[sectionIndex] else { return }
            
            for index in section.items.indices {
                update.objectChanges.append((.delete, [IndexPath(item: index, section: sectionIndex)]))
            }
            section.items.removeAll()
            update.completion = completion
        }
    }
    
    open func removeAllItems() {
        for section in self.sections {
            section.items.removeAll(keepingCapacity: false)
        }
        self.delegate?.storageNeedsReloading()
    }
    
    open func replaceItem(at indexPath: IndexPath, with item: Item, completion: BoolClosure? = nil) {
        self.startUpdate()
        self.performDatasourceUpdate { [weak self] update in
            guard let `self` = self else { return }
            guard self.isValid(indexPath: indexPath) else { return }
            
            self.sections[indexPath.section].items[indexPath.row] = item
            
            update.objectChanges.append((.update, [indexPath]))
            update.completion = completion
        }
        self.finishUpdate()
    }
    
    open func reloadItems(at indexPaths: [IndexPath], completion: BoolClosure? = nil) {
        self.startUpdate()
        self.performDatasourceUpdate { [weak self] update in
            guard let `self` = self else { return }
            
            for indexPath in indexPaths {
                guard self.isValid(indexPath: indexPath) else { continue }
                update.objectChanges.append((.update, [indexPath]))
            }
           
            update.completion = completion
        }
        self.finishUpdate()
    }
    
    open func moveItem(at source: IndexPath, to destination: IndexPath, completion: BoolClosure? = nil) {
        self.startUpdate()
        defer { self.finishUpdate() }
        
        self.performDatasourceUpdate { [weak self] update in
            guard let sourceItem = self?.item(at: source) else { return }
            
            let sourceSection = self?.getValidSection(source.section, collectChangesIn: update)
            let destinationSection = self?.getValidSection(destination.section, collectChangesIn: update)
            let destinationSectionItemsCount = destinationSection?.items.count ?? 0
            
            let numberOfItemsInSectionAfterRemovingSource =
                source.section == destination.section ? destinationSectionItemsCount - 1 : destinationSectionItemsCount
            
            guard numberOfItemsInSectionAfterRemovingSource > destination.row else { return }
            
            sourceSection?.items.remove(at: source.row)
            destinationSection?.items.insert(sourceItem, at: destination.item)
            
            update.objectChanges.append((.move, [source, destination]))
            update.completion = completion
        }
    }
    
    open func section(for identifier: String) -> (index: Int, section: Section)? {
        for section in self.sections.enumerated() {
            if section.element.uniqueID == identifier { return (index: section.offset, section: section.element) }
        }
        
        return nil
    }
    
    open func item(for identifier: String) -> (indexPath: IndexPath, item: Item)? {
        for section in self.sections.enumerated() {
            for item in self.sections[section.offset].items.enumerated() {
                if item.element.uniqueID == identifier {
                    return (indexPath: IndexPath(row: item.offset, section: section.offset), item: item.element)
                }
            }
        }
        
        return nil
    }
    
    open func reloadData() {
        self.delegate?.storageNeedsReloading()
    }
    
    func performDatasourceUpdate(_ block: @escaping (StorageUpdate) throws -> Void) {
        if self.defersDatasourceUpdates {
            self.currentUpdate?.enqueueDatasourceUpdate(block)
        } else {
            if let update = self.currentUpdate {
                try? block(update)
            }
        }
    }
    
    /// Finds-or-creates section at `sectionIndex`
    ///
    /// - Note: This method finds or create a SectionModel. It means that if you create section 2, section 0 and 1 will be automatically created.
    /// - Returns: SectionModel
    @discardableResult
    func getValidSection(_ sectionIndex: Int, collectChangesIn update: StorageUpdate?) -> Section {
        guard sectionIndex > self.sections.count  else {
            return sections[sectionIndex]
        }
        
        for i in sections.count...sectionIndex {
            self.sections.append(Section())
            update?.sectionChanges.append((.insert, [i]))
        }
        
        return self.sections.last !! fatalError("Couldn't get last section")
    }
    
    static func sortedArrayOfIndexPaths(_ indexPaths: [IndexPath], ascending: Bool) -> [IndexPath] {
        let unsorted = NSMutableArray(array: indexPaths)
        let descriptor = NSSortDescriptor(key: "self", ascending: ascending)
        return unsorted.sortedArray(using: [descriptor]) as? [IndexPath] ?? []
    }
    
}


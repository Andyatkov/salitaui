//
//  TableViewUpdater.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit


/// `TableViewUpdater` is responsible for updating `UITableView`, when it receives storage updates.
open class TableViewUpdater: StorageUpdating {
    
    /// table view, that will be updated
    weak var tableView: UITableView?
    
    /// closure to be executed before content is updated
    open var willUpdateContent: ((StorageUpdate?) -> Void)?
    
    /// closure to be executed after content is updated
    open var didUpdateContent: ((StorageUpdate?) -> Void)?
    
    /// Insert section animation. Default - .none.
    open var insertSectionAnimation = UITableView.RowAnimation.none
    
    /// Delete section animation. Default - .automatic
    open var deleteSectionAnimation = UITableView.RowAnimation.automatic
    
    /// Reload section animation. Default - .automatic.
    open var reloadSectionAnimation = UITableView.RowAnimation.automatic
    
    /// Insert row animation. Default - .automatic.
    open var insertRowAnimation = UITableView.RowAnimation.automatic
    
    /// Delete row animation. Default - .automatic.
    open var deleteRowAnimation = UITableView.RowAnimation.automatic
    
    /// Reload row animation. Default - .automatic.
    open var reloadRowAnimation = UITableView.RowAnimation.automatic
    
    /// Closure to be executed, when reloading a row.
    ///
    /// If this property is not nil, then reloadRowAnimation property is ignored.
    /// - SeeAlso: `DTTableViewManager.updateCellClosure()` method and `DTTableViewManager.coreDataUpdater()` method.
    open var reloadRowClosure: ((IndexPath, Any) -> Void)?
    
    /// When this property is true, move events will be animated as delete event and insert event.
    open var animateMoveAsDeleteAndInsert: Bool
    
    /// Creates updater with tableView.
    public init(tableView: UITableView? = nil, reloadRow: ((IndexPath, Any) -> Void)? = nil, animateMoveAsDeleteAndInsert: Bool = false) {
        self.tableView = tableView
        self.reloadRowClosure = reloadRow
        self.animateMoveAsDeleteAndInsert = animateMoveAsDeleteAndInsert
    }
    
    /// Updates `UITableView` with received `update`. This method applies object and section changes in `performBatchUpdates` block.
    open func storageDidPerformUpdate(_ update: StorageUpdate) {
        guard let tableView = self.tableView else {
            preconditionFailure("\(String(describing: self)) cound't perform action, because tableView is nil")
        }
        
        self.willUpdateContent?(update)
        
        tableView.performBatchUpdates({ [weak self] in
            if update.containsDeferredDatasourceUpdates {
                update.applyDeferredDatasourceUpdates()
            }
            self?.applyObjectChanges(from: update)
            self?.applySectionChanges(from: update)
        }, completion: { [weak self] result in
            self?.didUpdateContent?(update)
            update.completion?(result)
        })
        
        self.didUpdateContent?(update)
    }
    
    private func applyObjectChanges(from update: StorageUpdate) {
        guard let tableView = self.tableView else {
            preconditionFailure("\(String(describing: self)) cound't perform action, because tableView is nil")
        }
        
        for (change, indexPaths) in update.objectChanges {
            switch change {
            case .insert:
                if let indexPath = indexPaths.first {
                    tableView.insertRows(at: [indexPath], with: self.insertRowAnimation)
                }
            case .delete:
                if let indexPath = indexPaths.first {
                    tableView.deleteRows(at: [indexPath], with: self.deleteRowAnimation)
                }
            case .update:
                if let indexPath = indexPaths.first {
                    if let closure = reloadRowClosure, let model = update.updatedObjects[indexPath] {
                        closure(indexPath, model)
                    } else {
                        tableView.reloadRows(at: [indexPath], with: self.reloadRowAnimation)
                    }
                }
            case .move:
                if let source = indexPaths.first, let destination = indexPaths.last {
                    if animateMoveAsDeleteAndInsert {
                        tableView.moveRow(at: source, to: destination)
                    } else {
                        tableView.deleteRows(at: [source], with: self.deleteRowAnimation)
                        tableView.insertRows(at: [destination], with: self.insertRowAnimation)
                    }
                }
            }
        }
    }
    
    private func applySectionChanges(from update: StorageUpdate) {
        guard let tableView = self.tableView else { return }
        
        for (change, indices) in update.sectionChanges {
            switch change {
            case .delete:
                if let index = indices.first {
                    tableView.deleteSections([index], with: deleteSectionAnimation)
                }
            case .insert:
                if let index = indices.first {
                    tableView.insertSections([index], with: insertSectionAnimation)
                }
            case .update:
                if let index = indices.first {
                    tableView.reloadSections([index], with: reloadSectionAnimation)
                }
            case .move:
                if let source = indices.first, let destination = indices.last {
                    tableView.moveSection(source, toSection: destination)
                }
            }
        }
    }
    
    /// Call this method, if you want UITableView to be reloaded, and beforeContentUpdate: and afterContentUpdate: closures to be called.
    open func storageNeedsReloading() {
        self.willUpdateContent?(nil)
        self.tableView?.reloadData()
        self.didUpdateContent?(nil)
    }
    
}


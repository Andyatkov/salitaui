//
//  StorageUpdating.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import Foundation

/// `StorageUpdating` protocol is used to transfer data storage updates.
public protocol StorageUpdating: class {
    /// Transfers data storage updates.
    ///
    /// Object, that implements this method, may react to received update by updating UI for current storage.
    func storageDidPerformUpdate(_ update: StorageUpdate)
    
    /// Method is called when UI needs to be fully updated for data storage changes.
    func storageNeedsReloading()
}


//
//  StorageProtocols.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public protocol ListStorageProtocol: class {
    var sections: [ListSection] { get }
    var delegate: StorageUpdating? { get set }
}

extension ListStorageProtocol {
    
    public func isValid(indexPath: IndexPath) -> Bool {
        return indexPath.section < self.sections.count && indexPath.row < self.sections[indexPath.section].items.count
    }
    
    public func isValid(sectionIndex: Int) -> Bool {
        return self.sections.count > sectionIndex
    }
    
    public func section(at indexPath: IndexPath) -> ListSection? {
        guard self.isValid(indexPath: indexPath) else { return nil }
        return self.sections[indexPath.section]
    }
    
    public func item(at indexPath: IndexPath) -> ListItem? {
        guard self.isValid(indexPath: indexPath) else { return nil }
        return self.sections[indexPath.section].items[indexPath.row]
    }
    
    public func supplementary(at section: Int, kind: SupplementaryKind) -> ListItem? {
        guard section < self.sections.count else { return nil }
        return self.sections[section].supplementaryElements[kind]
    }
    
}


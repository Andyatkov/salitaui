//
//  ListStorage.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import Foundation

open class ListStorage {
    
    public typealias Item = ListItem
    public typealias Section = ListSection
    
    /// Current update
    open var currentUpdate: StorageUpdate?
    
    /// Batch updates are in progress. If true, update will not be finished.
    open var batchUpdatesInProgress = false
    
    /// Delegate for storage updates
    open weak var delegate: StorageUpdating?
    
    /// Performs update `block` in storage. After update is finished, delegate will be notified.
    /// Parameter block: Block to execute
    /// - Note: This method allows to execute several updates in a single batch. It is similar to UICollectionView method `performBatchUpdates:`.
    /// - Warning: Performing mutually exclusive updates inside block can cause application crash.
    open func performUpdates( _ block: () -> Void) {
        self.batchUpdatesInProgress = true
        self.startUpdate()
        block()
        self.batchUpdatesInProgress = false
        self.finishUpdate()
    }
    
    /// Starts update in storage.
    ///
    /// This creates StorageUpdate instance and stores it into `currentUpdate` property.
    open func startUpdate() {
        if self.currentUpdate == nil {
            self.currentUpdate = StorageUpdate()
        }
    }
    
    /// Finishes update.
    ///
    /// Method verifies, that update is not empty, and sends updates to the delegate. After this method finishes, `currentUpdate` property is nilled out.
    open func finishUpdate() {
        guard self.batchUpdatesInProgress == false else { return }
        defer { self.currentUpdate = nil }
        
        if let update = currentUpdate {
            if update.isEmpty { return }
            self.delegate?.storageDidPerformUpdate(update)
        }
    }
    
    public init() { }
    
}


//
//  SupplementaryKind.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public extension UITableView {
    class var elementKindSectionHeader: String { return "SectionHeader" }
    class var elementKindSectionFooter: String { return "SectionFooter" }
}

public enum SupplementaryKind: Hashable {
    case sectionHeader
    case sectionFooter
    case custom(String)
    
    public enum ListType {
        case table
        case collection
    }
    
    public init(kind: String) {
        switch kind {
        case UITableView.elementKindSectionHeader, UICollectionView.elementKindSectionHeader:
            self = .sectionHeader
        case UITableView.elementKindSectionFooter, UICollectionView.elementKindSectionFooter:
            self = .sectionFooter
        default:
            self = .custom(kind)
        }
    }
    
    public func kind(type: ListType) -> String {
        switch type {
        case .table:
            switch self {
            case .sectionHeader:
                return UITableView.elementKindSectionHeader
            case .sectionFooter:
                return UITableView.elementKindSectionFooter
            case .custom(let custom):
                return custom
            }
        case .collection:
            switch self {
            case .sectionHeader:
                return UICollectionView.elementKindSectionHeader
            case .sectionFooter:
                return UICollectionView.elementKindSectionFooter
            case .custom(let custom):
                return custom
            }
        }
    }
    
}


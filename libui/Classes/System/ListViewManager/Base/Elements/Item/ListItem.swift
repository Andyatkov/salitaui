//
//  ListItem.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

/// Base element in collection / table view.
public class ListItem: UniqueObjectProtocol {

    /// Unique item identifier.
    public let uniqueID: String
    
    /// Collection item reuse identifier.
    public let reuseIdentifier: String
    
    /// Collection item index path.
    public private(set) var indexPathInDataSource: IndexPath?
    
    /// Should highlight item.
    public var shouldHighlight: Bool = true
    
    /// This closure will be called after the item at the specified index path was selected.
    public var didSelect: IndexPathClosure?
    
    /// This closure will be called after the item at the specified index path was deselected.
    public var didDeselect: IndexPathClosure?
    
    /// This closure will be called when item will be displayed.
    public var willDisplay: IndexPathClosure?
    
    /// This closure will be called after the item at the specified index path did end displaying.
    public var didEndDisplaying: IndexPathClosure?
    
    /// This closure will be injected in reusable view that support CollectionActionClosure protocol
    /// And the reusable view can call this closure in some of it methods.
    public var customAction: CollectionActionClosure?
    
    /// Size for item. Use this property if you want to manual calculate item size.
    /// For example: You load data from server, than manual calculate size by some alghorithm(may be in backgound) and then store it in this property.
    /// After all you could use it in CollectionViewDelegate method f.e in sizeForItemAtIndexPath.
    public var size: CGSize?
    
    /// Helper property to indicate view model in collection view delegate methods.
    public var anyViewModel: AnyViewModel? { return nil }
    
    /// Helper property to indicate reusable view in collection view delegate methods.
    public var view: UIView? { return nil }
    
    /// The type of associeted view.
    public var viewType: AnyClass { fatalError("Abstract property") }
    
    /// A method to manual calculate reusable view size.
    ///
    /// - Returns: Calculated size.
    public func manualCalculateSize() -> CGSize {
        return .zero
    }
    
    /// A method to configure reusable view at the specified index path.
    ///
    /// - Parameters:
    ///   - instance: Reusable view.
    ///   - indexPath: IndexPath.

    public func configure(instance: UIView) {
        fatalError("Abstract method")
    }
    
    public init(uniqueID: String, reuseIdentifier: String) {
        self.uniqueID = uniqueID
        self.reuseIdentifier = reuseIdentifier
    }
    
    func setIndexPath(indexPath: IndexPath) {
        self.indexPathInDataSource = indexPath
    }

}

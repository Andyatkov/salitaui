//
//  GenericListItem.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

/// Base implementation of collection item.
public class GenericListItem<Cell: ReusableViewProtocol>: ListItem where Cell: UIView {
    
    weak var instance: Cell?
    
    public var viewModel: Cell.ViewModel
    
    public override var anyViewModel: AnyViewModel? {
        return self.viewModel
    }
    
    public override var view: UIView? {
        return self.instance
    }
    
    public override var viewType: AnyClass {
        return Cell.self
    }
    
    /// Initializer
    ///
    /// - Parameters:
    ///   - viewModel: View model.
    ///   - reuseIdentifier: Reuse identifier. If it set to nil, it will use Cell.reuseIdentifier
    public init(viewModel: Cell.ViewModel, uniqueID: String? = nil, reuseIdentifier: String? = nil) {
        let identifier = uniqueID ?? GenericListItem<Cell>.generateUniqueIdentifier()
        let reuseIdentifier = reuseIdentifier ?? Cell.reuseIdentifier
        
        self.viewModel = viewModel
        
        super.init(uniqueID: identifier, reuseIdentifier: reuseIdentifier)
    }
    
    /// Create array of items
    ///
    /// - Parameter viewModels: Array of view models.
    /// - Returns: Collection items.
    public class func create(with viewModels: [Cell.ViewModel]) -> [GenericListItem<Cell>] {
        return viewModels.compactMap() { GenericListItem<Cell>(viewModel: $0) }
    }
    
    public override func configure(instance: UIView) {
        self.instance = instance as? Cell
        self.instance?.configure(viewModel: self.viewModel)
    }
    
    public override func manualCalculateSize() -> CGSize {
        return Cell.manualCalculateSize(for: self.viewModel)
    }
    
}

public class GenericCollectionItem<Cell: ReusableViewProtocol>: GenericListItem<Cell> where Cell: UICollectionReusableView {
    //
}

public class GenericTableItem<Cell: ReusableViewProtocol>: GenericListItem<Cell> where Cell: UITableViewCell {
    //
}


//
//  UniqueObjectProtocol.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import Foundation

public protocol UniqueObjectProtocol {
    var uniqueID: String { get }
}

extension UniqueObjectProtocol {
    
    static func generateUniqueIdentifier() -> String {
        return UUID().uuidString + "\(Date().timeIntervalSince1970)"
    }
    
}

//
//  ListSection.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import Foundation

/// Base implementation of collection view section.
public final class ListSection: UniqueObjectProtocol {
    
    /// Unique section identifier.
    public let uniqueID: String
    
    /// Collection section index.
    public private(set) var indexInDataSource: Int?
    
    /// Collection items.
    public var items: [ListItem] = []
    
    /// Collection supplimentary elements. String key in dictionary must be equal to supplimentary element kind.
    /// For example for UICollectionFlowLayout it will be UICollectionView.elementKindSectionHeader.
    public let supplementaryElements: [SupplementaryKind: ListItem]
    
    var numberOfItems: Int {
        return items.count
    }
    
    /// Inializer.
    ///
    /// - Parameters:
    ///   - identifier: Section identifier.
    ///   - supplementaryElements: Supplementary elements.
    ///   - items: Collection items.
    public init(identifier: String? = nil, supplementaryElements: [SupplementaryKind: ListItem] = [:], items: [ListItem] = []) {
        self.uniqueID = identifier ?? ListSection.generateUniqueIdentifier()
        self.supplementaryElements = supplementaryElements
        self.items = items
    }
    
    func setIndex(_ index: Int) {
        indexInDataSource = index
    }
    
}


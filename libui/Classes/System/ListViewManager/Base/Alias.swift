//
//  Alias.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public typealias AnyViewModel = Any
public typealias IndexPathClosure = (IndexPath) -> Void
public typealias CollectionActionClosure = (Any) -> Void

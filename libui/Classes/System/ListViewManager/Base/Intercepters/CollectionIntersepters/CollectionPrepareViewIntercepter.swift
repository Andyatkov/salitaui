//
//  CollectionPrepareViewIntercepter.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import Foundation

public protocol CollectionPrepareViewIntercepter {
    func performActions(for data: CollectionIntercepterData)
}

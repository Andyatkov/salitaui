//
//  CollectionIntercepterData.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public struct CollectionIntercepterData {
    public let storage: ListStorageProtocol
    public let collectionView: UICollectionView
    public let view: UICollectionReusableView?
    public let indexPath: IndexPath
    public let kind: SupplementaryKind?
}

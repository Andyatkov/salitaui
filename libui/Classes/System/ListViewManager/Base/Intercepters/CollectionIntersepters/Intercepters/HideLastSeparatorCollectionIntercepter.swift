//
//  HideLastSeparatorCollectionIntercepter.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public final class HideLastSeparatorCollectionIntercepter: CollectionPrepareViewIntercepter {
    
    public init() {}
    
    public func hideSeparator(for cell: UICollectionReusableView?, at indexPath: IndexPath, storage: ListStorageProtocol) {
        let numberOfItems = storage.sections[indexPath.section].items.count
        let isHidden = numberOfItems == indexPath.row + 1
        
        if let cell = cell as? CustomSeparatorViewProtocol {
            cell.setSeparator(isLast: isHidden)
        }
    }
    
    public func performActions(for data: CollectionIntercepterData) {
        guard data.storage.isValid(indexPath: data.indexPath) else { return }
        
        hideSeparator(for: data.view, at: data.indexPath, storage: data.storage)
    }
    
}


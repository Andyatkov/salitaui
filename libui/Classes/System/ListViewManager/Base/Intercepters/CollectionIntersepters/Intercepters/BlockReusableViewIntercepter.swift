//
//  BlockReusableViewIntercepter.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public final class BlockReusableViewIntercepter: CollectionPrepareViewIntercepter {
    
    let block: (CollectionIntercepterData) -> Void
    
    public init(block: @escaping (CollectionIntercepterData) -> Void) {
        self.block = block
    }
    
    public func performActions(for data: CollectionIntercepterData) {
        self.block(data)
    }
    
}


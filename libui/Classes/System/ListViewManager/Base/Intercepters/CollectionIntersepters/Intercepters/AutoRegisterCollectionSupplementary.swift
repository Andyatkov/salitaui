//
//  AutoRegisterCollectionSupplementary.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public final class AutoRegisterCollectionSupplementary: CollectionPrepareViewIntercepter {
    
    /// All supplementary view registred ids.
    public var registeredSupplementaryIDs: Set<String> = []
    
    public init() {}
    
    public func registerSupplementary(collectionView: UICollectionView, item: ListItem, kind: String?) {
        guard let kind = kind else { return }
        
        let reuseIdentifier = item.reuseIdentifier
        let reuseIdentifierKey = kind + "." + reuseIdentifier
        
        guard !registeredSupplementaryIDs.contains(reuseIdentifier) else {
            return
        }
        
        let viewType: AnyClass = item.viewType
        let sourceBundle = Bundle(for: viewType)
        if sourceBundle.path(forResource: reuseIdentifier, ofType: "nib") != nil {
            let nib = UINib(nibName: reuseIdentifier, bundle: sourceBundle)
            collectionView.register(nib, forSupplementaryViewOfKind: kind, withReuseIdentifier: reuseIdentifier)
        } else {
            collectionView.register(viewType, forSupplementaryViewOfKind: kind, withReuseIdentifier: reuseIdentifier)
        }
        
        registeredSupplementaryIDs.insert(reuseIdentifierKey)
    }
    
    public func performActions(for data: CollectionIntercepterData) {
        let section = data.indexPath.section
        guard let kind = data.kind, let item = data.storage.supplementary(at: section, kind: kind) else { return }
        registerSupplementary(collectionView: data.collectionView, item: item, kind: kind.kind(type: .collection))
    }
    
}


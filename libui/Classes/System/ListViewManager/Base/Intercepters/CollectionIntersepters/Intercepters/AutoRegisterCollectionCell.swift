//
//  AutoRegisterCollectionCell.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public final class AutoRegisterCollectionCell: CollectionPrepareViewIntercepter {
    
    /// All cells registred ids.
    public var registeredCellIDs: Set<String> = []
    
    public init() {}
    
    func registerCell(collectionView: UICollectionView, item: ListItem) {
        let reuseIdentifier = item.reuseIdentifier
        
        guard !registeredCellIDs.contains(reuseIdentifier) else {
            return
        }
        
        let viewType: AnyClass = item.viewType
        let sourceBundle = Bundle(for: viewType)
        
        if sourceBundle.path(forResource: reuseIdentifier, ofType: "nib") != nil {
            let nib = UINib(nibName: reuseIdentifier, bundle: sourceBundle)
            collectionView.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
        } else {
            collectionView.register(viewType, forCellWithReuseIdentifier: reuseIdentifier)
        }
        
        registeredCellIDs.insert(reuseIdentifier)
    }
    
    public func performActions(for data: CollectionIntercepterData) {
        guard let item = data.storage.item(at: data.indexPath) else { return }
        
        registerCell(collectionView: data.collectionView, item: item)
    }
    
}


//
//  TableIntercepterData.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public struct TableIntercepterData {
    public let storage: ListStorageProtocol
    public let tableView: UITableView
    public let view: UIView?
    public let indexPath: IndexPath
    public let kind: SupplementaryKind?
}


//
//  AutoregisterTableSupplementary.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public final class AutoRegisterTableSupplementary: TablePrepareViewIntercepter {
    
    /// All supplementary view registred ids.
    public var registeredSupplementaryIDs: Set<String> = []
    
    public init() {}
    
    public func registerSupplementary(tableView: UITableView, item: ListItem) {
 
        let reuseIdentifier = item.reuseIdentifier
        
        guard !registeredSupplementaryIDs.contains(reuseIdentifier) else {
            return
        }
        
        let viewType: AnyClass = item.viewType
        let sourceBundle = Bundle(for: viewType)
        if sourceBundle.path(forResource: reuseIdentifier, ofType: "nib") != nil {
            let nib = UINib(nibName: reuseIdentifier, bundle: sourceBundle)
            tableView.register(nib, forHeaderFooterViewReuseIdentifier: reuseIdentifier)
        } else {
            tableView.register(viewType, forHeaderFooterViewReuseIdentifier: reuseIdentifier)
        }
        
        registeredSupplementaryIDs.insert(reuseIdentifier)
    }
    
    public func performActions(for data: TableIntercepterData) {
        let section = data.indexPath.section
        guard let kind = data.kind, let item = data.storage.supplementary(at: section, kind: kind) else { return }
        
        self.registerSupplementary(tableView: data.tableView, item: item)
    }
    
}


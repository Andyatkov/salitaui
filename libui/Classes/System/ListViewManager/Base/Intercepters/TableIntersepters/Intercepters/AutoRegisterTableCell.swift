//
//  AutoRegisterTableCell.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public final class AutoRegisterTableCell: TablePrepareViewIntercepter {
    
    /// All cells registred ids.
    public var registeredCellIDs: Set<String> = []
    
    public init() {}
    
    func registerCell(tableView: UITableView, item: ListItem) {
        let reuseIdentifier = item.reuseIdentifier
        
        guard !self.registeredCellIDs.contains(reuseIdentifier) else {
            return
        }
        
        let viewType: AnyClass = item.viewType
        let sourceBundle = Bundle(for: viewType)
        
        if sourceBundle.path(forResource: reuseIdentifier, ofType: "nib") != nil {
            let nib = UINib(nibName: reuseIdentifier, bundle: sourceBundle)
            tableView.register(nib, forCellReuseIdentifier: reuseIdentifier)
        } else {
            tableView.register(viewType, forCellReuseIdentifier: reuseIdentifier)
        }
        
        self.registeredCellIDs.insert(reuseIdentifier)
    }
    
    public func performActions(for data: TableIntercepterData) {
        guard let item = data.storage.item(at: data.indexPath) else { return }
        self.registerCell(tableView: data.tableView, item: item)
    }
    
}


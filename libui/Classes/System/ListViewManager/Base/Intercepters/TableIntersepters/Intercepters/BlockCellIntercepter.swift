//
//  BlockCellIntercepter.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public final class BlockCellIntercepter: TablePrepareViewIntercepter {
    
    let block: (TableIntercepterData) -> Void
    
    public init(block: @escaping (TableIntercepterData) -> Void) {
        self.block = block
    }
    
    public func performActions(for data: TableIntercepterData) {
        self.block(data)
    }
    
}

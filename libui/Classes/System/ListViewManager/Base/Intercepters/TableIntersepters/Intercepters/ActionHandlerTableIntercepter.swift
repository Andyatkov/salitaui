//
//  ActionHandlerTableIntercepter.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public final class ActionHandlerTableIntercepter: TablePrepareViewIntercepter {
    
    public init() {}
    
    public func performActions(for data: TableIntercepterData) {
        guard let view = data.view as? CollectionActionHandlerProtocol else { return }
        
        var customAction: CollectionActionClosure?
        
        if let kind = data.kind {
            guard let item = data.storage.sections[data.indexPath.section].supplementaryElements[kind] else { return }
            customAction = item.customAction
        } else {
            guard let item = data.storage.item(at: data.indexPath) else { return }
            customAction = item.customAction
        }
        
        view.set(actionHandler: customAction)
    }
    
}


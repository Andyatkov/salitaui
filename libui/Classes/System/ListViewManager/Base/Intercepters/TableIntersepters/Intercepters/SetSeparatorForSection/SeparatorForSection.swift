//
//  SeparatorForSection.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 19.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public struct SeparatorForSection: Hashable, Equatable {
    
    public let index: Int
    public let separatorInset: UIEdgeInsets?
    
    public init(index: Int, separatorInset: UIEdgeInsets?) {
        self.index = index
        self.separatorInset = separatorInset
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(index)
    }
    
    static public func == (lhs: SeparatorForSection, rhs: SeparatorForSection) -> Bool {
        return lhs.index == rhs.index
    }
}

//
//  SetSeparatorForSectionTableIntercepter.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 19.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

private extension UIEdgeInsets {
    
    static let hideSeparatorInset = UIEdgeInsets(top: 0.0,
                                                 left: .greatestFiniteMagnitude,
                                                 bottom: 0.0,
                                                 right: 0.0)
    static let defaultSeparatorInset = UIEdgeInsets(top: 0.0,
                                                    left: 16.0,
                                                    bottom: 0.0,
                                                    right: 0.0)
    
}

public final class SetSeparatorForSectionTableIntercepter: TablePrepareViewIntercepter {
    
    private var sectionIndixes: Set<SeparatorForSection>
    
    public init(sections: [Int]) {
        let separatorForSections = sections.map { (sectionIndex) -> SeparatorForSection in
            return SeparatorForSection(index: sectionIndex, separatorInset: nil)
        }
        self.sectionIndixes = Set<SeparatorForSection>(separatorForSections)
    }
    
    public init(sections: [SeparatorForSection]) {
        self.sectionIndixes = Set<SeparatorForSection>(sections)
    }
    
    public func setSeparator(for cell: UITableViewCell, at indexPath: IndexPath, storage: ListStorageProtocol) {
        if !sectionIndixes.contains(SeparatorForSection(index: indexPath.section, separatorInset: .zero)) {
            cell.separatorInset = .hideSeparatorInset
        } else if let item = sectionIndixes.first(where: { $0.index == indexPath.section }) {
            cell.separatorInset = item.separatorInset ?? .defaultSeparatorInset
        }
    }
    
    public func performActions(for data: TableIntercepterData) {
        guard
            data.storage.isValid(indexPath: data.indexPath),
            let cell = data.view as? UITableViewCell else { return }
        
        setSeparator(for: cell, at: data.indexPath, storage: data.storage)
    }
    
}


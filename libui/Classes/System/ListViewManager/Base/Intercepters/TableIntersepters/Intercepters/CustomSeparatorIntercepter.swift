//
//  CustomSeparatorIntercepter.swift
//  WBLibui
//
//  Created by Orlov Maxim on 10.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public final class CustomSeparatorIntercepter: TablePrepareViewIntercepter {
    
    public init() {}

    public func setCustomSepartor(for cell: UITableViewCell, at indexPath: IndexPath, storage: ListStorageProtocol) {
        let numberOfItems = storage.sections[indexPath.section].items.count
        let isLast = numberOfItems == indexPath.row + 1
        
        if let cell = cell as? CustomSeparatorViewProtocol {
            cell.setSeparator(isLast: isLast)
        }
    }
    
    public func performActions(for data: TableIntercepterData) {
        guard
            data.storage.isValid(indexPath: data.indexPath),
            let cell = data.view as? UITableViewCell else { return }
        
        setCustomSepartor(for: cell, at: data.indexPath, storage: data.storage)
    }
    
}

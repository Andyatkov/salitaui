//
//  HideFooterTableIntercepter.swift
//  WBLibui
//
//  Created by Andrey Dyatkov on 30.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit


public final class HideFooterTableIntercepter: TablePrepareViewIntercepter {
    
    private var footerHeight: CGFloat = 60.0
    
    public init() {}
    
    public init(footerHeight: CGFloat) {
        self.footerHeight = footerHeight
    }
    
    public func performActions(for data: TableIntercepterData) {
        
        let emptyFooterView = UIView()
        emptyFooterView.frame.size.height = footerHeight
        data.tableView.tableFooterView = emptyFooterView
    }
    
}

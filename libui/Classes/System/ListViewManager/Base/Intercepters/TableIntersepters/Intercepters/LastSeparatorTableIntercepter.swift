//
//  LastSeparatorTableIntercepter.swift
//  Demo
//
//  Created by Andrey Dyatkov on 03.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

public final class LastSeparatorTableIntercepter: TablePrepareViewIntercepter {
    
    public init() {}
    
    public func setLastSeparator(for cell: UITableViewCell, at indexPath: IndexPath, storage: ListStorageProtocol) {
        let numberOfItems = storage.sections[indexPath.section].items.count
        let isLast = numberOfItems == indexPath.row + 1
        
        if isLast {
            cell.separatorInset = .zero
        }
    }
    
    public func performActions(for data: TableIntercepterData) {
        guard
            data.storage.isValid(indexPath: data.indexPath),
            let cell = data.view as? UITableViewCell else { return }
        
        setLastSeparator(for: cell, at: data.indexPath, storage: data.storage)
    }
    
}


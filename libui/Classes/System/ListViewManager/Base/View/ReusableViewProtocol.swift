//
//  ReusableViewProtocol.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import CoreGraphics

/// Protocol for reusable views.
public protocol ReusableViewProtocol: class {
    associatedtype ViewModel
    
    /// View reuse identifier
    static var reuseIdentifier: String { get }
    
    /// Configure resuable view with view model.
    ///
    /// - Parameter viewModel: View model.
    func configure(viewModel: ViewModel)
    
    /// Static method to manual calculate size for specific view model.
    ///
    /// - Parameter viewModel: View model.
    /// - Returns: Calculated size.
    static func manualCalculateSize(for viewModel: ViewModel) -> CGSize
}

public extension ReusableViewProtocol {
    
    /// Base implementation return reusable view class name.
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    static func manualCalculateSize(for viewModel: ViewModel) -> CGSize {
        return .zero
    }
    
}


//
//  CollectionActionHandlerProtocol.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import Foundation

/// Protocol to views that can perform custom actions.
public protocol CollectionActionHandlerProtocol: class {
    /// Custom action handler.
    func set(actionHandler: CollectionActionClosure?)
}

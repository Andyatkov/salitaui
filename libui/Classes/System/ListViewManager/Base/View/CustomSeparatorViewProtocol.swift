//
//  CustomSeparatorViewProtocol.swift
//  libui
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import Foundation

/// Protocol to views that have custom separator.
public protocol CustomSeparatorViewProtocol {
    /// Set separator state.
    ///
    /// - Parameter isLast: True - if separator is last
    func setSeparator(isLast: Bool)
}

//
//  AppDelegate.swift
//  libui
//
//  Created by Andrey Dyatkov on 19.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import SalitaUI
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var disposables: [Disposable]?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UIFont.registerAllFonts()
        
        ApplicationThemesManager.palette = .default

        disposables = [
            UINavigationBar.observe(theme: \DefaultTheme.navigationBar, on: WBLibThemeManager.wblibuiManager) { $0.appearance() },
            //UIScrollView.observe(theme: \DefaultTheme.scrollView, on: WBLibThemeManager.wblibuiManager) { $0.appearance() },
        ]
        //TODO: - Добавим когда захотим темную тему
        //UIViewController.classInit
        return true
    }

}



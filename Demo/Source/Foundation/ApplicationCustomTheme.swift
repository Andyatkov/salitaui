//
//  ApplicationCustomTheme.swift
//  Demo
//
//  Created by Andrey Dyatkov on 03.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import SalitaUI

struct ApplicationCustomTheme: ColorTheme {
    
    init(palette: Palette) {}
}


class ApplicationThemesManager {
    
    static var palette: Palette = .default {
        didSet {
            WBLibThemeManager.palette = palette
            ApplicationThemesManager.customManager.theme = ApplicationCustomTheme(palette: palette)
        }
    }
    
    private static let wblibuiManager = WBLibThemeManager.wblibuiManager
    private static let customManager = ThemeManager()
    
}

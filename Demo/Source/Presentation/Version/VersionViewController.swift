//
//  VersionViewController.swift
//  Demo
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import SalitaUI
import UIKit

class VersionViewController: UIViewController {
    
    let infoLabel = UILabel()
    
    override func loadView() {
        super.loadView()
        
        view.addSubview(infoLabel)
        view.backgroundColor = .white
        
        infoLabel.numberOfLines = 0
        infoLabel.font = UIFont(name: "Menlo", size: 12)
        
        infoLabel.textColor = .gray
        infoLabel.textAlignment = .center
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        infoLabel.text =
            "libui\n\(SalitaUI.version) @ \(SalitaUI.build)\n\(SalitaUI.commit)\n\n" +
            "demo\n\(Demo.version) @ \(Demo.build)\n\(Demo.commit)"
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        infoLabel.sizeToFit()
        infoLabel.frame = CGRect(
            x: 0.0,
            y: view.bounds.size.height -
                infoLabel.bounds.size.height - view.safeAreaInsets.bottom - 5.0,
            width: view.bounds.size.width,
            height: infoLabel.bounds.size.height
        )
    }
    
}


//
//  DemoListViewController.swift
//  Demo
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

class DemoListViewController: UIViewController {
    
    var demos = [DemoItem]()
    
    var tableView: UITableView!
    
    override func loadView() {
        super.loadView()
        
        tableView = UITableView()
        view.addSubview(tableView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let insets = view.safeAreaInsets
        let frame = CGRect(
            x: insets.left,
            y: insets.top,
            width: view.frame.width - insets.left - insets.right,
            height: view.frame.height - insets.top - insets.bottom)
        
        tableView.frame = frame
    }
    
}

extension DemoListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = demos[indexPath.row]
        let st = UIStoryboard(name: item.storyboard, bundle: nil)
        let vc = st.instantiateViewController(withIdentifier: "\(item.controllerClass)")
        
        vc.title = item.name

        switch item.presentation {
        case .modal:
            present(vc, animated: true, completion: nil)
        case .push:
            navigationController?.pushViewController(vc, animated: true)
        }
    }

}

extension DemoListViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return demos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") else { preconditionFailure() }
        cell.textLabel?.text = demos[indexPath.row].name
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }

}


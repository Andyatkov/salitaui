//
//  ButtonsViewController.swift
//  Demo
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import SalitaUI
import UIKit

class ButtonsViewController: UIViewController {

    @IBOutlet var fillButton: FillButton!
    @IBOutlet var fillSwitch: UISwitch!
    
    @IBOutlet var borderButton: BorderButton!
    @IBOutlet var borderSwitch: UISwitch!
    
    @IBOutlet var plainButton: PlainButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fillButton.setTitle("Fill Button", for: [.normal])
        borderButton.setTitle("Border Button", for: [.normal])
    }
    
    @IBAction func fillSwitchAction(_ sender: UISwitch) {
        fillButton.isEnabled = sender.isOn
    }
    
    @IBAction func borderSwitchAction(_ sender: UISwitch) {
        borderButton.isEnabled = sender.isOn
    }
    
}

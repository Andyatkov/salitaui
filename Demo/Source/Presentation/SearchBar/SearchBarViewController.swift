//
//  SearchBarViewController.swift
//  Demo
//
//  Created by Andrey Dyatkov on 03.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import SalitaUI
import UIKit

class SearchBarViewController: ScrollViewController {

    @IBOutlet var icontentView: UIView!
    @IBOutlet weak var searchBar: SearchBar!
    
    override var contentView: UIView {
        return icontentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        view.addGestureRecognizer(tap)
        
        scrollView.keyboardDismissMode = .onDrag
        
        setupSeacrhBar()
    }
    
    func setupSeacrhBar() {
        searchBar.delegate = self
    }
    
    @objc func handleTap() {
        view.endEditing(true)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
}

extension SearchBarViewController: SearchBarDelegate {
}

// MARK: - Keyboard

extension SearchBarViewController {

    func setupViewResizerOnKeyboardShown() {
        NotificationCenter.default.addObserver(self,
            selector: #selector(keyboardWillShowForResizing),
            name: UIWindow.keyboardWillShowNotification,
            object: nil)
        
        NotificationCenter.default.addObserver(self,
            selector: #selector(keyboardWillHideForResizing),
            name: UIWindow.keyboardWillHideNotification,
            object: nil)
    }
    
    @objc func keyboardWillShowForResizing(notification: Notification) {
        guard let keyboardFrameValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let keyboardFrame = view.convert(keyboardFrameValue.cgRectValue, from: nil)
        
        scrollView.contentInset.bottom = keyboardFrame.size.height + 40
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }
    
    @objc func keyboardWillHideForResizing(notification: Notification) {
        scrollView.contentInset = .zero
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }

}



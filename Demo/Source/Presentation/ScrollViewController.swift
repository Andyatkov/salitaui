//
//  ScrollViewController.swift
//  Demo
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

class ScrollViewController: UIViewController {
    
    private(set) var scrollView: UIScrollView!
    
    var contentView: UIView {
        fatalError("Override this property")
    }
    
    override func loadView() {
        super.loadView()
        
        view.backgroundColor = .white
        
        scrollView = UIScrollView()
        scrollView.delaysContentTouches = false
        
        addScrollView()
        addContentView()
    }
    
    func addScrollView() {
        view.addSubview(scrollView)
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        
        let top = scrollView.topAnchor.constraint(equalTo: view.topAnchor)
        let bottom = scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        let left = scrollView.leftAnchor.constraint(equalTo: view.leftAnchor)
        let right = scrollView.rightAnchor.constraint(equalTo: view.rightAnchor)
        
        NSLayoutConstraint.activate([top, bottom, left, right])
    }
    
    func addContentView() {
        contentView.removeFromSuperview()
        scrollView.addSubview(contentView)
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        let top = contentView.topAnchor.constraint(equalTo: scrollView.topAnchor)
        let bottom = contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor)
        let left = contentView.leftAnchor.constraint(equalTo: scrollView.leftAnchor)
        let right = contentView.rightAnchor.constraint(equalTo: scrollView.rightAnchor)
        let width = contentView.widthAnchor.constraint(equalTo: view.widthAnchor)
        
        NSLayoutConstraint.activate([top, bottom, left, right, width])
    }

}


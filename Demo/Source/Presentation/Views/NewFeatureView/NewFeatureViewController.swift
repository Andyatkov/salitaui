//
//  NewFeatureViewController.swift
//  Demo
//
//  Created by Orlov Maxim on 19.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import SalitaUI
import UIKit

class NewFeatureViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var newFeatureView = NewFeatureView()
    
    override func viewDidLoad() {

        setupTableView()
        setupNewFeatureView()
    }
    
    private func setupTableView() {
        
        newFeatureView.frame.size.height = 142.0
        tableView.tableHeaderView = newFeatureView
    }
    
    private func setupNewFeatureView() {
        
        newFeatureView.delegate = self
        newFeatureView.configureViewWith(NewFeatureViewViewModel(titleLabelText: "Новый раздел",
                                                                 descriptionLabelText: "Тут долно быть описание нового раздела",
                                                                 bottomLabelText: "Отлично, нажмите продолжить",
                                                                 image: UIImage(asset: Asset.Views.NewFeatureView.ilBag)))
    }
    
}

extension NewFeatureViewController: NewFeatureViewDelegate {
    
    func continueButtonTapped() {
        
        tableView.beginUpdates()
        newFeatureView.frame.size.height = 0.0
        tableView.tableHeaderView = nil
        tableView.endUpdates()
    }

}

//
//  ViewsViewController.swift
//  Demo
//
//  Created by Orlov Maxim on 19.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

class ViewsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    private let demoViews = [
        DemoItem(name: "NewFeatureView", controllerClass: NewFeatureViewController.self, storyboard: "Main", presentation: .push),
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
   
}
 
extension ViewsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return demoViews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = demoViews[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = model.name
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
}

extension ViewsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = demoViews[indexPath.row]
        let st = UIStoryboard(name: item.storyboard, bundle: nil)
        let vc = st.instantiateViewController(withIdentifier: "\(item.controllerClass)")
        
        vc.title = item.name
        
        switch item.presentation {
        case .modal:
            present(vc, animated: true, completion: nil)
        case .push:
            navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}

//
//  HiddenMenuViewController.swift
//  Demo
//
//  Created by Andrey Dyatkov on 14.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

class HiddenMenuViewController: UIViewController {
    
    let stackView = UIStackView()
    let scrollView = UIScrollView()
    
    var mainView: UIView {
        fatalError("Override this property")
    }
    
    var menuView: UIView {
        fatalError("Override this property")
    }
    
    override func loadView() {
        super.loadView()
        
        scrollView.backgroundColor = .clear
        
        view.addSubview(scrollView)
        
        mainView.removeFromSuperview()
        menuView.removeFromSuperview()
        
        scrollView.addSubview(mainView)
        scrollView.addSubview(menuView)
        
        scrollView.bounces = false
        scrollView.delaysContentTouches = false
        scrollView.canCancelContentTouches = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentInsetAdjustmentBehavior = .never
        
        mainView.translatesAutoresizingMaskIntoConstraints = true
        menuView.translatesAutoresizingMaskIntoConstraints = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let insets = view.layoutMargins
        let frame = view.frame
        
        scrollView.frame = CGRect(x: 0, y: insets.top, width: frame.width, height: frame.height - insets.top - insets.bottom)
        
        mainView.frame = UIScreen.main.bounds
        mainView.frame.size.height -= insets.top + insets.bottom
        
        
        menuView.frame.size.width = mainView.frame.width
        menuView.frame.origin.y = mainView.frame.origin.y + mainView.frame.height
        menuView.sizeToFit()
        
        let height = mainView.frame.origin.y + mainView.frame.height + menuView.frame.height
        scrollView.contentSize = CGSize(width: frame.width, height: height)
    }
    
}

extension HiddenMenuViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate { scrollToContent(scrollView) }
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        scrollToContent(scrollView)
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        scrollToContent(scrollView)
    }

    func scrollToContent(_ scrollView: UIScrollView) {
        var offset = scrollView.contentOffset

        if offset.y < menuView.frame.height / 2 {
            offset.y = 0
        } else {
            offset.y = menuView.frame.height
        }

        scrollView.setContentOffset(offset, animated: true)
    }

}


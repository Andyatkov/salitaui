//
//  DefaultTableCell.swift
//  Demo
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import SalitaUI
import UIKit

struct DefaultTableCellViewModel {
    let title: String
    let image: AssetImageTypeAlias
}

struct PerfectTableCellViewModel {
    let title: String
}

class PerfectTableCell: UITableViewCell {
}

extension PerfectTableCell: ReusableViewProtocol {
    
    func configure(viewModel: PerfectTableCellViewModel) {
    }
    
}

class DefaultTableCell: UITableViewCell {
    
    @IBOutlet var infoTextLabel: UILabel!
    @IBOutlet var infoImageView: UIImageView!
    
    var actionHandler: CollectionActionClosure?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        infoTextLabel.text = nil
        separatorInset = UIEdgeInsets(top: 0.0,
                                      left: 16.0,
                                      bottom: 0.0,
                                      right: 0.0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        infoTextLabel.preferredMaxLayoutWidth = infoTextLabel.frame.width
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: 60.0)
    }
    
}

extension DefaultTableCell: ReusableViewProtocol {
    
    func configure(viewModel: DefaultTableCellViewModel) {
        infoTextLabel.text = viewModel.title
        infoImageView.image = viewModel.image
    }

}

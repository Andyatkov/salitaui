//
//  TableManagerViewConfigurator.swift
//  Demo
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import SalitaUI
import UIKit

class TableManagerViewConfigurator {
    
    func configureModule() -> UIViewController {
        let st = UIStoryboard(name: "Main", bundle: nil)
        let viewController = st.instantiateViewController(withIdentifier: "TableManagerViewController") as! TableManagerViewController
        let presenter = TableManagerPresenter()
        
        presenter.view = viewController
        viewController.presenter = presenter
        
        let tableManager = DefaultTableManager()
        viewController.tableManager = tableManager
        presenter.storage = tableManager.storage

        return viewController
    }
    
}

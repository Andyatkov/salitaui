//
//  TableManagerViewController.swift
//  Demo
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import SalitaUI
import UIKit

class TableManagerViewController: UIViewController {
    
    var presenter: TableManagerPresenter! = TableManagerPresenter()
    
    @IBOutlet weak var tableView: UITableView!
    
    var tableManager = DefaultTableManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        registerReusableViews()
        
        presenter.viewIsReady()
    }
    
    func configureTableView() {
        tableView.separatorStyle = .singleLine
        view.addSubview(tableView)
        tableManager.configure(for: tableView)
        tableManager.tableViewDelegate = self
        
        tableManager.afterDequeCellInterceptors = [SetSeparatorForSectionTableIntercepter(sections: [1])]
        tableView.delaysContentTouches = false
        tableView.contentInset.bottom = 16.0
        tableView.tableFooterView = UIView()
    }
    
    func registerReusableViews() {
        tableManager.sizeCalculator.register(cellTypes: [DefaultTableCell.self])
    }
    
}

extension TableManagerViewController: TableViewManagerDelegate {
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return presenter.isShowFooter(index: section) ? 60.0 : 0.0
    }
    
}

//
//  TableManagerPresenter.swift
//  Demo
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import SalitaUI

private extension String {
    static let mainIdentifier = "main"
    static let perfectIdentifier = "perfect"
}

enum Animals: String, CaseIterable {
    case bear
    case panda
    case lion
    case dog
    case cat
    
    var isPerfect: Bool {
        switch self {
        case .dog:
            return true
        default:
            return false
        }
    }
}

class TableManagerPresenter {
    
    weak var view: TableManagerViewController!
    var storage: InMemoryListStorage!
    
    fileprivate func configureMainSection(items: [ListItem]) -> ListSection {
        let mainSection = ListSection(identifier: .mainIdentifier, supplementaryElements: [:], items: items)
        return mainSection
    }
    
    fileprivate func configurePerfectSection(items: [ListItem]) -> ListSection {
        let perfectSection = ListSection(identifier: .perfectIdentifier, supplementaryElements: [:], items: items)
        return perfectSection
    }
    
    fileprivate func configureDefaultItem(_ data: DefaultTableCellViewModel) -> GenericListItem<DefaultTableCell> {
        let item = GenericListItem<DefaultTableCell>(viewModel: data)
        item.didSelect = { indexPath in
            print("\(indexPath.row)")
        }
        item.willDisplay = { _ in
            
        }
        return item
    }
    
    fileprivate func configurePerfectItem(_ data: PerfectTableCellViewModel) -> GenericListItem<PerfectTableCell> {
        let item = GenericListItem<PerfectTableCell>(viewModel: data)
        return item
    }
    
    func viewIsReady() {
        
        var mainItems = [ListItem]()
        var perfectItems = [ListItem]()
        
        for animal in Animals.allCases {
            let viewModel = DefaultTableCellViewModel(title: animal.rawValue,
                                                      image: Asset.Placeholder.Product.icProductPlaceholder.image)
            
            let item = configureDefaultItem(viewModel)
            if animal.isPerfect {
                perfectItems.append(item)
            } else {
                mainItems.append(item)
            }
        }
        
        let mainSection = configureMainSection(items: mainItems)
        let perfectSection = configurePerfectSection(items: perfectItems)
        
        storage.setSections([mainSection, perfectSection])
    }
    
    func isShowFooter(index: Int) -> Bool {
        guard
            storage.sections.count > index,
            storage.section(for: .mainIdentifier)?.index == index
            else { return false }
        
        return true
    }
    
}

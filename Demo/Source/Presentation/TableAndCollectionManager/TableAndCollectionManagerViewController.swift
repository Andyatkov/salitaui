//
//  TableAndCollectionManagerViewController.swift
//  Demo
//
//  Created by Andrey Dyatkov on 27.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

enum TableAndCollectionManagerItem: String, CaseIterable {
    case table = "Table Manager"
    case collection = "Collection Manager"
}

class TableAndCollectionManagerViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
    }
    
}

extension TableAndCollectionManagerViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TableAndCollectionManagerItem.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = TableAndCollectionManagerItem.allCases[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = item.rawValue
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = TableAndCollectionManagerItem.allCases[indexPath.row]
        
        switch item {
        case .table:
            let tableViewVC = TableManagerViewConfigurator().configureModule()
            navigationController?.pushViewController(tableViewVC, animated: true)
        case .collection:
            break
        }
    }
    
}

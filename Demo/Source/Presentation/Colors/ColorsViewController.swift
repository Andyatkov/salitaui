//
//  ColorsViewController.swift
//  Demo
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import SalitaUI
import UIKit

class ColorPaletteViewController: UIViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    
    var colors = [(nameColor: String, color: UIColor)]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        let paletteColors = ColorPalette.default.dynamic
        for case let (label?, value) in Mirror(reflecting: paletteColors)
            .children.map({ ($0.label, $0.value) }) {
                if let color = value as? UIColor {
                    colors.append((nameColor: label, color: color))
                }
        }
        
        let collectionViewLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        collectionViewLayout?.sectionInset = UIEdgeInsets(top: 16, left: 8, bottom: 16, right: 8)
        collectionViewLayout?.invalidateLayout()
    }
    
}

extension ColorPaletteViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80)
    }
}

extension ColorPaletteViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? ColorCell else {
            fatalError("Couldn't cast cell to ColorCell")
        }
        
        let paletteColor = colors[indexPath.row]
        
        let textColor: UIColor = (paletteColor.color.isLight() ?? false) ? .black : .white

        cell.hexLabel.text = paletteColor.nameColor
        
        cell.hexLabel.textColor = textColor
        cell.contentView.backgroundColor = paletteColor.color
        
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor(hexString: "#EFEFF4").cgColor
        
        return cell
    }
}


//
//  ColorCell.swift
//  Demo
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

class ColorCell: UICollectionViewCell {

    @IBOutlet var hexLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        hexLabel.text = nil
        titleLabel.text = nil
    }

}

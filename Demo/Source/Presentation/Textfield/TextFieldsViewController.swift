//
//  TextFieldsViewController.swift
//  Demo
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import SalitaUI
import UIKit

class TextFieldsViewController: ScrollViewController {

    @IBOutlet var icontentView: UIView!
    @IBOutlet weak var textField: TextField!
    @IBOutlet weak var searchTextField: SearchTextField!
    @IBOutlet weak var searchCancebleTextField: SearchCancelableTextField!
    @IBOutlet weak var materialTextField: MaterialTextField!
    @IBOutlet weak var strictTextField: StrictTextField!
    @IBOutlet weak var phoneTextField: PhoneTextField!
    @IBOutlet weak var passwordTextField: MaskedTextField!
    @IBOutlet weak var arrowTextField: ArrowTextField!
    @IBOutlet weak var regionTextField: RegionTextField!
    
    override var contentView: UIView {
        return icontentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        view.addGestureRecognizer(tap)
        
        scrollView.keyboardDismissMode = .onDrag
        
        setupTextField()
        setupPhoneTextField()
        setupArrowTextField()
        setupSearchTextField()
        setupStrictTextField()
        setupRegionTextField()
        setupMaterialTextField()
        setupPasswordTextField()
        setupSeacrhCancebleTextField()
        
        setupViewResizerOnKeyboardShown()
    }

    func setupSearchTextField() {
        searchTextField.placeholder = "Поиск"
    }
    
    func setupMaterialTextField() {
        materialTextField.placeholder = "Material Text Field"
        materialTextField.infoText = "Info Text"
    }
    
    func setupPhoneTextField() {
        phoneTextField.autoreplaceRussianCountryCode = true
        phoneTextField.placeholder = "Телефон"
        phoneTextField.delegate = self
    }
    
    func setupStrictTextField() {
        strictTextField.placeholder = "Strict Text Field"
    }
    
    func setupArrowTextField() {
        arrowTextField.placeholder = "ArrowTextField"
    }
    
    func setupTextField() {
        textField.placeholder = "Простой Text Field"
    }
    
    func setupRegionTextField() {
        
        regionTextField.placeholder = "RegionTextField"
        regionTextField.currentRegion = "+380"
        regionTextField.delegate = self
        regionTextField.regionImage = Asset.Textfield.Regions.flagRu.image
    }
    
    func setupPasswordTextField() {
        passwordTextField.placeholder = "Пароль(MaskedTextField)"
        passwordTextField.isMasked = true
    }
    
    func setupSeacrhCancebleTextField() {
        searchCancebleTextField.textField.placeholder = "Подпись"
        searchCancebleTextField.cancelButton.setTitle("Отменить", for: [.normal])
    }
    
    @IBAction func strictSegmentControlAction(sender: UISegmentedControl) {
        changeStatus(sender: sender, textField: strictTextField)
    }
    
    @IBAction func strictSwitchAction(sender: UISwitch) {
        strictTextField.isEnabled = sender.isOn
        self.lockMessage(textField: strictTextField, sender: sender)
    }
    
    @objc func handleTap() {
        view.endEditing(true)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    func lockMessage(textField: MaterialTextField, sender: UISwitch) {
        UIView.animate(withDuration: 1.0 / 6.0) {
            textField.infoText = sender.isOn ? "" : "Locked"
            textField.sizeToFit()
            self.view.layoutIfNeeded()
        }
    }
    
    func changeStatus(sender: UISegmentedControl, textField: StrictTextField) {
        let duration: TimeInterval = 1.0 / 3.0
        
        switch sender.selectedSegmentIndex {
        case 0:
            textField.status = .normal
            
            UIView.animate(withDuration: duration) {
                textField.infoText = nil
                textField.sizeToFit()
                self.view.layoutIfNeeded()
            }
        case 1:
            textField.status = .success
            
            UIView.animate(withDuration: duration) {
                textField.infoText = "Success message!\nSuccess message!"
                textField.sizeToFit()
                self.view.layoutIfNeeded()
            }
        case 2:
            textField.status = .error
            
            UIView.animate(withDuration: duration) {
                textField.infoText = "Error message!"
                textField.sizeToFit()
                self.view.layoutIfNeeded()
            }
        default:
            break
        }
    }
    
}

extension TextFieldsViewController: RegionTextFieldDelegate {
    
    func change(_ phone: String?, isValid: Bool) {
        print(phone ?? "")
        print("is valid = \(isValid)")
    }
    
    
    func tapRegion() {
        print("tapRegion")
    }
    
}

extension TextFieldsViewController: MaterialTextFieldDelegate {
    
    func textFieldDidChange(_ textField: MaterialTextField) {
        var text = textField.text
        
        if self.phoneTextField == textField {
            let phone = self.phoneTextField.phone
            text = "\(phone?.countryCode ?? 0) \(phone?.nationalNumber ?? 0) \(phoneTextField.isValidNumber)"
        }
        
        textField.infoText = text
        textField.sizeToFit()
    }
    
    func textField(_ textField: MaterialTextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let textField = textField as? StrictTextField {
            textField.status = .normal
        }
    
        return true
    }
    
    @nonobjc func textFieldShouldReturn(_ textField: MaterialTextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}

// MARK: - Keyboard

extension TextFieldsViewController {

    func setupViewResizerOnKeyboardShown() {
        NotificationCenter.default.addObserver(self,
            selector: #selector(keyboardWillShowForResizing),
            name: UIWindow.keyboardWillShowNotification,
            object: nil)
        
        NotificationCenter.default.addObserver(self,
            selector: #selector(keyboardWillHideForResizing),
            name: UIWindow.keyboardWillHideNotification,
            object: nil)
    }
    
    @objc func keyboardWillShowForResizing(notification: Notification) {
        guard let keyboardFrameValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let keyboardFrame = view.convert(keyboardFrameValue.cgRectValue, from: nil)
        
        scrollView.contentInset.bottom = keyboardFrame.size.height + 40
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }
    
    @objc func keyboardWillHideForResizing(notification: Notification) {
        scrollView.contentInset = .zero
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }

}


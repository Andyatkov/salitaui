//
//  SwitchesViewController.swift
//  Demo
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

class SwitchesViewController: ScrollViewController {
    
    @IBOutlet var icontentView: UIView!
    
    @IBOutlet var firstSwitch: UISwitch!
    @IBOutlet var secondSwitch: UISwitch!
    @IBOutlet var thirdSwitch: UISwitch!
    
    @IBOutlet var firstLabel: UILabel!
    @IBOutlet var secondLabel: UILabel!
    @IBOutlet var thirdLabel: UILabel!
    
    override var contentView: UIView {
        return icontentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstLabel.text = stringState(for: firstSwitch)
        secondLabel.text = stringState(for: secondSwitch)
        thirdLabel.text = stringState(for: thirdSwitch)
    }
    
    @IBAction func firstSwitchAction(_ sender: UISwitch) {
        firstLabel.text = stringState(for: sender)
    }
    
    @IBAction func secondSwitchAction(_ sender: UISwitch) {
        secondLabel.text = stringState(for: sender)
    }
    
    @IBAction func thirdSwitchAction(_ sender: UISwitch) {
        thirdLabel.text = stringState(for: sender)
    }
    
    func stringState(for sender: UISwitch) -> String {
        if !sender.isEnabled {
            return "State: disabled"
        } else if sender.isOn {
            return "State: On"
        } else {
            return "State: Off"
        }
    }

}


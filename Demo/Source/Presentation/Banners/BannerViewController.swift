//
//  BannerViewController.swift
//  Demo
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import SalitaUI
import UIKit

let stackPresenter = BannerNotification.defaultStackBanner()
let switchPresenter = BannerNotification.defaultSwitchBanner()

class BannerViewController: UIViewController {

    @IBOutlet weak var statusSwitch: CustomSwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func tapAction(_ sender: UIButton) {
        let displayingTime = TimeInterval(randomNumber(range: 1...5))
          let view = TextNotificationView(text: "Notification duration: \(displayingTime)", state: .error)
         
          if statusSwitch.isOn {
              view.state = .success
          } else {
              view.state = .error
          }
        
          var config = BannerNotification.Config.default
          config.displayingTime = displayingTime
          
          switchPresenter.show(banner: view, config: config)
    }
    
    func randomNumber(range: ClosedRange<Int>) -> Int {
        let min = range.lowerBound
        let max = range.upperBound
        return Int(arc4random_uniform(UInt32(1 + max - min))) + min
    }
    
}


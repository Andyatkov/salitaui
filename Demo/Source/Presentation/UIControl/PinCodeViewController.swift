//
//  PinCodeViewController.swift
//  Demo
//
//  Created by Andrey Dyatkov on 14.04.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import SalitaUI
import UIKit

class PinCodeViewController: HiddenMenuViewController, CodeInputControlDelegate {

    @IBOutlet var stepper: UIStepper!
    @IBOutlet var segmentCountLabel: UILabel!
    @IBOutlet var pinCodeControl: PinInputControl!
    
    @IBOutlet var imainView: UIView!
    @IBOutlet var imenuVIew: UIView!
    
    override var mainView: UIView {
        return imainView
    }
    
    override var menuView: UIView {
        return imenuVIew
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.keyboardDismissMode = .onDrag
        
        pinCodeControl.becomeFirstResponder()
        pinCodeControl.backgroundColor = .clear
        pinCodeControl.keyboardType = .numberPad
        pinCodeControl.isOnFocus = true
        
        stepper.value = Double(pinCodeControl.itemsCount)
        
        updateSegmentCountLabel()
        
        pinCodeControl.delegate = self
    }
    
    @IBAction func stepperAction(_ sender: UIStepper) {
        pinCodeControl.itemsCount = Int(stepper.value)
        pinCodeControl.sizeToFit()
        updateSegmentCountLabel()
    }
    
    @IBAction func statusSegmentAction(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            pinCodeControl.status = .normal
        case 1:
            pinCodeControl.status = .success
        case 2:
            pinCodeControl.status = .error
        default:
            break
        }
    }
    
    @IBAction func showKeyboardAction(_ sender: UIButton) {
        pinCodeControl.becomeFirstResponder()
        scrollView.setContentOffset(.zero, animated: true)
    }
    
    func updateSegmentCountLabel() {
        segmentCountLabel.text = "Segment count: \(pinCodeControl.itemsCount)"
    }
    
}

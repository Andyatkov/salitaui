//
//  DemoItem.swift
//  Demo
//
//  Created by Orlov Maxim on 19.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

struct DemoItem {
    enum Presentation {
        case modal
        case push
    }
    
    let name: String
    let controllerClass: UIViewController.Type
    let storyboard: String
    let presentation: Presentation
}

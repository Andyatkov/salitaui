//
//  MainDemosViewController.swift
//  Demo
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import SalitaUI
import UIKit

class MainDemosViewController: DemoListViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Main"
        
        demos = [
            DemoItem(name: "Buttons", controllerClass: ButtonsViewController.self, storyboard: "Main", presentation: .push),
            DemoItem(name: "Switches", controllerClass: SwitchesViewController.self, storyboard: "Main", presentation: .push),
            DemoItem(name: "TextFields", controllerClass: TextFieldsViewController.self, storyboard: "Main", presentation: .push),
            DemoItem(name: "Labels", controllerClass: LabelViewController.self, storyboard: "Main", presentation: .push),
            DemoItem(name: "SearchBar", controllerClass: SearchBarViewController.self, storyboard: "Main", presentation: .push),
            DemoItem(name: "UIControl", controllerClass: PinCodeViewController.self, storyboard: "Main", presentation: .push),
            DemoItem(name: "TableManager", controllerClass: TableAndCollectionManagerViewController.self, storyboard: "Main", presentation: .push),
            DemoItem(name: "Views", controllerClass: ViewsViewController.self, storyboard: "Main", presentation: .push),
            DemoItem(name: "Banner", controllerClass: BannerViewController.self, storyboard: "Main", presentation: .push),
            DemoItem(name: "Icons", controllerClass: IconsViewController.self, storyboard: "Main", presentation: .push),
            DemoItem(name: "Colors", controllerClass: ColorPaletteViewController.self, storyboard: "Main", presentation: .push),
            DemoItem(name: "SegmentControlListVC", controllerClass: SegmentControlListVC.self, storyboard: "Main", presentation: .push),
            DemoItem(name: "Version", controllerClass: VersionViewController.self, storyboard: "Main", presentation: .push)
        ]
    }
    
}


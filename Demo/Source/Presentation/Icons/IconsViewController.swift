//
//  IconsViewController.swift
//  Demo
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import SalitaUI
import UIKit

class ImageSection {
    var title: String
    var icons: [ImageAsset]
    var size: CGSize
    
    init(title: String,
         icons: [ImageAsset],
         size: CGSize) {
        self.title = title
        self.icons = icons
        self.size = size
    }
}

class IconsViewController: UIViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    
    var imageSections: [ImageSection] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        for image in Asset.allImages {
            
            let title = String(format: "Иконки %.0f x %.0f", image.image.size.width, image.image.size.height)
            if let indexSection = imageSections.firstIndex(where: { $0.title == title }) {
                imageSections[indexSection].icons.append(image)
            } else {
                imageSections.append(ImageSection(title: title, icons: [image], size: image.image.size))
            }
        }
        
        imageSections.sort { (sectionFirst, sectionSecond) -> Bool in
            return (sectionFirst.size.width * sectionFirst.size.height) < (sectionSecond.size.width * sectionSecond.size.height)
        }
        
        let collectionViewLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        collectionViewLayout?.sectionInset = UIEdgeInsets(top: 16, left: 8, bottom: 16, right: 8)
        collectionViewLayout?.invalidateLayout()
    }
    
}

extension IconsViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = imageSections[indexPath.section].size.height + 44
        return CGSize(width: height, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 50.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionView.elementKindSectionHeader {
            let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as! SectionHeader
            
            sectionHeader.titleLabel.text = imageSections[indexPath.section].title
            return sectionHeader
        } else {
            return UICollectionReusableView()
        }
    }
    
}

extension IconsViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return imageSections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageSections[section].icons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? IconCell else {
            fatalError("Couldn't cast cell to ColorCell")
        }
        
        let imageAsset = imageSections[indexPath.section].icons[indexPath.row]
        
        cell.imageNameLabel.text = imageAsset.name
        cell.imageView.image = imageAsset.image
        
        return cell
    }
    
}


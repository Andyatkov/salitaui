//
//  IconCell.swift
//  Demo
//
//  Created by Andrey Dyatkov on 26.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

class IconCell: UICollectionViewCell {

    @IBOutlet var imageNameLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        imageNameLabel.text = nil
        imageView.image = nil
    }

}

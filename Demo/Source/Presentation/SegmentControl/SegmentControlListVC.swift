//
//  SegmentControlListVC.swift
//  Demo
//
//  Created by Юрий Андрюшин on 31.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import UIKit

enum Sections: Int, CaseIterable {
    
    case filled
    case line
    
    var title: String {
        
        switch self {
        case .filled: return "Filled style"
        case .line: return "Line style"
        }
    }
    
}

class SegmentControlListVC: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = 44
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.tableFooterView = UIView()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Sections.allCases.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = Sections.allCases[indexPath.row].title
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let section = Sections(rawValue: indexPath.row) else { return }
        
        switch section {
        case .filled: navigationController?.pushViewController(SegmentControlFilledItemVC(), animated: true)
        case .line: navigationController?.pushViewController(SegmentControlLineItemVC(), animated: true)
        }
    }

}

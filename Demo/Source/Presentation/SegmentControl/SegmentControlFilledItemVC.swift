//
//  SegmentControlFilledItemVC.swift
//  Demo
//
//  Created by Юрий Андрюшин on 31.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import SalitaUI
import UIKit

class SegmentControlFilledItemVC: UIViewController {

    private var segmentControlVC = SegmentControlViewController(viewModel: SegmentControlVCViewModel(segmentItemType: .filled,
                                                                                                     isHiddenSeparator: true))
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    // MARK: - Private Properties
    
    private func setup() {
        
        view.backgroundColor = .white
        
        var viewControllers = [UIViewController]()
        
        let vc1 = UIViewController()
        vc1.title = "Первый"
        vc1.view.backgroundColor = .red
        viewControllers.append(vc1)
        
        let vc2 = UIViewController()
        vc2.title = "Второй"
        vc2.view.backgroundColor = .yellow
        viewControllers.append(vc2)
        
        let vc3 = UIViewController()
        vc3.title = "Третий"
        vc3.view.backgroundColor = .green
        viewControllers.append(vc3)
        
        let vc4 = UIViewController()
        vc4.title = "Четвертый"
        vc4.view.backgroundColor = .blue
        viewControllers.append(vc4)
        
        let vc5 = UIViewController()
        vc5.title = "Пятый"
        vc5.view.backgroundColor = .gray
        viewControllers.append(vc5)
        
        let vc6 = UIViewController()
        vc6.title = "Шестой"
        vc6.view.backgroundColor = .brown
        viewControllers.append(vc6)
        
        segmentControlVC.set(viewControllers)
        addChild(segmentControlVC)
        view.addSubview(segmentControlVC.view)
        
        addConstraints()
    }
    
    private func addConstraints() {
    
        segmentControlVC.view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
        
            segmentControlVC.view.topAnchor.constraint(equalTo: view.topAnchor),
            segmentControlVC.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            segmentControlVC.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            segmentControlVC.view.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
    
}

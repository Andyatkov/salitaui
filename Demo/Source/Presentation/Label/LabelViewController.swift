//
//  LabelViewController.swift
//  Demo
//
//  Created by Andrey Dyatkov on 17.03.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import SalitaUI
import UIKit

class LabelViewController: ScrollViewController {

    @IBOutlet var icontentView: UIView!
    @IBOutlet weak var copyableLabel: CopyableLabel!
    @IBOutlet weak var currencyLabel: CurrencyLabel!
    
    override var contentView: UIView {
        return icontentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currencyLabel.text = "1234,56 ₽"
    }

    
}

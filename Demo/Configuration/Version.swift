//
//  Version.swift
//  Demo
//
//  Created by Andrey Dyatkov on 25.02.2020.
//  Copyright © 2020 Andrey Dyatkov. All rights reserved.
//

import Foundation

fileprivate final class BundleDetectionClass {}

/// Build number of the module. Depends on the time only.
public let build: String = Bundle(for: BundleDetectionClass.self).infoDictionary?["CFBundleVersion"] as? String ?? "CORRUPTED"

/// Commit number of the module. Reflects hash of the commit it was built under.
public let commit: String = Bundle(for: BundleDetectionClass.self).infoDictionary?["CFBundleCommit"] as? String ?? "CORRUPTED"

/// Version of the module according to SemVer.
public let version: String = Bundle(for: BundleDetectionClass.self).infoDictionary?["CFBundleShortVersionString"] as? String ?? "CORRUPTED"


// swiftlint:disable all
import Foundation
import UIKit

public struct TextStyle: Equatable {

    public let font: UIFont
    public let textColor: UIColor
    public let paragraphSpacing: CGFloat?
    public let paragraphIndent: CGFloat?
    public let lineHeight: CGFloat?
    public let letterSpacing: CGFloat?

    public var actualLineHeight: CGFloat {
        return lineHeight ?? font.lineHeight
    }

    public init(
        font: UIFont,
        textColor: UIColor,
        paragraphSpacing: CGFloat? = nil,
        paragraphIndent: CGFloat? = nil,
        lineHeight: CGFloat? = nil,
        letterSpacing: CGFloat? = nil
    ) {
        self.font = font
        self.textColor = textColor
        self.paragraphSpacing = paragraphSpacing
        self.paragraphIndent = paragraphIndent
        self.lineHeight = lineHeight
        self.letterSpacing = letterSpacing
    }

    public init(
        fontName: String,
        fontSize: CGFloat,
        textColor: UIColor,
        paragraphSpacing: CGFloat? = nil,
        paragraphIndent: CGFloat? = nil,
        lineHeight: CGFloat? = nil,
        letterSpacing: CGFloat? = nil
    ) {
        self.init(
            font: UIFont(name: fontName, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize),
            textColor: textColor,
            paragraphSpacing: paragraphSpacing,
            paragraphIndent: paragraphIndent,
            lineHeight: lineHeight,
            letterSpacing: letterSpacing
        )
    }

    public func withTextColor(_ textColor: UIColor) -> TextStyle {
        return TextStyle(
            font: font,
            textColor: textColor,
            paragraphSpacing: paragraphSpacing,
            paragraphIndent: paragraphIndent,
            lineHeight: lineHeight,
            letterSpacing: letterSpacing
        )
    }

    public func attributes(
        textColor: UIColor? = nil,
        backgroundColor: UIColor? = nil,
        alignment: NSTextAlignment? = nil,
        lineBreakMode: NSLineBreakMode? = nil,
        ignoringParagraphStyle: Bool = false
    ) -> [NSAttributedString.Key: Any] {
        var attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: textColor ?? self.textColor,
        ]

        if let backgroundColor = backgroundColor {
            attributes[.backgroundColor] = backgroundColor
        }

        if let letterSpacing = letterSpacing {
            attributes[.kern] = NSNumber(value: Float(letterSpacing))
        }

        if ignoringParagraphStyle {
            return attributes
        }

        let paragraphStyle = NSMutableParagraphStyle()

        if let lineHeight = lineHeight {
            let paragraphLineSpacing = (lineHeight - font.lineHeight) / 2.0
            let paragraphLineHeight = lineHeight - paragraphLineSpacing

            paragraphStyle.lineSpacing = paragraphLineSpacing
            paragraphStyle.minimumLineHeight = paragraphLineHeight
            paragraphStyle.maximumLineHeight = paragraphLineHeight
        }

        if let paragraphSpacing = paragraphSpacing {
            paragraphStyle.paragraphSpacing = paragraphSpacing
        }

        if let paragraphIndent = paragraphIndent {
            paragraphStyle.firstLineHeadIndent = paragraphIndent
        }

        if let alignment = alignment {
            paragraphStyle.alignment = alignment
        }

        if let lineBreakMode = lineBreakMode {
            paragraphStyle.lineBreakMode = lineBreakMode
        }

        attributes[.paragraphStyle] = paragraphStyle

        return attributes
    }
}

public extension TextStyle {

    /// Body 16 / Medium
    ///
    /// Font: Lato (Lato-Medium); weight 500.0; size 16.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 22.0
    /// Letter spacing: -0.4
    static let body16Medium = TextStyle(
        fontName: "Lato-Medium",
        fontSize: 16.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 22.0,
        letterSpacing: -0.4
    )

    /// Body 16 / Semibold
    ///
    /// Font: Lato (Lato-SemiBold); weight 600.0; size 16.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 22.0
    /// Letter spacing: -0.4
    static let body16Semibold = TextStyle(
        fontName: "Lato-SemiBold",
        fontSize: 16.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 22.0,
        letterSpacing: -0.4
    )

    /// Body 16 / Bold
    ///
    /// Font: Lato (Lato-Bold); weight 700.0; size 16.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 22.0
    /// Letter spacing: -0.4
    static let body16Bold = TextStyle(
        fontName: "Lato-Bold",
        fontSize: 16.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 22.0,
        letterSpacing: -0.4
    )

    /// Body 16 / Regular
    ///
    /// Font: Lato (Lato-Regular); weight 400.0; size 16.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 22.0
    /// Letter spacing: -0.4
    static let body16Regular = TextStyle(
        fontName: "Lato-Regular",
        fontSize: 16.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 22.0,
        letterSpacing: -0.4
    )

    /// Body 18 / Bold
    ///
    /// Font: Lato (Lato-Bold); weight 700.0; size 18.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 24.0
    /// Letter spacing: -0.4
    static let body18Bold = TextStyle(
        fontName: "Lato-Bold",
        fontSize: 18.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 24.0,
        letterSpacing: -0.4
    )

    /// Body 18 / Semibold
    ///
    /// Font: Lato (Lato-SemiBold); weight 600.0; size 18.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 24.0
    /// Letter spacing: -0.4
    static let body18Semibold = TextStyle(
        fontName: "Lato-SemiBold",
        fontSize: 18.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 24.0,
        letterSpacing: -0.4
    )

    /// Body 18 / Medium
    ///
    /// Font: Lato (Lato-Medium); weight 500.0; size 18.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 24.0
    /// Letter spacing: -0.4
    static let body18Medium = TextStyle(
        fontName: "Lato-Medium",
        fontSize: 18.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 24.0,
        letterSpacing: -0.4
    )

    /// Body 18 / Regular
    ///
    /// Font: Lato (Lato-Regular); weight 400.0; size 18.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 24.0
    /// Letter spacing: -0.4
    static let body18Regular = TextStyle(
        fontName: "Lato-Regular",
        fontSize: 18.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 24.0,
        letterSpacing: -0.4
    )

    /// Large Title 34 / Bold
    ///
    /// Font: Lato (Lato-Bold); weight 700.0; size 34.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 42.0
    /// Letter spacing: 0.4
    static let largeTitle34Bold = TextStyle(
        fontName: "Lato-Bold",
        fontSize: 34.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 42.0,
        letterSpacing: 0.4
    )

    /// Large Title 32 / Bold
    ///
    /// Font: Lato (Lato-Bold); weight 700.0; size 32.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 38.0
    /// Letter spacing: 0.4
    static let largeTitle32Bold = TextStyle(
        fontName: "Lato-Bold",
        fontSize: 32.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 38.0,
        letterSpacing: 0.4
    )

    /// Headline / Bold
    ///
    /// Font: Lato (Lato-Bold); weight 700.0; size 28.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 34.0
    /// Letter spacing: 0.4
    static let headlineBold = TextStyle(
        fontName: "Lato-Bold",
        fontSize: 28.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 34.0,
        letterSpacing: 0.4
    )

    /// Headline / Semibold
    ///
    /// Font: Lato (Lato-SemiBold); weight 600.0; size 28.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 34.0
    /// Letter spacing: 0.4
    static let headlineSemibold = TextStyle(
        fontName: "Lato-SemiBold",
        fontSize: 28.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 34.0,
        letterSpacing: 0.4
    )

    /// Headline / Regular
    ///
    /// Font: Lato (Lato-Regular); weight 400.0; size 28.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 34.0
    /// Letter spacing: 0.4
    static let headlineRegular = TextStyle(
        fontName: "Lato-Regular",
        fontSize: 28.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 34.0,
        letterSpacing: 0.4
    )

    /// Headline 2 / Bold
    ///
    /// Font: Lato (Lato-Bold); weight 700.0; size 24.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 30.0
    /// Letter spacing: 0.4
    static let headline2Bold = TextStyle(
        fontName: "Lato-Bold",
        fontSize: 24.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 30.0,
        letterSpacing: 0.4
    )

    /// Headline 3 / Bold
    ///
    /// Font: Lato (Lato-Bold); weight 700.0; size 22.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 28.0
    /// Letter spacing: 0.4
    static let headline3Bold = TextStyle(
        fontName: "Lato-Bold",
        fontSize: 22.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 28.0,
        letterSpacing: 0.4
    )

    /// Headline 4 / Bold
    ///
    /// Font: Lato (Lato-Bold); weight 700.0; size 20.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 26.0
    /// Letter spacing: 0.4
    static let headline4Bold = TextStyle(
        fontName: "Lato-Bold",
        fontSize: 20.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 26.0,
        letterSpacing: 0.4
    )

    /// Headline 3 / Semibold
    ///
    /// Font: Lato (Lato-SemiBold); weight 600.0; size 22.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 28.0
    /// Letter spacing: 0.4
    static let headline3Semibold = TextStyle(
        fontName: "Lato-SemiBold",
        fontSize: 22.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 28.0,
        letterSpacing: 0.4
    )

    /// Headline 3 / Regular
    ///
    /// Font: Lato (Lato-Regular); weight 400.0; size 22.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 28.0
    /// Letter spacing: 0.4
    static let headline3Regular = TextStyle(
        fontName: "Lato-Regular",
        fontSize: 22.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 28.0,
        letterSpacing: 0.4
    )

    /// Headline 2 / Semibold
    ///
    /// Font: Lato (Lato-SemiBold); weight 600.0; size 24.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 30.0
    /// Letter spacing: 0.4
    static let headline2Semibold = TextStyle(
        fontName: "Lato-SemiBold",
        fontSize: 24.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 30.0,
        letterSpacing: 0.4
    )

    /// Headline 2 / Regular
    ///
    /// Font: Lato (Lato-Regular); weight 400.0; size 24.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 30.0
    /// Letter spacing: 0.4
    static let headline2Regular = TextStyle(
        fontName: "Lato-Regular",
        fontSize: 24.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 30.0,
        letterSpacing: 0.4
    )

    /// Body 14 / Medium
    ///
    /// Font: Lato (Lato-Medium); weight 500.0; size 14.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 20.0
    /// Letter spacing: -0.4
    static let body14Medium = TextStyle(
        fontName: "Lato-Medium",
        fontSize: 14.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 20.0,
        letterSpacing: -0.4
    )

    /// Body 14 / Semibold
    ///
    /// Font: Lato (Lato-SemiBold); weight 600.0; size 14.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 20.0
    /// Letter spacing: -0.4
    static let body14Semibold = TextStyle(
        fontName: "Lato-SemiBold",
        fontSize: 14.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 20.0,
        letterSpacing: -0.4
    )

    /// Tagline 14 / Semibold
    ///
    /// Font: Lato (Lato-SemiBold); weight 600.0; size 14.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 20.0
    /// Letter spacing: -0.25
    static let tagline14Semibold = TextStyle(
        fontName: "Lato-SemiBold",
        fontSize: 14.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 20.0,
        letterSpacing: -0.25
    )

    /// Body 14 / Bold
    ///
    /// Font: Lato (Lato-Bold); weight 700.0; size 14.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 20.0
    /// Letter spacing: -0.4
    static let body14Bold = TextStyle(
        fontName: "Lato-Bold",
        fontSize: 14.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 20.0,
        letterSpacing: -0.4
    )

    /// Body 14 / Regular
    ///
    /// Font: Lato (Lato-Regular); weight 400.0; size 14.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 18.0
    /// Letter spacing: -0.4
    static let body14Regular = TextStyle(
        fontName: "Lato-Regular",
        fontSize: 14.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 18.0,
        letterSpacing: -0.4
    )

    /// Caption 12 / Medium
    ///
    /// Font: Lato (Lato-Medium); weight 500.0; size 12.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 18.0
    /// Letter spacing: -0.4
    static let caption12Medium = TextStyle(
        fontName: "Lato-Medium",
        fontSize: 12.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 18.0,
        letterSpacing: -0.4
    )

    /// Caption 12 / Semibold
    ///
    /// Font: Lato (Lato-SemiBold); weight 600.0; size 12.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 18.0
    /// Letter spacing: -0.4
    static let caption12Semibold = TextStyle(
        fontName: "Lato-SemiBold",
        fontSize: 12.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 18.0,
        letterSpacing: -0.4
    )

    /// Tagline 12 / Semibold
    ///
    /// Font: Lato (Lato-SemiBold); weight 600.0; size 12.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 18.0
    /// Letter spacing: -0.25
    static let tagline12Semibold = TextStyle(
        fontName: "Lato-SemiBold",
        fontSize: 12.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 18.0,
        letterSpacing: -0.25
    )

    /// Caption 12 / Bold
    ///
    /// Font: Lato (Lato-Bold); weight 700.0; size 12.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 18.0
    /// Letter spacing: -0.4
    static let caption12Bold = TextStyle(
        fontName: "Lato-Bold",
        fontSize: 12.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 18.0,
        letterSpacing: -0.4
    )

    /// Caption 10 / Bold
    ///
    /// Font: Lato (Lato-Bold); weight 700.0; size 10.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 16.0
    /// Letter spacing: -0.4
    static let caption10Bold = TextStyle(
        fontName: "Lato-Bold",
        fontSize: 10.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 16.0,
        letterSpacing: -0.4
    )

    /// Caption 10 / Semibold
    ///
    /// Font: Lato (Lato-SemiBold); weight 600.0; size 10.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 16.0
    /// Letter spacing: -0.4
    static let caption10Semibold = TextStyle(
        fontName: "Lato-SemiBold",
        fontSize: 10.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 16.0,
        letterSpacing: -0.4
    )

    /// Caption 10 / Medium
    ///
    /// Font: Lato (Lato-Medium); weight 500.0; size 10.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 16.0
    /// Letter spacing: -0.4
    static let caption10Medium = TextStyle(
        fontName: "Lato-Medium",
        fontSize: 10.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 16.0,
        letterSpacing: -0.4
    )

    /// Caption 10 / Regular
    ///
    /// Font: Lato (Lato-Regular); weight 400.0; size 10.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 16.0
    /// Letter spacing: 0.0
    static let caption10Regular = TextStyle(
        fontName: "Lato-Regular",
        fontSize: 10.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 16.0,
        letterSpacing: 0.0
    )

    /// Caption 12 / Regular
    ///
    /// Font: Lato (Lato-Regular); weight 400.0; size 12.0
    /// Text color: hex: #000000FF; rgba: 0 0 0, 100%
    /// Paragraph spacing: default
    /// Paragraph indent: default
    /// Line height: 18.0
    /// Letter spacing: -0.4
    static let caption12Regular = TextStyle(
        fontName: "Lato-Regular",
        fontSize: 12.0,
        textColor: UIColor(rgbaHex: 0x000000FF),
        paragraphSpacing: nil,
        paragraphIndent: nil,
        lineHeight: 18.0,
        letterSpacing: -0.4
    )
}

public extension String {

    func styled(
        _ textStyle: TextStyle,
        textColor: UIColor? = nil,
        backgroundColor: UIColor? = nil,
        alignment: NSTextAlignment? = nil,
        lineBreakMode: NSLineBreakMode? = nil
    ) -> NSAttributedString {
        return NSAttributedString(
            string: self,
            attributes: textStyle.attributes(
                textColor: textColor,
                backgroundColor: backgroundColor,
                alignment: alignment,
                lineBreakMode: lineBreakMode
            )
        )
    }
}

private extension UIColor {

    convenience init(rgbaHex: UInt32) {
        self.init(
            red: CGFloat((rgbaHex >> 24) & 0xFF) / 255.0,
            green: CGFloat((rgbaHex >> 16) & 0xFF) / 255.0,
            blue: CGFloat((rgbaHex >> 8) & 0xFF) / 255.0,
            alpha: CGFloat(rgbaHex & 0xFF) / 255.0
        )
    }
}
// swiftlint:enable all

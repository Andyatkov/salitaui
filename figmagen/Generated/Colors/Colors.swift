// swiftlint:disable all
import UIKit.UIColor

public enum Colors {

    /// Colors / Elements / wbBrand
    ///
    /// Hex: #6C12C9FF; rgba: 108 18 201, 100%.
    public static let colorsElementsWbBrand = UIColor(rgbaHex: 0x6C12C9FF)

    /// Colors / Elements / wbAccent
    ///
    /// Hex: #7E00E6FF; rgba: 126 0 230, 100%.
    public static let colorsElementsWbAccent = UIColor(rgbaHex: 0x7E00E6FF)

    /// Colors / Elements / wbOrange
    ///
    /// Hex: #FF783CFF; rgba: 255 120 60, 100%.
    public static let colorsElementsWbOrange = UIColor(rgbaHex: 0xFF783CFF)

    /// Colors / Elements / wbGreen
    ///
    /// Hex: #1EA52DFF; rgba: 30 165 45, 100%.
    public static let colorsElementsWbGreen = UIColor(rgbaHex: 0x1EA52DFF)

    /// Colors / Elements / wbRed
    ///
    /// Hex: #EB3546FF; rgba: 235 53 70, 100%.
    public static let colorsElementsWbRed = UIColor(rgbaHex: 0xEB3546FF)

    /// Colors / Elements / wbDarkGray
    ///
    /// Hex: #A7A7A7FF; rgba: 167 167 167, 100%.
    public static let colorsElementsWbDarkGray = UIColor(rgbaHex: 0xA7A7A7FF)

    /// Colors / wbBlack
    ///
    /// Hex: #131313FF; rgba: 19 19 19, 100%.
    public static let colorsWbBlack = UIColor(rgbaHex: 0x131313FF)

    /// Colors / wbGray
    ///
    /// Hex: #E6E6E6FF; rgba: 230 230 230, 100%.
    public static let colorsWbGray = UIColor(rgbaHex: 0xE6E6E6FF)

    /// Colors / wbWhite
    ///
    /// Hex: #FFFFFFFF; rgba: 255 255 255, 100%.
    public static let colorsWbWhite = UIColor(rgbaHex: 0xFFFFFFFF)

    /// Colors / Elements / wbYellow
    ///
    /// Hex: #F5BA45FF; rgba: 245 186 69, 100%.
    public static let colorsElementsWbYellow = UIColor(rgbaHex: 0xF5BA45FF)

    /// Colors / Backgrounds / wbLightGray
    ///
    /// Hex: #F6F6F6FF; rgba: 246 246 246, 100%.
    public static let colorsBackgroundsWbLightGray = UIColor(rgbaHex: 0xF6F6F6FF)
}

private extension UIColor {

    convenience init(rgbaHex: UInt32) {
        self.init(
            red: CGFloat((rgbaHex >> 24) & 0xFF) / 255.0,
            green: CGFloat((rgbaHex >> 16) & 0xFF) / 255.0,
            blue: CGFloat((rgbaHex >> 8) & 0xFF) / 255.0,
            alpha: CGFloat(rgbaHex & 0xFF) / 255.0
        )
    }
}
// swiftlint:enable all
